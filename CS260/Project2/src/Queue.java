/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS260
INSTRUCTOR:		Gyorgy Petruska
DUE DATE:		11/07/2018
 */
import java.util.NoSuchElementException;

public class Queue {
	private Node front;
	private Node rear;
	/**
	 * No args constructor
	 */
	public Queue() {
	  this.front = null;
	  this.rear = null;
	}
	/**
	 * Constructor that sets the fields of front and rear.
	 * @param f Node representing the front of the queue
	 * @param r Node representing the rear of the queue
	 */
	public Queue(Node f, Node r) {
		front = f;
		rear = r;
	}
	/**
	 * add takes a Junction and adds it to the queue. If the queue is
	 * empty, creates a new node and adds it to the queue and sets rear to front.
	 * Otherwise, adds a new node to the rear and sets rear to the link of the node added.
	 * @param junc a Junction to be added to the queue
	 */
	public void add(Junction junc) {
		if(isEmpty()) {
			this.front = new Node(junc, null);
			this.rear = front;
		}else {
			this.rear.setLink(new Node(junc, null));
			this.rear = rear.getLink();
		}

	}
	/**
	 * Throws exception of the queue is empty. Otherwise, returns the data of front and sets front to
	 * the link of front. If the queue is empty now, sets rear to null.
	 * @return the Junction that was removed
	 */
	public Junction remove() {
		Junction answer;
		
		if(isEmpty()) {
			throw new NoSuchElementException();
		}else {
			answer = front.getData();
			front = front.getLink();
		}
		if(isEmpty()) {
			rear = null;
		}
		
		return answer;
	}
	/**
	 * Prints a string representing the queue's contents
	 */
	public String toString() {
		if(isEmpty()) {
			return "Empty Queue";
		}else {
			return front.toString();
		}
	}
	/**
	 * returns if the queue is empty based on if front is null.
	 * @return a boolean representing if the queue is empty
	 */
	public boolean isEmpty() {
		return(front == null);
	}
}
