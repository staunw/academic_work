/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS260
INSTRUCTOR:		Gyorgy Petruska
DUE DATE:		11/07/2018
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Applications {

	public static void main(String[] args) throws FileNotFoundException {
		String eastWest = "eastWest.txt";
		String northSouth1 = "northSouth.txt";
		String northSouth2 = "northSouth1.txt";
		
		File eastWestFile, northSouthFile;
		Scanner eastWestReader, northSouthReader;
		
		
		northSouthFile = new File(northSouth1);
		eastWestFile = new File(eastWest);
		eastWestReader = new Scanner(eastWestFile);
		northSouthReader = new Scanner(northSouthFile);
		
		int length, width;
		
		length = eastWestReader.nextInt();
		width = eastWestReader.nextInt();
		eastWestReader.nextLine();
		
		Queue pathfinder = new Queue(null, null);
		
		Network map = new Network(length, width, pathfinder);
		
		map.connectNetwork(eastWestReader, northSouthReader);
		
		map.displayResult(map.findPath());
		
		eastWestReader.close();
		northSouthReader.close();
		
		pathfinder = null;
		map = null;
		northSouthFile = new File(northSouth2);
		eastWestReader = new Scanner(eastWestFile);
		northSouthReader = new Scanner(northSouthFile);
		
		length = eastWestReader.nextInt();
		width = eastWestReader.nextInt();
		eastWestReader.nextLine();
		
		pathfinder = new Queue(null, null);
		map = new Network(length, width, pathfinder);
		
		System.out.println("\n\n-----------------------------------------------------------------------------\n");
		
		map.connectNetwork(eastWestReader, northSouthReader);
		
		map.displayResult(map.findPath());
		
		eastWestReader.close();
		northSouthReader.close();

	}

}
