/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS260
INSTRUCTOR:		Gyorgy Petruska
DUE DATE:		11/07/2018
 */
/**
 * The Node class represents a simplified linked list data structure.
 * @author Nick Stauffer
 */
public class Node {
	Junction data;
	Node link;
	/**
	 * getter for field data
	 * @return a Junction representing the next link.
	 */
	public Junction getData() {
		return data;
	}
	/**
	 * setter for field data
	 * @param data the Junction reference to set the data to
	 */
	public void setData(Junction data) {
		this.data = data;
	}
	/**
	 * getter for next link in the list
	 * @return a Node representing the next link
	 */
	public Node getLink() {
		return link;
	}
	/**
	 * setter for link field
	 * @param link the Node to be set to the field
	 */
	public void setLink(Node link) {
		this.link = link;
	}
	/**
	 * Constructor for the Node class
	 * @param data the Junction value of the field data
	 * @param link the reference the next link in the list
	 */
	public Node(Junction data, Node link) {
		this.data = data;
		this.link = link;
	}
	/**
	 * recursive toString method that prints each Node in the list.
	 */
	public String toString() {
		String field1 = "";
		String field2 = "";
		if(this.data == null) {
			//field1 = "dummy";
		}else {
			field1 = data.toString();
		}
		if(this.link == null) {
			//field2 = "null in tail";
		}else {
			field2 = link.toString();
		}
		return field1 + "\n" + field2;
	}
}

