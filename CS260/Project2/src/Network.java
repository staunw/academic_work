/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS260
INSTRUCTOR:		Gyorgy Petruska
DUE DATE:		11/07/2018
 */
import java.util.Scanner;
/**
 * the Network class represents a road network comprised of road intersections
 * that are either open or closed to traffic.
 * @author Nick Stauffer
 *
 */
public class Network {
	int length, width, maxDistance;
	Junction entry, exit, maxJunction, toNorth, toSouth, toEast, toWest;
	Junction[][] junctionArray;
	Queue pathfinder;
	/**
	 * Constructor. Sets fields length, width and pathfind. Sets up junctionArray
	 * by instantiating Junction fields row and column appropriately. 
	 * @param length the length(Number of rows) in the junction 2d array.
	 * @param width the width of the junction 2d array
	 * @param pf the Queue field to be set
	 */
	public Network(int length, int width, Queue pf) {
		this.length = length;
		this.width = width;
		pathfinder = pf;
		
		junctionArray = new Junction[this.length][this.width];
		
		for(int i = 0; i < junctionArray.length; i++) {
			for(int j = 0; j < junctionArray[i].length; j++) {
				junctionArray[i][j] = new Junction(i, j);
			}
		}
		
		entry = junctionArray[0][0];
		exit = junctionArray[length - 1][width - 1];
	}
	/**
	 * Takes two scanners and instantiates the junction 2d array's boolean values isSouth and isEast
	 * @param eastWest the scanner that is reading the eastWest file
	 * @param northSouth the scanner that is reading the northSouth file
	 */
	public void connectNetwork(Scanner eastWest, Scanner northSouth) {
		
		for(int i = 0; i < length; i++) {
			for(int j = 0; j < width; j++) {
				junctionArray[i][j].setEast(getBoolean(eastWest.nextInt()));
				junctionArray[i][j].setSouth(getBoolean(northSouth.nextInt()));
			}
		}
		
	}
	/**
	 * findPath searches the 2d array and if a junction is open, checks subsequent junctions
	 * for a path to the exit. It does this by adding potential junction openings to the queue
	 * and checking those junctions for more open junctions.
	 * @return a boolean value that represents if a path exists from enter and exit.
	 */
	public boolean findPath() {
		boolean isPath = false;
		Junction current;
		
		entry.setVisited(true);
		maxJunction = entry;
		pathfinder.add(entry);
		
		while(!(pathfinder.isEmpty())) {
			current = pathfinder.remove();
			
			if(current == exit) {
				isPath = true;
				break;
			}else {
				loadNeighbors(current);
				
				feedQueue(current);
				
			}
		}
		
		return isPath;
	}
	/**
	 * loadNeighbors is a private helper method that checks if the current Junction has a neighbor to the
	 * north, south east and west and sets the appropriate fields to those junctions from the 2d array.
	 * @param current the current Junction.
	 */ 
	private void loadNeighbors(Junction current) {
		
		if((current.row - 1) < 0) {
			toNorth = null;
		}else {
			toNorth = junctionArray[current.row -1][current.column];
		}
		if((current.row + 1) > (length - 1)) {
			toSouth = null;
		}else {
			toSouth = junctionArray[current.row + 1][current.column];
		}
		if((current.column + 1) > (width - 1)) {
			toEast = null;
		}else {
			toEast = junctionArray[current.row][current.column + 1];
		}
		if((current.column - 1) < 0) {
			toWest = null;
		}else {
			toWest = junctionArray[current.row][current.column - 1];
		}
	}
	/**
	 * Private helper method that checks if the neighbors have not been visited, are not null
	 * and have an open junction. Then, adds those to the queue and sets the back link to current.   
	 * @param current the current Junction being searched.
	 */
	private void feedQueue(Junction current) {
		if((toNorth != null && !toNorth.isVisited()) && toNorth.isSouth()) {
			toNorth.setVisited(true);
			toNorth.setBackLink(current);
			pathfinder.add(toNorth);
		}
		if(toEast != null && !toEast.isVisited() && current.isEast()) {
			toEast.setVisited(true);
			toEast.setBackLink(current);
			pathfinder.add(toEast);
			if(maxDistance < toEast.sum()) {
				maxDistance = toEast.sum();
				maxJunction = toEast;
			}
		}
		if(toSouth != null && !toSouth.isVisited() && current.isSouth()) {
			toSouth.setVisited(true);
			toSouth.setBackLink(current);
			pathfinder.add(toSouth);
			if(maxDistance < toSouth.sum()) {
				maxDistance = toSouth.sum();
				maxJunction = toSouth;
			}
		}
		if(toWest != null && !toWest.isVisited() && toWest.isEast()) {
			toWest.setVisited(true);
			toWest.setBackLink(current);
			pathfinder.add(toWest);
		}
		
		
	}
	/**
	 * Private helper method checks if the parameter reached is equal to the exit Junction. If so, 
	 * reverses the linked junction list and prints the contents of that list. If not, prints up to
	 * the furthest reached junction. 
	 * @param reached the final junction that was reached.
	 */
	private void printPath(Junction reached) {
		
		int count;
		if (reached == exit) {
			reached = Junction.reverse(reached);
			System.out.println("Found this path traversing the network:\n");
			count = 0;
			while(reached != null) {
				if(count > width) {
					System.out.println();
					count = 0;
				}
				System.out.print(reached + " ");
				reached = reached.getBackLink();
				count++;
			}
			
		}else {
			reached = Junction.reverse(reached);
			System.out.printf("Maximum distance from entry: %d\n", maxDistance);
			System.out.println("Maximum distance attained at junction " + maxJunction.toString());
			System.out.println("Path to Max:\n");
			count = 0;
			while(reached != null) {
				if(count > width) {
					System.out.println();
					count = 0;
				}
				System.out.print(reached + " ");
				reached = reached.getBackLink();
				count++;
			}
		}
	}
	
	/**
	 * Calls the printPath helper method based on whether the findPath method was successful method.
	 * @param succesful
	 */
	public void displayResult(boolean successful) {
		if(successful) {
			printPath(exit);
		}else {
			printPath(maxJunction);
		}
	}
	/**
	 * private helper method that takes an integer value and returns true or false
	 * based on the number provided.
	 * @param val an integer representing boolean value
	 * @return a boolean
	 */
	private boolean getBoolean(int val) {
		if(val == 0) {
			return true;
		}else {
			return false;
		}
	}
	
}
