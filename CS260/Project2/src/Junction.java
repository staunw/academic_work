/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS260
INSTRUCTOR:		Gyorgy Petruska
DUE DATE:		11/07/2018
 */
/**
 * A junciton represents a street intersection and if it has been visited and if it is open 
 * to the south and if it is open to the east. Also, maintains a link to the previous junction
 * visited.
 * @author Nick Stauffer
 *
 */
public class Junction {
	int row;
	int column;
	private boolean east;
	private boolean south;
	private boolean visited;
	private Junction backLink;
	/**
	 * Constructor. Sets up row and column fields representing where they are located. 
	 * @param row an integer value representing the location of the Junction vertically.
	 * @param column an integer value representing the location of the Junction horizontally.
	 */
	public Junction(int row, int column) {
		this.row = row;
		this.column = column;
	}
	/**
	 * sum calculates the sum of the row and column value of the Junction
	 * @return an integer value representing the sum of row and column
	 */
	public int sum(){
		return row + column;
	}
	/**
	 * Creates a string representing the row and column in the format "(row, column)"
	 * of the Junction.
	 */
	public String toString() {
	
		return "(" + Integer.toString(this.row) 
		+ ", " + Integer.toString(this.column) + ")";
	}
	/**
	 * Takes a Junction parameter that represents a linked list and reverses it.
	 * @param head
	 * @return A Junction value representing the head of the list containing the path that
	 * has been reached to the exit.
	 */
	public static Junction reverse(Junction head) {
		
		Junction prevJunc = null;
		Junction nextJunc;
		
		while(head != null) {
			nextJunc = head.getBackLink();
			
			head.setBackLink(prevJunc);
			
			prevJunc = head;
			head = nextJunc;
		}
		return prevJunc;
		
	}
	/**
	 * getter for east field
	 * @return the boolean value of east field.
	 */
	public boolean isEast() {
		return east;
	}
	/**
	 * setter for east field
	 * @param east a boolean value to set the east field. 
	 * True if Junction is open false if not
	 */
	public void setEast(boolean east) {
		this.east = east;
	}
	/**
	 * getter for field south
	 * @return a boolean value for south
	 */
	public boolean isSouth() {
		return south;
	}
	/**
	 * setter for field south
	 * @param south boolean value for south. 
	 * True if Junction is open false if not
	 */
	public void setSouth(boolean south) {
		this.south = south;
	}
	/**
	 * getter for field visited
	 * @return boolean value for visited
	 */
	public boolean isVisited() {
		return visited;
	}
	/**
	 * Setter for field visited
	 * @param visited a boolean value to set visited field
	 */
	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	/**
	 * getter for backLink field
	 * @return a Junction object representing the previous Junction visited
	 */
	public Junction getBackLink() {
		return backLink;
	}
	/**
	 * Setter for backLink field
	 * @param backLink a backLink representing a previous Junction
	 */
	public void setBackLink(Junction backLink) {
		this.backLink = backLink;
	}
}
