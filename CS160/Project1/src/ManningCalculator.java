/*
NAME:			Nick Stauffer
PROJECT:		Project 1
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LECTURE/Lab:	M/W 4:30 PM / W 6:00 PM
DUE DATE:		September 22, 2017
 */

//import java.text.DecimalFormat;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;
public class ManningCalculator{
	/*
	 * The purpose of the main method in the class ManningCalculator is to calculate the flow rate of a channel given the inputs 
	 * of the desired flow, the maximum depth of the channel, the width of the channel, the choice of channel construction, the
	 * change in channel height, and the change in channel length that are solicited from the user. At each input, if it is not 
	 * within the parameter the program will solicit a correct input. On the second try the program will terminate if the input 
	 * is still invalid. Once all inputs are gathered, The program will calculate if the given parameters are viable, if not the 
	 * program will terminate telling the user to try new data. If the data is viable, The program will solicit a depth from the
	 * user and decide if that depth will yield the desired flow within a one percent tolerance. The program will give the user 
	 * four attempts to find a depth that is within one percent and exit the program if it is found or not found. After each 
	 * attempt, The program will tell the user the difference in the calculated flow of the given height to the desired flow.
	 * After four attempts, if the depth is not within the tolerance, the program will tell the user the closet depth entered.
	 */
	public static void main(String[] args){
		DecimalFormat format = new DecimalFormat("####.00");	//Decimal format object to format 
		//Declare Constants
		final float MANNING_COEFFICIENT = 1.486F;				//The constant for Manning's coefficient 
		final float COEFFICIENT_BRICK = 0.015F;					//The constant value of the coefficient for brick
		final float COEFFICIENT_CONCRETE = 0.012F;				//The constant value of the coefficient for concrete 
		final float COEFFICIENT_ASPHALT = 0.016F;				//The constant value of the coefficient for asphalt
		final float COEFFICIENT_GRAVEL = 0.023F;				//The constant value of the coefficient for gravel
		final String MATERIAL_BRICK = "Brick";					//Constant name for brick
		final String MATERIAL_CONCRETE = "Concrete";			//Constant name for concrete
		final String MATERIAL_ASPHALT = "Asphalt";				//Constant name for Asphalt
		final String MATERIAL_GRAVEL = "Gravel";				//Constant name for Gravel
		final short MENU_CHOICE_BRICK = 1;						//Constant number for menu choice brick
		final short MENU_CHOICE_CONCRETE = 2;					//Constant number for menu choice concrete
		final short MENU_CHOICE_ASPHALT = 3;					//Constant number for menu choice asphalt
		final int MENU_CHOICE_GRAVEL = 4;						//Constant number for menu choice gravel
		final float TOLERANCE = 0.01F;							//Constant value for the tolerance desired
		final float POW = 2/3F;									//Constant value for the power that hydraulicRadius needs to be raised to 
		final short DEFAULT_ATTEMPT = 0;						
		
		//
		//Declare Local Variables
		//
		int attemptNumber;
		double desiredFlow;										//The desired flow
		double maxDepth;										//The maximum depth of the channel
		double width;											//The width of the channel
		double roughnessCoefficient;							//The stored location of the coefficient selected
		double deltaHeight;										//The change in channel height
		double deltaLength;										//The Change in channel length
		double enteredDepth;									//The depth the user wants to test
		double calculatedFlow;									//The calculation of flow given the entered depth
		double difference;										//The difference of the desired flow and the calculated flow
		double minError = 0;									//The value of the closest difference
		String resultMessage;									//Builds of the string to display the output to the user
		double toleranceMax;									//The value of Desired flow * (1 + Tolerance)
		double toleranceMin;									//The value of Desired flow * (1 - Tolerance)
		double bestGuess = 0;									//The value of the closest depth entered
		double velocity;										//Stores the velocity
		double crossSecArea;									//Stores the calculation of the cross sectional channel area
		double hydraulicRadius;									//Stores the calculation of the hydraulic radius
		double slope;											//Stores the calculation of the slope of the channel
		double wetPerim;										//The value of the wetted perimeter
		
		//
		//Greeting
		//
		JOptionPane.showMessageDialog(null, "Welcome to the Manning Equation Calculator!");
		//
		//User Input: Asks user for each input and checks if each input is valid exiting after two invalid inputs
		//
		desiredFlow =
				Double.parseDouble(JOptionPane.showInputDialog("Enter Desired Flow"));
				if (desiredFlow <= 0) {
					resultMessage = "Desired flow must be greater than zero";
					desiredFlow =
					Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
				}
					if (desiredFlow <= 0){
						resultMessage = "Desired flow not valid\nThe program terminates";
						JOptionPane.showMessageDialog(null, resultMessage);
						System.exit(0);
					}
					
		maxDepth =
				Double.parseDouble(JOptionPane.showInputDialog("Enter the Maximum Depth"));
				if (maxDepth <= 0) {
					resultMessage = "Maximum depth must be greater than zero";
					maxDepth =
					Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
				}
					if (maxDepth <= 0){ 
						resultMessage = "Maximum depth not valid\nThe program terminates";
						JOptionPane.showMessageDialog(null, resultMessage);
						System.exit(0);
					}
		

		width =
				Double.parseDouble(JOptionPane.showInputDialog("Enter the Width"));
		
				if (width <= 0){
					resultMessage = "width must be greater than zero";
					width =
					Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
				}
					if (width <= 0){ 
						resultMessage = "Width is not valid\nThe program terminates";
						JOptionPane.showMessageDialog(null, resultMessage);
						System.exit(0);
					}
		
		
		roughnessCoefficient =
				Double.parseDouble(JOptionPane.showInputDialog("Enter Choice for Roughness Coefficient: "
																+ "\n 1 " + MATERIAL_BRICK 
																+ "\n 2 " + MATERIAL_CONCRETE
																+ "\n 3 " + MATERIAL_ASPHALT
																+ "\n 4 " + MATERIAL_GRAVEL));
			
			if (roughnessCoefficient < MENU_CHOICE_BRICK | roughnessCoefficient > MENU_CHOICE_GRAVEL) {
				resultMessage = "Choice for roughness coefficient must be between " 
						      + MENU_CHOICE_BRICK + " & " + MENU_CHOICE_GRAVEL;
				roughnessCoefficient =
				Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
	
			}
				if (roughnessCoefficient < MENU_CHOICE_BRICK | roughnessCoefficient > MENU_CHOICE_GRAVEL){    		 
					resultMessage = "Choice not valid\nThe program terminates";
					JOptionPane.showMessageDialog(null, resultMessage);
				    System.exit(0);
				}
				
			else if (roughnessCoefficient == MENU_CHOICE_BRICK){
				roughnessCoefficient = COEFFICIENT_BRICK;
				}
				else if (roughnessCoefficient == MENU_CHOICE_CONCRETE){
						roughnessCoefficient = COEFFICIENT_CONCRETE;
				}
				else if (roughnessCoefficient == MENU_CHOICE_ASPHALT){
						roughnessCoefficient = COEFFICIENT_ASPHALT;
				}
				else if (roughnessCoefficient == MENU_CHOICE_GRAVEL){
						roughnessCoefficient = COEFFICIENT_GRAVEL;
				}	
		
		
		deltaHeight = 
				Double.parseDouble(JOptionPane.showInputDialog("Enter the Change in Channel Height"));
				if (deltaHeight <= 0){
					resultMessage = "Change in height must be greater than zero";
					deltaHeight =
					Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
				}
			    	if (deltaHeight <= 0){
				    	resultMessage = "Change in height is invalid\nThe program terminates";
				    	JOptionPane.showMessageDialog(null, resultMessage);
				    	System.exit(0);
			    	}
			    	
	   
		deltaLength =
				Double.parseDouble(JOptionPane.showInputDialog("Enter the Change in Channel Length"));
				if (deltaLength <= 0){
					resultMessage = "Change in Length must be greater than zero";
					deltaLength =
					Double.parseDouble(JOptionPane.showInputDialog(resultMessage));
				}
		    		if (deltaLength <= 0){
			    		resultMessage = "Change in Length is invalid\nThe program terminates";
			    		JOptionPane.showMessageDialog(null, resultMessage);
			    		System.exit(0);
		    		}	
		
		//
		//Calculation to determine if the desired flow can be met with the given input.
		//
		slope = deltaHeight / deltaLength;
		crossSecArea = maxDepth * width;
		wetPerim = maxDepth + maxDepth + width;
		hydraulicRadius = crossSecArea / wetPerim;
		velocity = (MANNING_COEFFICIENT / roughnessCoefficient) * Math.pow(hydraulicRadius, POW) * Math.sqrt(slope);
		calculatedFlow = velocity * crossSecArea;
		//
		//If the desired flow cannot be met, the program exits
		//
		if (calculatedFlow < desiredFlow) {
			resultMessage = "Desired flow cannont be met with this channel." 
							+ "\nPlease try again with different settings. "
							+ "\nThe program will terminate";
			JOptionPane.showMessageDialog(null, resultMessage);
			System.exit(0);
		}
		//
		//Attempt 1: Asks user for a depth, if the depth is not valid, sets value to the maximum depth then calculates flow
		//
		attemptNumber = DEFAULT_ATTEMPT;
		attemptNumber ++ ;
		enteredDepth =
					Double.parseDouble(JOptionPane.showInputDialog("Attempt " 
																+ attemptNumber 
																+ "\nEnter a Depth:"));
					if (enteredDepth <= 0 || enteredDepth > maxDepth) {
						resultMessage = "Entered depth is not valid. \nDesired depth is set to " + maxDepth;
						enteredDepth = maxDepth;
						JOptionPane.showMessageDialog(null, resultMessage);
					}
	
		
						crossSecArea = enteredDepth * width;
						wetPerim = enteredDepth + enteredDepth + width;
						hydraulicRadius = crossSecArea / wetPerim;
						velocity = (MANNING_COEFFICIENT / roughnessCoefficient) * Math.pow(hydraulicRadius, POW) * Math.sqrt(slope);
						calculatedFlow = velocity * crossSecArea;
						toleranceMax = (1 + TOLERANCE) * desiredFlow;
						toleranceMin = (1 - TOLERANCE) * desiredFlow;
						//
						//determines if the entered depth is a flow within the tolerance of the desired flow
						//
						if (calculatedFlow > toleranceMin & calculatedFlow < toleranceMax){
						resultMessage = "At a depth of " 
										+ format.format(enteredDepth) 
										+ " ft the flow is " 
										+ format.format(calculatedFlow) 
										+ " cubic ft per second.\nSuccess!\nThe program will terminate";;
										JOptionPane.showMessageDialog(null, resultMessage);
										System.exit(0);
						}
						//
						//Calculates the difference of desired flow, sets minError to the absolute value of the difference
						//
						else {
							 difference = calculatedFlow - desiredFlow;
							 minError = Math.abs(difference);
							 resultMessage = "At a depth of " 
									 		+ format.format(enteredDepth) 
									 		+ " ft the flow is " 
									 		+ format.format(calculatedFlow) 
									 		+ " cubic ft per second." 
									 		+ "\nDifference: " 
									 		+ format.format(difference);
							 JOptionPane.showMessageDialog(null, resultMessage);
							 bestGuess = enteredDepth;
						}		
		
		
		//Attempt 2: repeats the process of step 1
		attemptNumber ++ ;
		enteredDepth =
					Double.parseDouble(JOptionPane.showInputDialog("Attempt " 
																  + attemptNumber 
																  + "\nEnter a Depth:"));
		
					if (enteredDepth <= 0 || enteredDepth > maxDepth) {
						resultMessage = "Entered depth is not valid.\nDesired depth is set to " + maxDepth;
						enteredDepth = maxDepth;
						JOptionPane.showMessageDialog(null, resultMessage);
				    }
						 
						crossSecArea = enteredDepth * width;
						wetPerim = enteredDepth + enteredDepth + width;
						hydraulicRadius = crossSecArea / wetPerim;
						velocity = (MANNING_COEFFICIENT / roughnessCoefficient) * Math.pow(hydraulicRadius, POW) * Math.sqrt(slope);
						calculatedFlow = velocity * crossSecArea;
						toleranceMax = (1 + TOLERANCE) * desiredFlow;
						toleranceMin = (1 - TOLERANCE) * desiredFlow;
							
						if (calculatedFlow > toleranceMin & calculatedFlow < toleranceMax){
						resultMessage = "At a depth of " 
										+ format.format(enteredDepth) 
										+ " ft the flow is " 
										+ format.format(calculatedFlow) 
										+ " cubic ft per second.\nSuccess!\nThe program will terminate";;
										JOptionPane.showMessageDialog(null, resultMessage);
										System.exit(0);
						}
							
						else {
						difference = calculatedFlow - desiredFlow;
						minError = Math.min(minError, Math.abs(difference));
						resultMessage = "At a depth of " 
										+ format.format(enteredDepth) 
										+ " ft the flow is " 
										+ format.format(calculatedFlow) 
										+ " cubic ft per second." 
										+ "\nDifference: " 
										+ format.format(difference);
						JOptionPane.showMessageDialog(null, resultMessage);
							//
							//Compares minError to the absolute value of difference. If these match, sets bestGuess to the enteredDepth
							//
							if (minError == Math.abs(difference)) {
							bestGuess = enteredDepth;
							}
									
						}
		//Attempt 3: repeat of previous steps
		attemptNumber ++ ;
		enteredDepth =
					Double.parseDouble(JOptionPane.showInputDialog("Attempt " + attemptNumber + "\nEnter a Depth:"));
					if (enteredDepth <= 0 || enteredDepth > maxDepth) {
						resultMessage = "Entered depth is not valid. \nDesired depth is set to " + maxDepth;
						enteredDepth = maxDepth;
						JOptionPane.showMessageDialog(null, resultMessage);
					}
						 
					crossSecArea = enteredDepth * width;
					wetPerim = enteredDepth + enteredDepth + width;
					hydraulicRadius = crossSecArea / wetPerim;
					velocity = (MANNING_COEFFICIENT / roughnessCoefficient) * Math.pow(hydraulicRadius, POW) * Math.sqrt(slope);
					calculatedFlow = velocity * crossSecArea;
					toleranceMax = (1 + TOLERANCE) * desiredFlow;
					toleranceMin = (1 - TOLERANCE) * desiredFlow;
				
				
						if (calculatedFlow > toleranceMin & calculatedFlow < toleranceMax){
							resultMessage = "At a depth of " 
										+ format.format(enteredDepth) 
										+ " ft the flow is " 
										+ format.format(calculatedFlow) 
										+ " cubic ft per second.\nThe program will terminate";
							JOptionPane.showMessageDialog(null, resultMessage);
							System.exit(0);
						}
							
						else {
						difference = calculatedFlow - desiredFlow;
						minError = Math.min(minError, Math.abs(difference));
						resultMessage = "At a depth of " 
										+ format.format(enteredDepth) 
										+ " ft the flow is " 
										+ format.format(calculatedFlow) 
										+ " cubic ft per second." 
										+ "\nDifference: " 
										+ format.format(difference);
						JOptionPane.showMessageDialog(null, resultMessage);
						if (minError == Math.abs(difference)) {
							bestGuess = enteredDepth;
						}
						
						}
		
		//Attempt 4: repeat of previous steps
		attemptNumber ++ ;
		enteredDepth =
					Double.parseDouble(JOptionPane.showInputDialog("Attempt " + attemptNumber + "\nEnter a Depth:"));
					if (enteredDepth <= 0 || enteredDepth > maxDepth) {
						resultMessage = "Entered depth is not valid. \nDesired depth is set to " 
										+ maxDepth;
						enteredDepth = maxDepth;
						JOptionPane.showMessageDialog(null, resultMessage);
						}
					 
					crossSecArea = enteredDepth * width;
					wetPerim = enteredDepth + enteredDepth + width;
					hydraulicRadius = crossSecArea / wetPerim;
					velocity = (MANNING_COEFFICIENT / roughnessCoefficient) * Math.pow(hydraulicRadius, POW) * Math.sqrt(slope);
					calculatedFlow = velocity * crossSecArea;
					toleranceMax = (1 + TOLERANCE) * desiredFlow;
					toleranceMin = (1 - TOLERANCE) * desiredFlow;
				
				
								if (calculatedFlow > toleranceMin & calculatedFlow < toleranceMax){
									resultMessage = "At a depth of " 
													+ format.format(enteredDepth) 
													+ " ft the flow is " 
													+ format.format(calculatedFlow) 
													+ " cubic ft per second.\nSuccess!\nThe program will terminate";
									JOptionPane.showMessageDialog(null, resultMessage);
									System.exit(0);
									}
										
								else {
									difference = calculatedFlow - desiredFlow;
									minError = Math.min(minError, Math.abs(difference));
									resultMessage = "At a depth of " 
													+ format.format(enteredDepth) 
													+ " ft the flow is " 
													+ format.format(calculatedFlow) 
													+ " cubic ft per second."
													+ "\nDifference: " 
													+ format.format(difference);
									JOptionPane.showMessageDialog(null, resultMessage);
										if (minError == Math.abs(difference)) {
											bestGuess = enteredDepth;
										}
																	
								}
		//
		//Displays final message if the depth was not found after four attempts
		//
		resultMessage = "Your closest guess was " 
						+ format.format(bestGuess) 
						+ " ft with a difference of " 
						+ format.format(minError) 
						+ " cubic feet per second";
		JOptionPane.showMessageDialog(null, resultMessage);
	System.exit(0);
	} //end main
}// End Class ManningCalculator
/*My Test Case
 * Enter Desired Flow
 * 8800
 * Enter Max Depth
 * 24
 * Enter Width
 * 48
 * Enter Choice for roughness coefficient:
 * 3
 * Enter the change in channel height
 * 16
 * Enter the change in channel length
 * 30000
 * Attempt: 1
 * Enter a depth
 * 24
 * At a depth of 24.00 ft the flow is 12951.05 cubic ft per second.
 * Difference: 4151.05
 * Attempt 2
 * Enter a depth
 * At a depth of 16.00 ft the flow is 7440.58 cubic ft per second.
 * Difference: -1359.42
 * Attempt 3
 * Enter a Depth:
 * 20
 * At a depth of 20.00 ft the flow is 10128.11 cubic ft per second.
 * Difference: 1328.11
 * Attempt 4
 * Enter a Depth:
 * 18
 * At a depth of 18.00 ft the flow is 8764.66 cubic ft per second.
 * Success!
 * The program terminates
 */ 
