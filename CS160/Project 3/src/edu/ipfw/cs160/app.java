/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		November 16, 2017
 */
//IMPORTANT Do not modify any of this file

package edu.ipfw.cs160;

import java.io.FileNotFoundException;

public class app {


	public static void main(String[] args) throws FileNotFoundException {
		frmMain frame = new frmMain();
		frame.setVisible(true);
	}//end main method

}//end class
