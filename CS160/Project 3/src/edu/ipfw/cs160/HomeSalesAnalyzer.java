/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		November 16, 2017
 */
package edu.ipfw.cs160;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;
import java.io.*;
/**
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 * @version .5
 * The HomeSalesAnalyzer class reads a data file with the format of zip code home price separated by a comma on
 * a single line. The class then takes the data and stores it into an array for each of three unique zip codes.
 * The class contains methods for creating a copy of the zip codes array, finding the average home sales price for 
 * each zip code and all zip codes, calculating the max and min sales price for a single zip code and all zip 
 * codes, and calculating the total number of homes sold for a single zip code and all zip codes. Finally, the
 * class contains a method for printing out a report of all the above information.
 */
public class HomeSalesAnalyzer {
	
	private String[] zipCodes;
	private double[] values1;
	private	double[] values2;
	private double[] values3;
	/**HomeSalesAnalyzer constructor takes a filename parameter for a File object. The constructor reads the file
	 * and creates an array of all the tokens in the file. Then, the constructor takes the tokens and places all
	 * unique zip code tokens and places those into an array. From there, The constructor reads the array and counts
	 * how many instances of each unique zip code there are and then instantiates three arrays, one for each unique
	 * zip code, to store the home values of each of those zip codes. Finally, the constructor fills the values arrays
	 * with the appropriate values associated with each zip code.
	 * @param filename
	 * @throws FileNotFoundException
	 */
	public HomeSalesAnalyzer(File filename) throws FileNotFoundException {
		final int MAX_ZIP_CODES = 3;			//total number of unique zip codes in the file
		String reader = "";						//variable to hold the tokens read from the file
		Scanner inFile = new Scanner(filename);	//scanner object to read the file
		zipCodes = new String[MAX_ZIP_CODES];	//instantiates zipCodes array with the max number of zip codes
		//
		//read the file and accumulate everything into a string 
		//
		while (inFile.hasNext()) {
			reader += inFile.next() + ",";
			}
		String[] tokens = reader.split(",");	//split tokens and store into an array
		
		inFile.close(); //close file reader
		
		zipCodes[0] = tokens[0];				//stores the value located in tokens index 1 into zipCodes array
		//
		//fill zipCodes array by going through the tokens array to find unique zip codes
		//
		for(int t = 0, z = 1; z < MAX_ZIP_CODES; t += 2) {
			if (!tokens[t].equals(zipCodes[z - 1])) {
				zipCodes[z] = tokens[t];
				z++;
			}
		}// end fill zipCodes array
		
		Arrays.sort(zipCodes); // sort zipCodes array
		//
		//find counts of each zip code. Accomplishes by looping through tokens array and adding to the count of
		//associated zip code. The index for tokens increases by two because even indexes are zip codes and odd
		//are home sales prices.
		//
		int zipCode1Count = 0;	
		int zipCode2Count = 0;
		int zipCode3Count = 0;
		for (int t = 0; t < tokens.length; t += 2) {
			if (tokens[t].equals(zipCodes[0])) {
				zipCode1Count ++;
			}	
			else if (tokens[t].equals(zipCodes[1])) {
				zipCode2Count ++;
			}
			else if (tokens[t].equals(zipCodes[2])) {
				zipCode3Count ++;
			}
		}//end find zip code count
		//
		//instantiate values arrays
		//
		values1 = new double[zipCode1Count];
		values2 = new double[zipCode2Count];
		values3 = new double[zipCode3Count];
		//
		//Fill values1 array. Accomplishes by checking if the tokens at the specified index is equal to the index value
		//of values1 array. Increases tokens array, if it is equal to the zipCodes value, once before filling values1 
		//array to get to the home price and then again after filling the values1 array to get to the next zip code index.
		//If the index for tokens does not match the desired zip code, the tokens index is increased by two and the 
		//values index count is reduced by 1 so the index is not skipped.
		//
		for (int i = 0, t = 0, z = 0; i < values1.length; i++) {
			if (tokens[t].equals(zipCodes[z])) {
				t++;
				values1[i] = Double.parseDouble(tokens[t]);
				t++;
			}else {
				t += 2;
				i --; 
			}
		}//end fill values1 array
		//
		//Fill values2 array like values1 array was filled.
		//
		for (int i = 0, t = 0, z = 1; i < values2.length; i++) {
			if (tokens[t].equals(zipCodes[z])) {
				t++;
				values2[i] = Double.parseDouble(tokens[t]);
				t++;
			}
			else {
				t += 2;
				i --; 
			}
		}//end fill values2 array
		//
		//Fill values3 array like values1 & 2 arrays.
		//
		for (int i = 0, t = 0, z = 2; i < values3.length; i++) {
			if (tokens[t].equals(zipCodes[z])) {
				t++;
				values3[i] = Double.parseDouble(tokens[t]);
				t++;
			}
			else {
				t += 2;
				i --; 
			}
		}//end fill values3 array
		tokens = null;								//set the tokens array to null
		
	}//End HomeSalesAnalyzer constructor
	/**getZipCodes method gets copy of zipCodes array.
	 * @return returns a deep copy of the zipCodes array.
	 */
	public String[] getZipCodes() {
		String[] copy = new String[zipCodes.length];
				
		for (int i = 0; i < zipCodes.length; i++) {
			copy[i] = zipCodes[i];
		}
		return copy;
	
	}//End getZipCodes
	/**calculateAverage method calculates the average for a specific zip code.
	 * @param zipcode a string parameter of the desired zip code.
	 * @return returns the average value calculated.
	 */
	public double calculateAverage(String zipcode) {
		double average = 0;
		if (zipcode.equals(zipCodes[0])) {
			double sum = 0;
			for (int index = 0; index < values1.length; index ++) {
				sum += values1[index];
				average = sum / values1.length;
			}
		}
		if (zipcode.equals(zipCodes[1])) {
			double sum = 0;
			for (int index = 0; index < values2.length; index ++) {
				sum += values2[index];
				average = sum / values2.length;
			}
		}
		if (zipcode.equals(zipCodes[2])) {
			double sum = 0;
			for (int index = 0; index < values3.length; index ++) {
				sum += values3[index];
				average = sum / values3.length;
			}
		}
		return average;
	}
	/**calculateAverage method calculates the average for all zip codes.
	 * @return returns the average of all zip codes.
	 */
	public double calculateAverage() {
		double sum1 = 0;
		double sum2 = 0;
		double sum3 = 0;
		for (int i = 0; i < values1.length; i++) {
			sum1 += values1[i];
		}
		for (int i = 0; i < values2.length; i++) {
			sum2 += values2[i];
		}
		for (int i = 0; i < values3.length; i++) {
			sum3 += values3[i];
		}
		
		return ((sum1 + sum2 + sum3) / (values1.length + values2.length + values3.length));
	}// End calculateAverage for all zipcodes
	/**calculateMax method calculates the maximum sales price for a specified zip code.
	 * @param zipcode a string parameter of the desired zip code.
	 * @return returns the maximum home sales price for the specified zip code.
	 */
	public double calculateMax(String zipcode) {
		double maxHomeSale = Double.MIN_VALUE;
		if (zipcode.equals(zipCodes[0])) {
			for (int index = 0; index < values1.length; index ++) {
				if (values1[index] > maxHomeSale) {
					maxHomeSale = values1[index];
				}
			}
		}
		if (zipcode.equals(zipCodes[1])) {
			for (int index = 0; index < values2.length; index ++) {
				if (values2[index] > maxHomeSale) {
					maxHomeSale = values2[index];
				}
			}
		}
		if (zipcode.equals(zipCodes[2])) {
			for (int index = 0; index < values3.length; index ++) {
				if (values3[index] > maxHomeSale) {
					maxHomeSale = values3[index];
				}
			}	
		}
		return maxHomeSale;
	}//End calculateMax for single zipcode
	/**calculateMax method calculates the maximum sales price of a home for all zip codes.
	 * @return returns the maximum value found for all zip codes.
	 */
	public double calculateMax() {
		double maxHomeSale = Double.MIN_VALUE;
		for (int i = 0; i < values1.length; i++) {
			if (values1[i] > maxHomeSale) {
				maxHomeSale = values1[i];
			}
		}
		for (int i = 0; i < values2.length; i++) {
			if (values2[i] > maxHomeSale) {
				maxHomeSale = values2[i];
			}
		}
		for (int i = 0; i < values3.length; i++) {
			if (values3[i] > maxHomeSale) {
				maxHomeSale = values3[i];
			}
		}
		
		return maxHomeSale;
	}//end calculateMax for all zipcodes
	/**calculateMin method calculates the minimum sale price for a home in a specific zip code.
	 * @param zipcode a string parameter for the desired zip code.
	 * @return returns the minimum sale price for a home in a specific zip code.
	 */
	public double calculateMin(String zipcode) {
		double minHomeSale = Double.MAX_VALUE;
		if (zipcode.equals(zipCodes[0])) {
			for (int index = 0; index < values1.length; index ++) {
				if (values1[index] < minHomeSale) {
					minHomeSale = values1[index];
				}
			}
		}
		if (zipcode.equals(zipCodes[1])) {
			for (int index = 0; index < values2.length; index ++) {
				if (values2[index] < minHomeSale) {
					minHomeSale = values2[index];
				}
			}
		}
		if (zipcode.equals(zipCodes[2])) {
			for (int index = 0; index < values3.length; index ++) {
				if (values3[index] < minHomeSale) {
					minHomeSale = values3[index];
				}
			}	
		}
		return minHomeSale;
	}//End calculateMin for single zipcode
	/**calculateMin method calculates the minimum sale price for a home in all zip codes.
	 * @return returns the minimum sale price for a home in all zip codes.
	 */
	public double calculateMin() {
		double minHomeSale = Double.MAX_VALUE;
		for (int i = 0; i < values1.length; i++) {
			if (values1[i] < minHomeSale) {
				minHomeSale = values1[i];
			}
		}
		for (int i = 0; i < values2.length; i++) {
			if (values2[i] < minHomeSale) {
				minHomeSale = values2[i];
			}
		}
		for (int i = 0; i < values3.length; i++) {
			if (values3[i] < minHomeSale) {
				minHomeSale = values3[i];
			}
		}
		
		return minHomeSale;
	}//end calculateMin for all zipcodes
	/**calculateNumberSold method finds the total number of homes sold for a specific zip code.
	 * @param zipcode a string value of the desired zip code.
	 * @return returns the total number of homes for the desired zip code.
	 */
	public int calculateNumberSold(String zipcode) {
		int numberSold = 0;
		if (zipcode.equals(zipCodes[0])) {
			numberSold = values1.length;
		}
		if (zipcode.equals(zipCodes[1])) {
			numberSold = values2.length;
		}
		if (zipcode.equals(zipCodes[2])) {
			numberSold = values3.length;
		}
		return numberSold;
	}//End calculateNumberSold for a single zip code
	/**calculateNumberSold method calculates the total number of homes sold for all zip codes.
	 * @return
	 */
	public int calculateNumberSold() {
		return values1.length + values2.length + values3.length;
	}//end calculateNumberSold for all zip codes
	/**the createReport method uses the previous methods to and writes them to a file with a user specified 
	 * file name.
	 * @param filename parameter of a File object of a user specified file name.
	 * @throws FileNotFoundException
	 */
	public void createReport(File filename) throws FileNotFoundException {
		String fileName = filename + ".txt";
		DecimalFormat format = new DecimalFormat("$###,###.00");
		PrintWriter outputFile = new PrintWriter(fileName);
		
		for (int i = 0; i < zipCodes.length; i++) {
			outputFile.println("Zip Code " + zipCodes[i]);
			outputFile.println("--------------");
			outputFile.println("Number Sold: " + (calculateNumberSold(zipCodes[i]))); 
			outputFile.println("Average sales price: " + format.format(calculateAverage(zipCodes[i])));
			outputFile.println("Range: " + format.format(calculateMin(zipCodes[i])) 
								+ " - "	+ format.format(calculateMax(zipCodes[i])));
			outputFile.println();
			outputFile.println();
		}
		outputFile.println("All Zip Codes");
		outputFile.println("--------------");
		outputFile.println("Number Sold: " + (calculateNumberSold())); 
		outputFile.println("Average sales price: " + format.format(calculateAverage()));
		outputFile.println("Range: " + format.format(calculateMin()) 
							+ " - "	+ format.format(calculateMax()));
		
		outputFile.close();
	}//End createReport method
}//End HomeSalesAnalyzer class
