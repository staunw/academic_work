/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		November 16, 2017
 */
package edu.ipfw.cs160;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;

public class frmMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 487907442935562553L;
	private JPanel contentPane;
	private JTextField lblResult;
	private JRadioButton radAverage;
	private JRadioButton radRange;
	private JRadioButton radNumberSold;
	private JRadioButton radAllZipcodes;
	private JRadioButton radSingleZipcode;
	private JButton btnProcess;
	private JComboBox<String> cboZipcodes;
	
	private DecimalFormat formatter = new DecimalFormat("$###,###.00");
	
	//TODO Uncomment the following statement after creating the class
	private HomeSalesAnalyzer analyzer;
	private JMenuBar menuBar;
	private JMenuItem mntmCreateReport;

	
	/**
	 * Create the frame.
	 * @throws FileNotFoundException 
	 */
	public frmMain() throws FileNotFoundException {
			
		setTitle("Real Estate Analyzer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 477, 277);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmLoadFile = new JMenuItem("Load File");
		mntmLoadFile.addActionListener(new LoadFileListener());
		mnFile.add(mntmLoadFile);
		
		JMenuItem mntmClose = new JMenuItem("Close");
		mntmClose.addActionListener(new CloseListener());
		
		mntmCreateReport = new JMenuItem("Create Report");
		mntmCreateReport.addActionListener(new ReportListener());
		mntmCreateReport.setEnabled(false);
		mnFile.add(mntmCreateReport);
		mnFile.add(mntmClose);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(10, 11, 121, 91);
		contentPane.add(panel);
		panel.setLayout(null);
		
		radAverage = new JRadioButton("Average");
		radAverage.setEnabled(false);
		radAverage.setBounds(6, 7, 79, 23);
		panel.add(radAverage);
		radAverage.setSelected(true);
		
		radRange = new JRadioButton("Range");
		radRange.setEnabled(false);
		radRange.setBounds(6, 33, 67, 23);
		panel.add(radRange);
		
		radNumberSold = new JRadioButton("Number Sold");
		radNumberSold.setEnabled(false);
		radNumberSold.setBounds(6, 59, 109, 23);
		panel.add(radNumberSold);
		
		ButtonGroup choices1 = new ButtonGroup();
		choices1.add(radAverage);
		choices1.add(radRange);
		choices1.add(radNumberSold);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setBounds(162, 11, 142, 64);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		radAllZipcodes = new JRadioButton("All Zip Codes");
		radAllZipcodes.setEnabled(false);
		radAllZipcodes.setSelected(true);
		radAllZipcodes.addItemListener(new RadioCheckedChanged());
		radAllZipcodes.setBounds(7, 7, 103, 23);
		panel_1.add(radAllZipcodes);
		
		radSingleZipcode = new JRadioButton("Single Zip Code");
		radSingleZipcode.setEnabled(false);
		radSingleZipcode.addItemListener(new RadioCheckedChanged());
		radSingleZipcode.setBounds(7, 33, 117, 23);
		panel_1.add(radSingleZipcode);
		
		ButtonGroup choices2 = new ButtonGroup();
		choices2.add(radAllZipcodes);
		choices2.add(radSingleZipcode);
		
		JLabel lblEnterZipcode = new JLabel("Select Zip Code");
		lblEnterZipcode.setBounds(331, 11, 103, 14);
		contentPane.add(lblEnterZipcode);
		
		btnProcess = new JButton("Process");
		btnProcess.setEnabled(false);
		btnProcess.addActionListener(new ProcessListener());
		btnProcess.setBounds(191, 117, 89, 23);
		contentPane.add(btnProcess);
		
		lblResult = new JTextField();
		lblResult.setHorizontalAlignment(SwingConstants.CENTER);
		lblResult.setEditable(false);
		lblResult.setBounds(57, 174, 357, 20);
		contentPane.add(lblResult);
		lblResult.setColumns(10);
		
		cboZipcodes = new JComboBox<String>();
		cboZipcodes.setEnabled(false);
		cboZipcodes.setBounds(331, 36, 89, 20);
		contentPane.add(cboZipcodes);
		
		JOptionPane.showMessageDialog(null, "Welcome to the Real Estate Analyzer.\n " +
											"Select data file.");
		loadFile();
	}//end constructor
	
	
	private class RadioCheckedChanged implements ItemListener{

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED && radSingleZipcode.isSelected()) {
				cboZipcodes.setEnabled(true);
				cboZipcodes.requestFocus();
				cboZipcodes.setSelectedIndex(0);
			} else {
				cboZipcodes.setSelectedIndex(-1);
				cboZipcodes.setEnabled(false);
			}			
		}//end itemStateChanged method
	}//end RadioCheckedChanged class
	
	private class ProcessListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			//TODO Complete blanks as shown in directions
			if (analyzer == null) {
				JOptionPane.showMessageDialog(null, "No data loaded.");
			}
			if (radAllZipcodes.isSelected()) {
				if (radAverage.isSelected()) {
					double average = analyzer.calculateAverage();
					lblResult.setText("Average for all zip codes is " + formatter.format(average));
				}else if (radRange.isSelected()) {
					double min = analyzer.calculateMin();
					double max = analyzer.calculateMax();
					lblResult.setText("Range for all zip codes is " + formatter.format(min) + " - " + formatter.format(max));
				}else {
					int numSold = analyzer.calculateNumberSold();
					lblResult.setText("Number sold for all zip codes is " + numSold);
				}
			}else {
				String zipcode = cboZipcodes.getSelectedItem().toString();
				if (radAverage.isSelected()) {
					double average = analyzer.calculateAverage(zipcode);
					lblResult.setText("Average for " + zipcode + " is " + formatter.format(average));
				}else if (radRange.isSelected()) {
					double min = analyzer.calculateMin(zipcode);
					double max = analyzer.calculateMax(zipcode);
					lblResult.setText("Range for " + zipcode + " is " + formatter.format(min) + " - " + formatter.format(max));
				}else {
					int numSold = analyzer.calculateNumberSold(zipcode);
					lblResult.setText("Number sold for " + zipcode + " is " + numSold);
				}
			}
		}//end actionPerformed method
	}//end ProcessListener class
	
	private class CloseListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);		
		}//end actionPerformed method
		
	}//end CloseListener
	
	private class LoadFileListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			loadFile();			
		}//end actionPerformed method
		
	}//end LoadFileListener
	
	private void loadFile() {
		JFileChooser fileChooser = new JFileChooser();		
		fileChooser.setDialogTitle("Select data file");
		fileChooser.setAcceptAllFileFilterUsed(false);
		FileFilter filter = new FileNameExtensionFilter("Real Estate Analysis File", "rea");
		fileChooser.setFileFilter(filter);
		
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileChooser.showOpenDialog(getParent());
		
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			try {
				cboZipcodes.removeAllItems();
			
				//TODO Complete blanks as shown in directions
				//Initialize analyzer
				analyzer = new HomeSalesAnalyzer(selectedFile);
				
				for (String s: analyzer.getZipCodes()) {
					cboZipcodes.addItem(s);
				}
					
				cboZipcodes.setSelectedIndex(-1);
				
				radAverage.setEnabled(true);
				radRange.setEnabled(true);
				radNumberSold.setEnabled(true);
				mntmCreateReport.setEnabled(true);
				radAllZipcodes.setEnabled(true);
				radSingleZipcode.setEnabled(true);
				btnProcess.setEnabled(true);
				lblResult.setText("");
				
				//TODO remove the following after blanks are completed above
				
				
				
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null, "Error opening file");
			}
		} else {
			radAverage.setEnabled(false);
			radRange.setEnabled(false);
			radNumberSold.setEnabled(false);
			radAllZipcodes.setEnabled(false);
			radSingleZipcode.setEnabled(false);
			mntmCreateReport.setEnabled(false);
			cboZipcodes.setEnabled(false);
			btnProcess.setEnabled(false);
			lblResult.setText("");
		}
	}//end LoadFile method
	
	private class ReportListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fileChooser = new JFileChooser();		
			fileChooser.setDialogTitle("Specify a file to save");
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
			
			fileChooser.setAcceptAllFileFilterUsed(false);
			FileFilter filter = new FileNameExtensionFilter("Text File", "txt");
			fileChooser.setFileFilter(filter);
			
			int result = fileChooser.showOpenDialog(getParent());
			
			if (result == JFileChooser.APPROVE_OPTION) {
				File fileToSave = fileChooser.getSelectedFile();
				
				//TODO Complete blanks as shown in directions
				try {
					analyzer.createReport(fileToSave);//Ask about exception
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Error saving file");
				}
			}
			
		}//end actionPerformed
		
	}//end ReportListener
	
}//end frmMain class