/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
/**
 * Represents a question. A question has a question, an answer, a category and an Id number. 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 * @version .1
 */
public class Question {
	private String question;
	private String answer;
	private String category;
	private int idNumber;
	/**
	 * Question constructor that takes a category, a question and an answer and sets the the corresponding fields.
	 * @param category A string representing the category type for the question.
	 * @param question A string representing the question to be asked.
	 * @param answer A string representing the answer to the question asked.
	 */
	public Question(String category, String question, String answer) {
		this.question = question;
		this.answer = answer;
		this.category = category;
	}//end constructor
	/**
	 * Question constructor that takes parameters for idNumber, category, question, and answer and sets the corresponding fields.
	 * @param idNumber An integer that represents the id number of the question.
	 * @param category A string that represents the category type of the question.
	 * @param question A string that represents the question to be asked. 
	 * @param answer A string that represents the answer to the question asked.
	 */
	public Question(int idNumber, String category, String question, String answer) {
		this.idNumber = idNumber;
		this.question = question;
		this.answer = answer;
		this.category = category;
	}//end constructor
	/**
	 * Constructor that returns a deep copy of the question parameter.
	 * @param copyQuestion a Question that is to be copied.
	 */
	public Question(Question copyQuestion) {
		this.question = copyQuestion.getQuestion();
		this.answer = copyQuestion.getAnswer();
		this.category = copyQuestion.getCategory();
		this.idNumber = copyQuestion.getIdNumber();
	}//end constructor
	/**
	 * Constructor that takes a Question object and idNumber parameter.
	 * @param question A Question object that sets the category, question and answer fields 
	 * @param idNumber An integer value that sets the idNumber of the question.
	 */
	public Question(Question question, int idNumber) {
		this.question = question.getQuestion();
		this.answer = question.getAnswer();
		this.category = question.getCategory();
		this.idNumber = idNumber;
	}//end constructor
	/**
	 * Gets the idNumber of the question.
	 * @return An integer representing the idNumber of the question.
	 */
	public int getIdNumber() {
		return idNumber;
	}//end getIdNumber
	/**
	 * Gets the question of the Question object
	 * @return A string representing the question that is asked.
	 */
	public String getQuestion() {
		return question;
	}//end getQuestion
	/**
	 * Gets the answer of the Question object.
	 * @return A string representing the answer to the question asked.
	 */
	public String getAnswer() {
		return answer;
	}//end getAnswer
	/**
	 * Gets the category of the Question object.
	 * @return A string representing the category type of the question being asked.
	 */
	public String getCategory(){
		return category;
	}//end getCategory
	/**
	 * Sets the question that is to be asked.
	 * @param question A string containing the question.
	 */
	public void setQuestion(String question) {
		this.question = question;
	}//end setQuestion
	/**
	 * Sets the answer to the question to be asked.
	 * @param answer A string containing the answer to the question.;
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}//end setAnswer
	/**
	 * Checks if the answer equals that was typed in by the user is equal to the answer of the question asked ignoring the case.
	 * @param answer A String representing the answer that was typed in.
	 * @return A boolean representing if the answer was correct or incorrect.
	 */
	public boolean isAnswer(String answer) {
		return this.answer.trim().equalsIgnoreCase(answer);
	}//end isAnswer
	/**
	 * Method for displaying the fields of category and question in a string.
	 * @return A string representing the fields of Category and Question in the form Category: (contents of category field)
	 * Question: (contents of question field).
	 */
	public String toString() {
		return "Category: " + this.category + " Question: " + this.question;
	}//End toString
	/**
	 * Method for displaying all fields in a string.
	 * @return A string representing all Question fields in the form (contents of idNumber field) Category: (contents of category field)
	 * Question: (contents of question field) Answer: (contents of answer field).
	 */
	public String toStringLong() {
		return this.idNumber + " Category: " + this.category + " Question: " + this.question + " Answer: " + this.answer;
	}
	/**
	 * A method to determine if an object equals that of a Question object.
	 * @ return A boolean representing if the object equals that of a Question object.
	 */
	public boolean equals(Object object){
		if ((object != null) && (getClass() == object.getClass())){
	    	if (this.idNumber == ((Question) object).getIdNumber()) {
	        return false;
			}
			if ((this.question.equals(((Question) object).getQuestion())) && (this.answer.equals(((Question) object).getAnswer()))) {
	        return true;
			}
	    }
	    object = null;
	    return false;
 	}
	
}//End Question class
