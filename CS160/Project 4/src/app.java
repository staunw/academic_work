/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;
/**
 * The app class contains the main method used to play the trivia game. The class sets up the loop structure for the menus of the game.
 * The three main options are for Administrator, playing the game and exiting the program. If the Administrator option is selected, 
 * and new Administrator object is created and the user is taken to the menu for Administrator options where the user can perform 
 * administrator tasks to the game. If the play game option is selected, a new game object is created and the user can begin to play.
 * Game play consists of being asked a question then the level of confidence in your answer. The answer is checked and the confidence
 * value is added to the player's score. After 10 questions, a half time question is asked where 5 points will be added for a correct
 * answer. Another 10 questions will be asked with the same rules being applied as the first half. After the completion of the second
 * half, A final question will be asked and a confidence of 1-15 will be allowed to be selected. If the player's answer is correct, the
 * confidence level will be added to the score, if incorrect, the confidence level will be subtracted from the final score.
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 * @version .1
 */
public class app {

	public static void main(String[] args) throws IOException, FileNotFoundException{		

		final int MENU_CHOICE_ADMIN = 1;
		final int MENU_CHOICE_GAME = 2;
		final int MENU_CHOICE_EXIT = 3;
		
		boolean keepRunning = true;
		
		do {		
			boolean validMenuChoice;
			int menuChoice = -1;
			do {
				validMenuChoice = true;
				String choice = (JOptionPane.showInputDialog("Main Menu\n" + 
						"1. Administrator Options\n" + 
						"2. Play Game\n" + 
						"3. Exit"));	
				
				if (choice == null) {
					System.exit(0);
				}
				
				try {
					menuChoice = Integer.parseInt(choice);
				}catch (Exception ex) {
					validMenuChoice = false;
				}
			} while (!validMenuChoice);
			
		//////////////////////////////////////////////////////////////////////////////////////
		//User selected Administrator
		//////////////////////////////////////////////////////////////////////////////////////			
			if (menuChoice == MENU_CHOICE_ADMIN) {
				
			
				Administrator admin = new Administrator();
				
				final int MENU_CHOICE_VIEW_QUESTIONS = 1;		final int MENU_CHOICE_VIEW_HALFTIME = 2;
				final int MENU_CHOICE_VIEW_FINAL = 3;			final int MENU_CHOICE_EDIT_QUESTION = 4;
				final int MENU_CHOICE_EDIT_HALFTIME = 5;		final int MENU_CHOICE_EDIT_FINAL = 6;
				final int MENU_CHOICE_DELETE_QUESTION = 7;		final int MENU_CHOICE_DELETE_HALFTIME = 8;
				final int MENU_CHOICE_DELETE_FINAL = 9;			final int MENU_CHOICE_ADD_QUESTION = 10;
				final int MENU_CHOICE_ADD_HALFTIME = 11;		final int MENU_CHOICE_ADD_FINAL = 12;
				final int MENU_CHOICE_MAIN_MENU = 13;			
				
				boolean runAdminMenu = true;
				
				do {
					boolean validChoice;
					
					do {
						validChoice = true;
						
						String choice = (JOptionPane.showInputDialog("Administrator Options\n" +
								"1. View questions\n" + 			"2. View halftime questions\n"  +
								"3. View final questions\n" +		"4. Edit question\n" +
								"5. Edit halftime question\n" +		"6. Edit final question\n" +
								"7. Delete question\n" +			"8. Delete halftime question\n" +
								"9. Delete final question\n" +		"10. Add question\n" +
								"11. Add halftime question\n" +		"12. Add final question\n" +
								"13. Return to Main Menu"));
						
						
						if (choice == null) {
							System.exit(0);
						}
						
						try {
							menuChoice = Integer.parseInt(choice);
						} catch (Exception ex) {
							validChoice = false;
						}
						
					} while (!validChoice);
						
					if (menuChoice == MENU_CHOICE_VIEW_QUESTIONS) {						
						String result = "Questions:\n";
						for (Question q : admin.getQuestions(Constants.QuestionType.QUESTION)) {
							result += q.toStringLong() + "\n";
						}
						JOptionPane.showMessageDialog(null, result);
						
					} else if (menuChoice == MENU_CHOICE_VIEW_HALFTIME) {
						
						String result = "Halftime:\n";
						for (Question q : admin.getQuestions(Constants.QuestionType.HALFTIME)) {
							result += q.toStringLong() + "\n";
						}
						JOptionPane.showMessageDialog(null, result);
						
					} else if (menuChoice == MENU_CHOICE_VIEW_FINAL) {
						
						String result = "Final:\n";
						for (Question q : admin.getQuestions(Constants.QuestionType.FINAL)) {
							result += q.toStringLong() + "\n";
						}
						JOptionPane.showMessageDialog(null, result);
						
					} else if (menuChoice == MENU_CHOICE_EDIT_QUESTION) {
						String question;
						String answer;
						int idNumber = -1;
						boolean validInput;
						
						do {
							validInput = true;							
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);
						
					
						Question selectedQuestion = admin.getQuestion(Constants.QuestionType.QUESTION, idNumber);
						if (selectedQuestion == null) {
							JOptionPane.showMessageDialog(null, "No question with that id number", "Error", JOptionPane.ERROR_MESSAGE);
						}else {
							question = JOptionPane.showInputDialog("Edit the question:", selectedQuestion.getQuestion());
							answer = JOptionPane.showInputDialog("Edit the answer:", selectedQuestion.getAnswer());
							try {
								if (admin.editQuestion(Constants.QuestionType.QUESTION, idNumber, question, answer)) {
									JOptionPane.showMessageDialog(null, "Question edited successfully");
								}else {
									JOptionPane.showMessageDialog(null, "Error editing question");
								}
							}catch(Exception ex) {
								JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
							}
						}
						

					} else if (menuChoice == MENU_CHOICE_EDIT_HALFTIME) {
						String question;
						String answer;
						int idNumber = -1;

						boolean validInput;
						
						do {
							validInput = true;	
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);
						
					
						Question selectedQuestion = admin.getQuestion(Constants.QuestionType.HALFTIME, idNumber);
						if (selectedQuestion == null) {
							JOptionPane.showMessageDialog(null, "No question with that id number", "Error", JOptionPane.ERROR_MESSAGE);
						}else {
							question = JOptionPane.showInputDialog("Edit the question:", selectedQuestion.getQuestion());
							answer = JOptionPane.showInputDialog("Edit the answer:", selectedQuestion.getAnswer());
							try {
								if (admin.editQuestion(Constants.QuestionType.HALFTIME, idNumber, question, answer)) {
									JOptionPane.showMessageDialog(null, "Question edited successfully");
								}else {
									JOptionPane.showMessageDialog(null, "Error editing question");
								}
							}catch(Exception ex) {
								JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
							}
						}
						
						
					} else if (menuChoice == MENU_CHOICE_EDIT_FINAL) {
						String question;
						String answer;
						int idNumber = -1;

						boolean validInput;
						
						do {
							validInput = true;							
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);

						
						Question selectedQuestion = admin.getQuestion(Constants.QuestionType.FINAL, idNumber);
						if (selectedQuestion == null) {
							JOptionPane.showMessageDialog(null, "No question with that id number", "Error", JOptionPane.ERROR_MESSAGE);
						}else {
							question = JOptionPane.showInputDialog("Edit the question:", selectedQuestion.getQuestion());
							answer = JOptionPane.showInputDialog("Edit the answer:", selectedQuestion.getAnswer());
							try {
								if (admin.editQuestion(Constants.QuestionType.FINAL, idNumber, question, answer)) {
									JOptionPane.showMessageDialog(null, "Question edited successfully");
								}else {
									JOptionPane.showMessageDialog(null, "Error editing question");
								}
							}catch(Exception ex) {
								JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
							}
						}
						
						
					} else if (menuChoice == MENU_CHOICE_DELETE_QUESTION) {
						int idNumber = -1;
						boolean validInput;
						
						do {
							validInput = true;					
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);
						
						if(admin.deleteQuestion(Constants.QuestionType.QUESTION, idNumber)) {
							JOptionPane.showMessageDialog(null, "Question deleted successfully");
						}else {
							JOptionPane.showMessageDialog(null, "No question with id number: " + idNumber);
						}
						
						
						
					}  else if (menuChoice == MENU_CHOICE_DELETE_HALFTIME) {
						int idNumber = -1;
						boolean validInput;
						
						do {
							validInput = true;				
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);
						
						if(admin.deleteQuestion(Constants.QuestionType.HALFTIME, idNumber)) {
							JOptionPane.showMessageDialog(null, "Question deleted successfully");
						}else {
							JOptionPane.showMessageDialog(null, "No question with id number: " + idNumber);
						}
						
						
					} else if (menuChoice == MENU_CHOICE_DELETE_FINAL) {
						int idNumber = -1;
						boolean validInput;
						
						do {
							validInput = true;			
							String input = (JOptionPane.showInputDialog("Enter the id number:"));
							
							try {
								idNumber = Integer.parseInt(input);
							}catch (Exception ex) {
								validInput = false;
							}							
						} while (!validInput);
						
						if(admin.deleteQuestion(Constants.QuestionType.FINAL, idNumber)) {
							JOptionPane.showMessageDialog(null, "Question deleted successfully");
						}else {
							JOptionPane.showMessageDialog(null, "No question with id number: " + idNumber);
						}
						
						
					} else if (menuChoice == MENU_CHOICE_ADD_QUESTION) {
						
						String question = JOptionPane.showInputDialog("Enter the question:");
						String answer = JOptionPane.showInputDialog("Enter the answer:");
						String category = JOptionPane.showInputDialog("Enter the category:");
						
						Question q = new Question(category, question, answer);
						
						try {
							if (admin.addQuestion(Constants.QuestionType.QUESTION, q)) {
								JOptionPane.showMessageDialog(null, "Question added successfully");
							}
							else {
								JOptionPane.showMessageDialog(null, "Cannot add duplicate question");
							}
								
						}catch(Exception ex) {
							JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
						}
						
						
					} else if (menuChoice == MENU_CHOICE_ADD_HALFTIME) {
						String question = JOptionPane.showInputDialog("Enter the question:");
						String answer = JOptionPane.showInputDialog("Enter the answer:");
						String category = JOptionPane.showInputDialog("Enter the category:");
						
						Question q = new Question(category, question, answer);
						
						try {
							if (admin.addQuestion(Constants.QuestionType.HALFTIME, q)) {
								JOptionPane.showMessageDialog(null, "Question added successfully");
							}
							else {
								JOptionPane.showMessageDialog(null, "Cannot add duplicate question");
							}
								
						}catch(Exception ex) {
							JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
						}
						
						
					} else if (menuChoice == MENU_CHOICE_ADD_FINAL) {
						String question = JOptionPane.showInputDialog("Enter the question:");
						String answer = JOptionPane.showInputDialog("Enter the answer:");
						String category = JOptionPane.showInputDialog("Enter the category:");
						
						Question q = new Question(category, question, answer);
						
						try {
							if (admin.addQuestion(Constants.QuestionType.FINAL, q)) {
								JOptionPane.showMessageDialog(null, "Question added successfully");
							}
							else {
								JOptionPane.showMessageDialog(null, "Cannot add duplicate question");
							}
								
						}catch(Exception ex) {
							JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
						}
						
						
					} else if (menuChoice == MENU_CHOICE_MAIN_MENU){
						
						if (admin.isDataChanged()) {
							admin.saveData();
						}
						
						
						runAdminMenu = false;

					} else {
						JOptionPane.showMessageDialog(null, "That is not a valid choice. Try again!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} while (runAdminMenu);
				
		//////////////////////////////////////////////////////////////////////////////////////
		//User selected Game
		//////////////////////////////////////////////////////////////////////////////////////

			} else if (menuChoice == MENU_CHOICE_GAME) {
				
				Game game = new Game();
				
				String answer;
				String confidence;
				String message;
				for (int i = 0; i < 10; i++) {
					message = "Score: " + game.getScore() + "\nFirst Half, Question " + (i + 1) + "\n" 
							+ game.getQuestion(i);
					answer = JOptionPane.showInputDialog(message + "\nEnter your answer: ");
					
					message = "Confidences: " + game.getConfidences() + "\nEnter your confidence:";
					confidence = JOptionPane.showInputDialog(message);
					
					int score = Integer.parseInt(confidence);
					message = "The Correct Answer is: " + game.getAnswer(i) + "\n";
					if (game.scoreQuestion(i, answer, score)) {
						message = message + "Your answer is correct!";
					}else {
						message = message + "Your answer is incorrect";
					}
					JOptionPane.showMessageDialog(null, message);
					
				}
				
				
				message = "Half Time\n" + game.getQuestion(Game.HALFTIME_QUESTION);
				answer = JOptionPane.showInputDialog(message + "\nEnter your answer: ");
				message = "The Correct Answer is: " + game.getAnswer(20) + "\n";
				if (game.scoreHalftimeQuestion(answer)) {
					message = message + "Your answer is correct!";
				}else {
					message = message + "Your answer is incorrect";
				}
				JOptionPane.showMessageDialog(null, message);
				
				
				game.setConfidences(Game.REGULAR_CONFIDENCE);
				for (int i = Game.NUMBER_OF_QUESTIONS - 10; i < Game.NUMBER_OF_QUESTIONS; i++) {
					message = "Score: " + game.getScore() + "\nSecond Half, Question " + (i - 9) + "\n" 
							+ game.getQuestion(i);
					answer = JOptionPane.showInputDialog(message + "\nEnter your answer: ");
					
					message = "Confidences: " + game.getConfidences() + "\nEnter your confidence:";
					confidence = JOptionPane.showInputDialog(message);
					int score = Integer.parseInt(confidence);
					message = "The Correct Answer is: " + game.getAnswer(i) + "\n";
					if (game.scoreQuestion(i, answer, score)) {
						message = message + "Your answer is correct!";
					}else {
						message = message + "Your answer is incorrect";
					}
					JOptionPane.showMessageDialog(null, message);
					
				}

			
				game.setConfidences(Game.FINAL_CONFIDENCE);
				message = "Final Question\n" + game.getQuestion(Game.FINAL_QUESTION);
				answer = JOptionPane.showInputDialog(message + "\nEnter your answer: ");
				
				message = "Confidences: " + game.getConfidences() + "\nEnter your confidence:";
				confidence = JOptionPane.showInputDialog(message);
		
				int score = Integer.parseInt(confidence);
				message = "The Correct Answer is: " + game.getAnswer(Game.FINAL_QUESTION) + "\n";
				if (game.scoreFinalQuestion(answer, score)) {
					message = message + "Your answer is correct!";
				}else {
					message = message + "Your answer is incorrect";
				}
				JOptionPane.showMessageDialog(null, message);
				
				JOptionPane.showMessageDialog(null, "Final Score: " + game.getScore());
				
				
				
				
			} else if (menuChoice == MENU_CHOICE_EXIT) {
				keepRunning = false;
				JOptionPane.showMessageDialog(null, "Good Bye!", "Exit", JOptionPane.CLOSED_OPTION);
			} else {
				JOptionPane.showMessageDialog(null, "That is not a valid choice. Try again!", "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		} while (keepRunning);
	
	}//end main method
	
}//end class