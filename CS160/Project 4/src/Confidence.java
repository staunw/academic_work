/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
/**
 * Represents the level of confidence of the player on his/her answer to the question asked. Is used for tabulating the score for the 
 * game.
 * @author Nick Stauffer
 * @author www.staunw01@students.ipfw.edu
 * version .1
 *
 */
public class Confidence {
	
	private int value;
	private boolean used = false;
	/**
	 * Represents an integer value of the confidence
	 * @param value the level of confidence of the player 
	 */
	public Confidence(int value) {
		this.value = value;
	}
	/**
	 * Gets the value of the confidence
	 * @return an integer representing the value of the confidence.
	 */
	public int getValue() {
		return value;
	}
	/**
	 * Gets the value of isUsed
	 * @return A boolean representing the value of isUsed, true if it is used, false if it isn't used.
	 */
	public boolean isUsed() {
		return used;
	}
	/**
	 * Sets the value of isUsed
	 * @param used sets value to true when confidence is used.
	 */
	public void setUsed(boolean used) {
		this.used = true;
	}
}//End confidence method
