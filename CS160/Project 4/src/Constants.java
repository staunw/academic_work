/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
/**
 * Constant values for common variable names. String constants for the three question type files, questions.dat, halftime_questions.dat,
 * final_questions.dat that the Game constructor and Administrator constructor reads from to set up the the questions in an array.
 * enums for type of questions that exist in the game, QUESTION, HALFTIME, FINAL.
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 * @version .1
 */
public class Constants {
	public static final String FILENAME_QUESTIONS = "questions.dat";
	public static final String FILENAME_HALFTIME_QUESTIONS = "halftime_questions.dat";
	public static final String FILENAME_FINAL_QUESTIONS = "final_questions.dat";
	public static enum QuestionType{QUESTION, HALFTIME, FINAL}
	
	private Constants() {
	}
}//End class constants
