/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
/**
 * The administrator class represents the tasks that can be performed by the administrator. The administrator class starts by reading
 * each of the three question files and creates question objects that are then stored in array lists. The tasks that the administrator
 * may perform include: viewing all questions by question type(regular, halftime, final), adding a new question, deleting an existing
 * question, and modifying an existing question. The administrator class keeps track of data that has been changed and will then save
 * any modifications to the appropriate questions file.
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 * @version .1
 */

public class Administrator {
	
	private int nextIdNumber;
	private ArrayList<Question> regularQuestions;
	private ArrayList<Question> halftimeQuestions;
	private ArrayList<Question> finalQuestions;
	private boolean dataChanged = false;
	/**
	 * The Administrator constructor reads each file and calls the loadQuestions method to create ArrayLists and Question objects for 
	 * each question in the file. Then, the constructor loops through each ArrayList finding the IdNumber of each question and adding 1.
	 * Finally, the constructor sets nextIdNumber to the final idNumber + 1 so this number may be used in writing a new question.
	 * @throws FileNotFoundException 
	 */
	public Administrator() throws FileNotFoundException {
		nextIdNumber = 0;
		regularQuestions = loadQuestions(Constants.FILENAME_QUESTIONS);
		halftimeQuestions = loadQuestions(Constants.FILENAME_HALFTIME_QUESTIONS);
		finalQuestions = loadQuestions(Constants.FILENAME_FINAL_QUESTIONS);
		for (Question q : regularQuestions) {
			this.nextIdNumber = q.getIdNumber() + 1;
		}
		for (Question q : halftimeQuestions) {
			this.nextIdNumber = q.getIdNumber() + 1;
		}
		for (Question q: finalQuestions) {
			this.nextIdNumber = q.getIdNumber() + 1;
		}
		
	}//End Administrator constructor
	/**
	 * loadQuestions reads the file that was passed as the parameter, loops through the file and adds a Question object to the ArrayList
	 * and returns the created ArrayList. 
	 * @param questions A string representing the filename to be read.
	 * @return an ArrayList of Question objects created from the file.
	 * @throws FileNotFoundException
	 */
	private ArrayList<Question> loadQuestions(String questions) throws FileNotFoundException{
		ArrayList<Question> questionsArrayList = new ArrayList<Question>();
		File file = new File(questions);
		Scanner fileReader = new Scanner(file);
		while (fileReader.hasNext()) {
			String entireline = fileReader.nextLine();
			
			String[] tokens = entireline.split(",");
			int idNumber = Integer.parseInt(tokens[0]);
			String category = tokens[1];
			Object question = tokens[2];
			String answer = tokens[3];
			
			 questionsArrayList.add(new Question(idNumber, category, (String)question, answer));
			}
		fileReader.close();
		
		return questionsArrayList;
	}//End loadQuestions
	/**
	 * isDataChanged method is an accessor method that returns the value of dataChanged field.
	 * @return A boolean that represents if the data has been modified.
	 */
	public boolean isDataChanged() {
		return this.dataChanged;
	}//End isDataChanged
	/**
	 * The saveData method writes the contents of each ArrayList field to the corresponding file by calling the writeQuestions method.
	 * @throws IOException
	 */
	public void saveData() throws IOException {
		writeQuestions(Constants.FILENAME_QUESTIONS, this.regularQuestions);
		writeQuestions(Constants.FILENAME_HALFTIME_QUESTIONS, this.halftimeQuestions);
		writeQuestions(Constants.FILENAME_FINAL_QUESTIONS, this.finalQuestions);
	}//End saveData
	/**
	 * The writeQuestions method loops through the ArrayList that was passed into the parameter and writes the contents to the file that
	 * was passed as a parameter.
	 * @param question A string representing the file to save the data to.
	 * @param questionsList An ArrayList to be searched and written to the file.
	 * @throws IOException
	 */
	private void writeQuestions(String question, ArrayList<Question> questionsList) throws IOException {
		PrintWriter outputFile = new PrintWriter(question);
		for (Question q : questionsList) {
			outputFile.print(q.getIdNumber() + "," 
							 + q.getCategory() + ","
							 + q.getQuestion() + ","
							 + q.getAnswer());
			outputFile.println();
		}
		outputFile.close();
	}//End writeQuestions
	/**
	 * The getQuestion method uses the question type passed as the parameter and the idNumber to be searched passed in the parameter
	 * and creates a deep copy that is returned to the administrator.
	 * @param question A QuestionType (stored as an enum in Constants class)that represents 1 of the 3 types of questions the administrator is looking for. 
	 * @param idNumber An integer value of the desired idNumber the Administrator is looking for.
	 * @return A deep copy of the desired Question object.
	 */
	public Question getQuestion(Constants.QuestionType question, int idNumber) {
		ArrayList<Question> questions;
		Question copyQuestion = null;
		if (question.equals(Constants.QuestionType.QUESTION)) {
			questions = regularQuestions;
		}
		else if(question.equals(Constants.QuestionType.HALFTIME)) {
			questions = halftimeQuestions;
		}
		else {
			questions = finalQuestions;
		}
		for (Question q : questions) {
			if (q.getIdNumber() == idNumber) {
				copyQuestion = new Question(q, idNumber);
			}
		}
		return copyQuestion;
	}//End getQuestion
	/**
	 * The getQuestions method takes the QuestionType parameter and fills a new ArrayList with the questions from the desired QuestionType.
	 * @param question An enumerated QuestionType used to determine which ArrayList to search.
	 * @return A deep copy of an ArrayList of Question objects.
	 */
	public ArrayList<Question> getQuestions(Constants.QuestionType question){
		ArrayList<Question> questions;
		if (question.equals(Constants.QuestionType.QUESTION)) {
			questions = regularQuestions;
		}
		else if(question.equals(Constants.QuestionType.HALFTIME)) {
			questions = halftimeQuestions;
		}
		else if(question.equals(Constants.QuestionType.FINAL)){
			questions = finalQuestions;
		}else {
			throw new IllegalArgumentException("Type not valid");
		}
		ArrayList<Question> newQuestion = new ArrayList<Question>();
		for (Question q : questions) {
			Question copyQuestion = new Question(q);
			newQuestion.add(copyQuestion);
		}
		return newQuestion;
	}//End getQuestions
	/**
	 * The editQuestion method uses the editQuestion parameter to set the Questions Arraylist that needs to be searched. Then the method
	 * loops through the specified ArrayList to find the question(using the idNumber parameter) that needs to be changed. Once the 
	 * desired question is found, the question is updated with the inQuestion and answer parameters. isUpdated is set to true. The 
	 * method also validates the question and answer to check if they contain commas and throws illegalArgumentException if the data is
	 * not correct.  
	 * @param editQuestion An enumerated QuestionType for the desired ArrayList that needs to be searched.
	 * @param idNumber An integer value of the specific idNumber of the question that needs to be retrieved.
	 * @param inQuestion A string value of the updated question the Administrator has entered.
	 * @param answer A string value of the updated answer the Administrator has entered.
	 * @return a boolean value to note that the data has been altered.
	 */
	public boolean editQuestion(Constants.QuestionType editQuestion, int idNumber, String inQuestion, String answer) {
		ArrayList<Question> editQuestions;
		if (inQuestion.contains(",")) {
			throw new IllegalArgumentException("Question contains comma");
		}
		if (answer.contains(",")) {
			throw new IllegalArgumentException("Answer contains comma");
		}
		boolean isUpdated = false;
		if (editQuestion.equals(Constants.QuestionType.QUESTION)) {
			editQuestions = regularQuestions;
		}
		else if (editQuestion.equals(Constants.QuestionType.HALFTIME)) {
			editQuestions = halftimeQuestions;
		}
		else if (editQuestion.equals(Constants.QuestionType.FINAL)) {
			editQuestions = finalQuestions;
		}
		else {
			throw new IllegalArgumentException("Invalid Type");
		}
		for (Question eq : editQuestions) {
			if (idNumber == eq.getIdNumber()) {
				eq.setQuestion(inQuestion);
				eq.setAnswer(answer);
				isUpdated = true;
				break;
			}
		}
		dataChanged = true;
		return isUpdated;
	}//End editQuestions
	/**
	 * the deleteQuesiton method takes the QuestionType parameter and idNumber parameter to determine which ArrayList to search that is
	 * holding the question with the specified idNumber. Once the question is found it is removed from the ArrayList. dataChanged is set
	 * to true to reflect that changes to the data have been made.
	 * @param editQuestion An enumerated QuestionType used to set the ArrayList that needs to be searched. 
	 * @param idNumber An integer value representing the idNumber of the question that is to be deleted.
	 * @return A boolean value to note that the question has been successfully deleted.
	 */
	public boolean deleteQuestion(Constants.QuestionType editQuestion, int idNumber) {
		ArrayList<Question> deleteQuestions;
		boolean isDeleted = false;
		if (editQuestion.equals(Constants.QuestionType.QUESTION)) {
			deleteQuestions = regularQuestions;
		}
		else if (editQuestion.equals(Constants.QuestionType.HALFTIME)) {
			deleteQuestions = halftimeQuestions;
		}
		else if (editQuestion.equals(Constants.QuestionType.FINAL)) {
			deleteQuestions = finalQuestions;
		}
		else {
			throw new IllegalArgumentException("Invalid Type");
		}
		for (int i = 0; i < deleteQuestions.size(); i++) {
			if (deleteQuestions.get(i).getIdNumber() == idNumber) {
				deleteQuestions.remove(i);
				isDeleted = true;
				dataChanged = true;
				break;
			}
		}
		
		return isDeleted;
	}//End deleteQuestion
	/**
	 * The addQuestion method takes the QuestionType parameter and sets the appropriate ArrayList to be searched. The questionToAdd
	 * parameter is validated to make sure it contains no commas in any of the Question object fields. Then the Question is checked 
	 * to see if it is already in the ArrayList. Once the new Question passes all validation, it is added to the appropriate Arraylist
	 * with the next available idNumber (nextIdNumber +1) that was calculated in the constructor. DataChanged is set to true.  
	 * @param addQuestion enumerated QuestionType of the appropriate ArrayList to be searched.
	 * @param questionToAdd A Question object created from the information entered by the Administrator. Represents the new question to add
	 * @return A boolean value to determine if the question was successfully added.
	 */
	public boolean addQuestion(Constants.QuestionType addQuestion, Question questionToAdd) {
		if (questionToAdd.getQuestion().contains(",")) {
			throw new IllegalArgumentException("Question contains comma");
		}
		if (questionToAdd.getAnswer().contains(",")) {
			throw new IllegalArgumentException("Answer contains comma");
		}
		if (questionToAdd.getCategory().contains(",")) {
			throw new IllegalArgumentException("Category contains comma");
		}
		boolean isAdded = false;
		ArrayList<Question> newQuestion;
		if (addQuestion.equals(Constants.QuestionType.QUESTION)) {
			newQuestion = this.regularQuestions;
		}
		else if (addQuestion.equals(Constants.QuestionType.HALFTIME)) {
			newQuestion = this.halftimeQuestions;
		}
		else if (addQuestion.equals(Constants.QuestionType.FINAL)) {
			newQuestion = this.finalQuestions;
		}
		else {
			throw new IllegalArgumentException("Invalid Type");
		}
		if (newQuestion.contains(questionToAdd)) {
			isAdded = false;
		}
		else {
			questionToAdd = new Question(questionToAdd, nextIdNumber);
			newQuestion.add(questionToAdd);
			nextIdNumber += 1;
			dataChanged = true;
			isAdded = true;
		}
		return isAdded;
	}//End addQuestion
}//End Administrator class
