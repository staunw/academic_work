/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 4
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		December 7, 2017
 */
/**
 * Represents the game. The game class sets up the game by reading the three questions files: questions.dat, halftime_questions.dat, 
 * and final_questions.dat. After reading the questions.dat file, Game sets up 20 random regular questions and places those into an 
 * array from index 0-19. Game then reads the halftime_questions.dat file and places a random question from that file into array index 20. 
 * Finally, Game reads the final_questions.dat file and places a random question into array index 21. Game will then set up an array 
 * for the confidence level of the player, from 1-10 for regular questions and 1-15 for the final question that is set after each round
 * with the setConfidence method. Game will then check if the answer the user provided matches the actual answer to the question and
 * scores the game accordingly. The score is tabulated using the confidence level the player selected.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game {
	//
	//fields
	//
	private final int MIN_CONFIDENCE = 1;
	public static final int REGULAR_CONFIDENCE = 10;
	public static final int FINAL_CONFIDENCE = 15;
	private final int HALFTIME_QUESTION_VALUE = 5;
	public static final int NUMBER_OF_QUESTIONS = 20;
	public static final int HALFTIME_QUESTION = 20;
	public static final int FINAL_QUESTION = 21;
	private Question[] questions = new Question[NUMBER_OF_QUESTIONS + 2];
	private Confidence[] confidences;
	private int score;
	/**
	 * Constructor for the Game class. Reads each file individually and sets up an array of random questions. Sets the confidence level
	 * to the REGULAR_CONFIDENCE level. 
	 * @throws FileNotFoundException
	 */
	public Game()throws FileNotFoundException{
		File inFile = new File(Constants.FILENAME_QUESTIONS);
		Scanner fileReader = new Scanner(inFile);
		int count = 0;
		while (fileReader.hasNext()) {
			count++;
			fileReader.nextLine();
		}
		fileReader.close();
		List<Integer> range = IntStream.range(0, count).boxed().collect(Collectors.toCollection(ArrayList::new));
		Collections.shuffle(range);
		range.subList(0, NUMBER_OF_QUESTIONS);
		
		int index = 0;
		int idNumber = 0;
		String category = "";
		String question = "";
		String answer = "";
		fileReader = new Scanner(inFile);
		Question[] fileQuestions = new Question[count];
		while (fileReader.hasNext()) {
			String entireline = fileReader.nextLine();
			
			String[] tokens = entireline.split(",");
			idNumber = Integer.parseInt(tokens[0]);
			category = tokens[1];
			question = tokens[2];
			answer   = tokens[3];
			
			fileQuestions[index] = new Question(idNumber, category, question, answer);
			index++;
		}
		fileReader.close();
		int questionsIndex = 0;
		
		for(int i: range) {
			if (questionsIndex > NUMBER_OF_QUESTIONS - 1) {
				break;
			}
			questions[questionsIndex] = fileQuestions[i];
			questionsIndex++;
		}
		//fileQuestions = null;
		
		inFile = new File(Constants.FILENAME_HALFTIME_QUESTIONS);
		fileReader = new Scanner(inFile);
		int halfQuestionCount = 0;
	    while (fileReader.hasNext()){
	    	halfQuestionCount++;
	    	fileReader.nextLine();
	    }
	    fileReader.close();
	    
	    //Random randomQuestion;
	    int randomNumber = new Random().nextInt(halfQuestionCount) + 1;
	    inFile = new File(Constants.FILENAME_HALFTIME_QUESTIONS);
	    fileReader = new Scanner(inFile);
	    for (int n = 1; fileReader.hasNext(); n++){
	    	String entireline = fileReader.nextLine();
			
			String[] tokens = entireline.split(",");
			idNumber = Integer.parseInt(tokens[0]);
			category = tokens[1];
			question = tokens[2];
			answer   = tokens[3];
			if (n == randomNumber) {
				questions[HALFTIME_QUESTION] = new Question(idNumber, category, question, answer);
				break;
			}
			
	    }
	    fileReader.close();
		
	    inFile = new File(Constants.FILENAME_FINAL_QUESTIONS);
		fileReader = new Scanner(inFile);
		int finalQuestionCount = 0;
	    while (fileReader.hasNext()){
	    	finalQuestionCount++;
	    	fileReader.nextLine();
	    }
	    fileReader.close();
	    
	    randomNumber = new Random().nextInt(finalQuestionCount) + 1;
	    inFile = new File(Constants.FILENAME_FINAL_QUESTIONS);
	    fileReader = new Scanner(inFile);
	    for (int n = 1; fileReader.hasNext(); n++){
	    	String entireline = fileReader.nextLine();
			
			String[] tokens = entireline.split(",");
			idNumber = Integer.parseInt(tokens[0]);
			category = tokens[1];
			question = tokens[2];
			answer   = tokens[3];
			if (n == randomNumber) {
				questions[FINAL_QUESTION] = new Question(idNumber, category, question, answer);
				break;
			}
			
	    }
	    fileReader.close();
		setConfidences(REGULAR_CONFIDENCE);
	}
	/**
	 * Sets the confidences with the parameter value.
	 * @param confidences An integer value that represents the maximum value of confidence objects.
	 */
	public void setConfidences(int confidences) {
		this.confidences = new Confidence[confidences]; 
		for (int i = MIN_CONFIDENCE; i <= confidences; i++) {
			this.confidences[i - 1] = new Confidence(i);
		}
	}
	/**
	 * Gets the confidences from the confidence array and returns their value. If the confidence has been used, it will not display the 
	 * value.
	 * @return A string representing the confidence values.
	 */
	public String getConfidences() {
		String confidences = "";
		for (Confidence c : this.confidences) {
			if (c.isUsed() != true) {
			confidences += c.getValue() + " ";
			}
		}
		return confidences.trim();
	}
	/**
	 * Gets the question with the array index that was passed into the question parameter.
	 * @param question Integer value from a loop in the main method that represents the question to retrieve. 
	 * @return A string representing the question that needs to be retrieved.
	 */
	public String getQuestion(int question) {
		return questions[question].toString();
	}
	/**
	 * Gets the answer with the array index that was passed into the answer parameter.
	 * @param Answer An integer value from a loop in the main method that represents the answer to retrieve.
	 * @return A string representing the answer that needs to be retrieved.
	 */
	public String getAnswer(int Answer) {
		return this.questions[Answer].getAnswer();
	}
	/**
	 * Calculates the score based on whether the question was answered correctly. 
	 * @param question an integer value representing the index of the question that was answered.
	 * @param answer a string representing the answer that the user entered.
	 * @param score an integer value representing the confidence level the user entered.
	 * @return Boolean value representing if the question was answered correctly or incorrectly. If the answer is correct, sets 
	 * local boolean bool to true and updates the score.
	 */
	public boolean scoreQuestion(int question,String answer, int score) {
		boolean bool = false;
		if (this.questions[question].getAnswer().equalsIgnoreCase(answer)) {
			this.score += score;
			bool = true;
		}
		for (Confidence c : confidences) {
			if (c.getValue() == score) {
				c.setUsed(true);
				
				break;
			}
		}
		return bool;
	}
	/**
	 * Calculates and updates the score based on the result of the halftime question. 
	 * @param answer string value representing the answer the user typed in.
	 * @return a boolean value representing if the user's answer was correct or incorrect. If correct, returns true and updates the score.
	 * If false, returns false and no change is made to the score.
	 */
	public boolean scoreHalftimeQuestion(String answer) {
		if (this.questions[HALFTIME_QUESTION].getAnswer().equalsIgnoreCase(answer)) {
			this.score += HALFTIME_QUESTION_VALUE;
			return true;
		}else {
			return false;
		}
	}
	/**
	 * calculates the total score based on whether the the user correctly answered the question.
	 * @param answer A string representing the user's answer.
	 * @param score An integer value representing the value of the confidence the user entered.
	 * @return a boolean representing whether the user answered the question correctly. If true, the score is updated based on the score
	 * parameter. If false, the score is negatively impacted based on score parameter. 
	 */
	public boolean scoreFinalQuestion(String answer, int score) {
		boolean bool = false;
		if (this.questions[FINAL_QUESTION].getAnswer().equalsIgnoreCase(answer)) {
			this.score += score;
			bool = true;
		}
		else {
			this.score -= score;
			bool = false;
		}
		return bool;
	}
	public int getScore() {
		return this.score;
	}
}// End Game
