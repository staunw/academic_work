/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS160
INSTRUCTOR:		Matthew Parker
LAB TIME:		W 6:00 PM
DUE DATE:		October 29, 2017
 */
//
//imports
//
import java.util.Scanner;
import java.text.DecimalFormat;
import java.io.*;

public class HomeSalesAnalysisCalculator {
	final static String FILE = "data.dat";						//global constant for file data.dat
	final static String ZIPCODE46815 = "46815";					//global constant for first zip code
	final static String ZIPCODE46825 = "46825";					//global constant for second zip code
	final static String ZIPCODE46835 = "46835";					//global constant for third zip code
	/*
	 * The purpose of the main method in the class HomeSalesAnalysisCalculator is to work in conjunction with other methods to 
	 * read data from a file and to display the calculations of home sales to the user for various zip codes. The main method 
	 * accomplishes this by: printing a menu and soliciting user input. The menu options are: calculate the average home sales 
	 * price for a specific zip code, calculate the range for a specific zip code, calculate the number of homes sold for a 
	 * zip code, calculate the average for all zip codes, calculate the range for all zip codes, calculate the number of homes 
	 * sold for all zip codes, create a report, and exit the program. The menu options accomplish their tasks by calling the 
	 * appropriate method outside of the main method. When the user enters a number for the menu, the number is validated. 
	 * If the number is outside of the menu range, the program will ask for valid input. Once a valid option is entered, the 
	 * program will perform the appropriate task. For options that require a zip code to be entered, the user will be prompted 
	 * for a supported zip code and the zip code will be validated before the associated calculation method is called. For 
	 * create report option, the user will be prompted for a string value of the name of the report they would like to use. 
	 * The name is validated to make sure it is not the name of the file being read, not blank and ends with ".txt" extension. 
	 */
	public static void main(String[] args) throws IOException{
		//
		//Declare Constants
		//
		final int MENU_CHOICE_AVERAGE_ZIP_CODE = 1;				//constant int for menu choice average sold for a zip code
		final int MENU_CHOICE_RANGE_ZIP_CODE = 2;				//constant int for menu choice range for a zip code
		final int MENU_CHOICE_NUMBER_SOLD_ZIPCODE = 3;			//constant int for menu choice number sold for a zip code
		final int MENU_CHOICE_AVERAGE_ALL_ZIPCODE = 4;			//constant int for menu choice average sold for all zip codes 
		final int MENU_CHOICE_RANGE_ALL_ZIPCODE = 5;			//constant int for menu choice range for all zip codes
		final int MENU_CHOICE_NUMBER_SOLD_ALL_ZIPCODE = 6;		//constant int for menu choice number sold for all zip codes
		final int MENU_CHOICE_CREATE_REPORT = 7;				//constant menu choice option to create a report
		final int MENU_CHOICE_EXIT = 8;							//constant menu choice to exit
		//
		//Declare objects
		//
		Scanner keyboard = new Scanner(System.in);					//Scanner Keyboard object
		DecimalFormat format = new DecimalFormat("$###,###.00");	//Decimal formatter
		File file = new File(FILE);									//file object
			//
			//Check file existence
			//
			if (!file.exists()) {
				System.out.println("The file data.dat is missing. Press any key (and ENTER) to exit.");
				keyboard.nextLine();
				System.exit(0);
			}//end check file
		//
		//Menu Loop
		//
		boolean keepRunning = true;		//boolean value to keep running the program.
		int menuChoice = 0;				//integer variable to hold the menu option the user wants.
		String reportFileName;			//String variable to hold the name of the file the user wants to call their report.
		String zipCode;					//Variable to hold the zip code choice the user will enter.
		do {
			System.out.println("\n     Home Sales Analysis" 
							   + "\n-------------------------------"
							   + "\n1. Average for a zip code"
							   + "\n2. Range for a zip code"
							   + "\n3. Number sold for a zip code"
							   + "\n4. Average for all zip codes"
							   + "\n5. Range for all zip codes"
							   + "\n6. Number sold for all zip codes"
							   + "\n7. Create Report"
							   + "\n8. Exit");
			//
			//get menu choice
			//
			menuChoice = keyboard.nextInt();
			//
			//validate input
			//
			if (menuChoice < MENU_CHOICE_AVERAGE_ZIP_CODE || menuChoice > MENU_CHOICE_EXIT){
				boolean notValid = true;
				do {
					System.out.println("That is not a valid choice. Try again!");
					menuChoice = keyboard.nextInt();
					if (menuChoice >= MENU_CHOICE_AVERAGE_ZIP_CODE && menuChoice <= MENU_CHOICE_EXIT) {
						notValid = false;
					}
						
				}while(notValid);//end validation loop
				
			}//end validate input
			//
			//Menu choice 1, Calculate average home price for a certain zip code.
			//
			if(menuChoice == MENU_CHOICE_AVERAGE_ZIP_CODE) {
				keyboard.nextLine();						//consume return character
				System.out.println("Enter a zip code ");
				zipCode = keyboard.nextLine();
				//
				//validate input
				//
				while(!zipCode.equals(ZIPCODE46835) 
					  && !zipCode.equals(ZIPCODE46825) 
					  && !zipCode.equals(ZIPCODE46815)) {
					System.out.println("That is not a supported zip code. Try again!");
					zipCode = keyboard.nextLine();
				}
				System.out.println("Average for zip code " + zipCode + " is " 
								   + format.format(calculateAverage(zipCode, FILE)));	
			}//End Option 1
			//
			//Menu choice 2, range for a particular zip code
			//
			if (menuChoice == MENU_CHOICE_RANGE_ZIP_CODE) {
				keyboard.nextLine();
				System.out.println("Enter a zip code ");
				zipCode = keyboard.nextLine();
				//
				//validate zip code
				//
				while(!zipCode.equals(ZIPCODE46835) 
					  && !zipCode.equals(ZIPCODE46825) 
					  && !zipCode.equals(ZIPCODE46815)) {
					System.out.println("That is not a supported zip code. Try again!");
					zipCode = keyboard.nextLine();
				}//end validate
				System.out.println("The range for zip code " + zipCode + " is " 
								  + format.format(calculateMin(zipCode, FILE))
								  + " - " + format.format(calculateMax(zipCode, FILE)));
				
			}//end menu choice 2
			//
			//menu choice 3, number of homes sold for a zip code
			//
			if (menuChoice == MENU_CHOICE_NUMBER_SOLD_ZIPCODE) {
				keyboard.nextLine();
				System.out.println("Enter a zip code ");
				zipCode = keyboard.nextLine();
				//
				//validate zip code
				//
				while(!zipCode.equals(ZIPCODE46835) 
					  && !zipCode.equals(ZIPCODE46825) 
					  && !zipCode.equals(ZIPCODE46815)) {
					System.out.println("That is not a supported zip code. Try again!");
					zipCode = keyboard.nextLine();
				}//end validate
				System.out.println("The number of homes sold for zip code " + zipCode + " is " 
									+ (calculateNumberSold(zipCode, FILE)));
				
			}//end menu choice 3
			//
			//menu choice 4, average for all zip codes
			//
			if (menuChoice == MENU_CHOICE_AVERAGE_ALL_ZIPCODE) {
				//
				//call associated method
				//
				System.out.println("Average for all zip codes " + format.format(calculateAverage(FILE)));
			}//end menu choice 4
			//
			//menu choice 5, sales range for all zip codes.
			//
			if (menuChoice == MENU_CHOICE_RANGE_ALL_ZIPCODE) {
				System.out.println("Range for all zip codes is " + format.format(calculateMin(FILE)) 
								+ " - " + format.format(calculateMax(FILE)));	
			}//end menu choice 5
			//
			//menu choice 6, total sales for all zip codes.
			//
			if (menuChoice == MENU_CHOICE_NUMBER_SOLD_ALL_ZIPCODE) {
				System.out.println("The number sold for all zip codes is " + calculateNumberSold(FILE));
			}//end menu choice 6
			//
			//menu choice 7, create report that prints to a file
			//
			if (menuChoice == MENU_CHOICE_CREATE_REPORT) {
				boolean isntValidName = true;				//boolean value to check if name entered by user is valid
				keyboard.nextLine();
				//
				//validation loop
				//
				do{
					System.out.println("Enter the name of the file");
					reportFileName = keyboard.nextLine();
					//
					//check entered name to see if it is the same as the file being read.
					//
					if(reportFileName.equals(FILE)) {
						System.out.println("File name cannot be same as data file. Try again!");
						continue;
					}
					//
					//check entered name to see if it is empty
					//
					else if(reportFileName.isEmpty()) {
						System.out.println("File name cannot be blank. Try again!");
						continue;
					}
					//
					//check if entered name ends with .txt
					//
					else if(!reportFileName.endsWith(".txt")) {
						System.out.println("File name must contain .txt extension. Try again!");
						continue;
					}	
					//
					//change isn't valid boolean to accept the name entered
					//
					else {isntValidName = false;
					}
				}while(isntValidName); //end validation loop
				//
				//create report
				//
				createReport(reportFileName, FILE);
				
			}
			//
			//menu choice 8, exit
			//
			if (menuChoice == MENU_CHOICE_EXIT) {
				System.out.println("The application exits. Press any key (and ENTER) to close.");
				keyboard.nextLine();
				keepRunning = false;		//change boolean value to end keepRunning loop
			}
			
			
		}while(keepRunning);//end menu loop
		System.exit(0);  	//exit program
		keyboard.close();	//close keyboard
	}//End Main Method
	/*
	 * The calculateAverage method (for a single zip code) takes two parameters, the requested zip code and the file to be read
	 * The the method reads loops reading the zip code and home price. When the zip code matches the desired zip code, the
	 * method will accumulate the home price and accumulate the total number of homes for that zip code. Finally, the method
	 * calculates the average by diving home price with total number of homes and returns that as a double. 
	 */
	public static double calculateAverage(String zip, String file)throws FileNotFoundException {
		String zipCode = zip;						
		int sum = 0;								//variable to hold the sum of the homes sold in a particular zip code
		double homeSalePrice = 0;					//variable to hold the price of the home sold in a particular zip code
		String nextZip;								//variable to read the zip code
		double average;								//variable to hold the average of homes sold for a particular zip code
		File fileIn = new File(FILE);				//object to read file
		Scanner inputFile = new Scanner(fileIn);	//scanner object to read file contents
		//
		//loop to read file contents
		//
		while(inputFile.hasNext()){
			nextZip = inputFile.next();
				//
				//check if zip code is the zip code we want data for
				//
				if (nextZip.equals(zipCode)) {
					homeSalePrice += inputFile.nextDouble(); 
					sum ++;
					continue;
				}
			//
			//read the sale price of the home not associated with the zip code we want
			//
			inputFile.nextDouble();
		}
		average = (double)homeSalePrice / sum;		//calculate average
			//
			//check if the sum of homes sold is zero
			//
			if(sum == 0) {
				System.out.println("Division by zero error!");
			}
		inputFile.close(); //close file reader
		return average;
	}//End CalculateAverage method
	/*
	 * The calculateAverage (for all zip codes) takes a single parameter, the file to be read. Then the method reads the file
	 * and accumulates the sales for all zip codes and the total number of homes. Finally, the method calculates the average 
	 * and returns the average as a double.
	 */
	public static double calculateAverage(String file)throws FileNotFoundException{
		int sum = 0;								//variable to accumulate the number of homes sold
		double homeSalePrice = 0;					//variable to accumulate the sales of homes
		double average;								//variable to hold the average
		File fileIn = new File(FILE);				
		Scanner inputFile = new Scanner(fileIn);
		//
		//read file and accumulate sales and total number of homes
		//
		while(inputFile.hasNext()){
			inputFile.nextDouble();
			homeSalePrice += inputFile.nextDouble();
				sum ++;
		}	
		average = (double) homeSalePrice / sum;
			if(sum == 0) {
				System.out.println("Division by zero error!");
			}
		inputFile.close();
		return average;
	}//End CalculateAverage all method
	/*
	 * the calculateMax (for single zip code method) takes two parameters, the zip code and the file to be read. Then the 
	 * method reads the file's zip codes and determines if it is the zip code to be calculated. The method proceeds to check
	 * each zip code for the desired zip code, reads the price of the home of desired zip code, checks if it is the max home 
	 * price for the desired zip code using the Math.max method and then stores and returns the max price of the homes.
	 */
	public static double calculateMax(String zip, String file)throws FileNotFoundException {
		String zipCode = zip;	//parses the zip parameter and stores it at as double
		double homeSalePrice = 0;					//variable to store the price of a home
		String nextZip;								//variable to hold the zip code being read
		double maxHomeSale = Double.MIN_VALUE;		//variable to hold the maximum price for the home
		
		File fileIn = new File(FILE);
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
			nextZip = inputFile.next();
				if (nextZip.equals(zipCode)) {
					homeSalePrice = inputFile.nextDouble();  
					maxHomeSale = Math.max(maxHomeSale, homeSalePrice);
					continue;
				}
			inputFile.nextDouble();
		}
		inputFile.close();
		return maxHomeSale;
	}//End Max Method
	/*
	 * The calculate max (for all zip codes) method takes one parameter, the file to be read from, reads the desired 
	 * file's zip code then reads the home price. The method determines if the home price is the max home price using 
	 * the Math.max method. Finally, the method returns the maximum sale price of all homes.
	 */
	public static double calculateMax(String file)throws FileNotFoundException{
		double homeSalePrice = 0;					//variable that stores the price for each home
		double maxHomeSale = Double.MIN_VALUE;		//variable to hold the max home price
		File fileIn = new File(FILE);				
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
			inputFile.nextDouble();
			homeSalePrice = inputFile.nextDouble();
			maxHomeSale = Math.max(maxHomeSale, homeSalePrice);
		}	
		inputFile.close();
		return maxHomeSale;
	}//End calculateMax for all zip codes method	
	/*
	 * the calculateMin (for single zip code method) takes two parameters, the zip code and the file to be read. Then the 
	 * method reads the file's zip codes and determines if it is the zip code to be calculated. The method proceeds to check
	 * each zip code for the desired zip code, reads the price of the home of desired zip code, checks if it is the min home 
	 * price of the desired zip code using the Math.min method and then stores and returns the min price of the homes in that
	 * zip code.
	 */
	public static double calculateMin(String zip, String file)throws FileNotFoundException {
		String zipCode = zip;
		double homeSalePrice = 0;
		String nextZip;
		double minHomeSale = Double.MAX_VALUE;
		
		File fileIn = new File(FILE);
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
			nextZip = inputFile.next();
				if (nextZip.equals(zipCode)) {
					homeSalePrice = inputFile.nextDouble();  
					minHomeSale = Math.min(minHomeSale, homeSalePrice);
					continue;
				}
			inputFile.nextDouble();
		}
		inputFile.close();
		return minHomeSale;
	}//End calculateMinMethod for single zip code
	/*
	 * The calculateMin (for all zip codes) takes one parameter, the file to be read. The method reads the desired file's 
	 * zip code then reads the home price. The method determines if the home price is the minimum home price using the 
	 * Math.min method. Finally, the method returns the maximum sale price of all homes. 
	 */
	public static double calculateMin(String file)throws FileNotFoundException{
		double homeSalePrice = 0;
		double minHomeSale = Double.MAX_VALUE;
		File fileIn = new File(FILE);
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
			inputFile.nextDouble();
			homeSalePrice = inputFile.nextDouble();
			minHomeSale = Math.min(minHomeSale, homeSalePrice);
		}	
		inputFile.close();
		return minHomeSale;
	}//End calculateMax for all zip codes method
	/*
	 * the calculatedNumberSold (for a single zip code) takes two parameters, the desired zip code and the file to be read
	 * determines the total number of homes sold for a zip code by reading the desired file's zip codes and determining if
	 * the zip code is the desired zip code to be calculated. If it is the desired zip code, the method accumulates the 
	 * number of times it has read that zip code and returns the sum for the desired zip code.
	 */
	public static int calculateNumberSold(String zip, String file)throws FileNotFoundException {
		String zipCode = zip;	//variable to hold the desired zip code
		int sum = 0;
		String nextZip;								//variable to hold the read zip code
		
		File fileIn = new File(FILE);
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
			nextZip = inputFile.next();
				if (nextZip.equals(zipCode)) {
					inputFile.nextDouble(); 
					sum ++;
					continue;
				}
			inputFile.nextDouble();
		}
		inputFile.close();
		return sum;
	}//End calculateNumberSold for single zip code method
	/*
	 * the calculateNumberSold(for all zip codes) takes one parameter, the file to be read from, The method reads the 
	 * desired file's contents and accumulates the total number of lines in the file and returns the sum.
	 */
	public static int calculateNumberSold(String file)throws FileNotFoundException {
		int sum = 0;
		File fileIn = new File(FILE);
		Scanner inputFile = new Scanner(fileIn);
		while(inputFile.hasNext()){
				inputFile.nextDouble();
				inputFile.nextDouble();
				sum++;
				}
		inputFile.close();
		return sum;
	}//End calculateNumberSold for all zip codes method
	/*
	 * the createReport method takes two parameters, the filename to be created and the file that is read. The method then
	 * creates a report of the results of all previous methods by writing the results of all the other methods to a file 
	 * named by the user.  
	 */
	public static void createReport(String filename, String file)throws FileNotFoundException, IOException{
		DecimalFormat format = new DecimalFormat("$###,###.00");
		String fileName = "";
		file = FILE;
		fileName = filename;
		FileWriter fwriter = new FileWriter(fileName, true);
		PrintWriter outputFile = new PrintWriter(fwriter);
		
		outputFile.println("Zip Code " + ZIPCODE46815);
		outputFile.println("--------------");
		outputFile.println("Number sold: " + (calculateNumberSold(ZIPCODE46815, file))); 
		outputFile.println("Average sales price: " + format.format(calculateAverage(ZIPCODE46815, file)));
		outputFile.println("Range: " + format.format(calculateMin(ZIPCODE46815, file)) 
							+ " - "	+ format.format(calculateMax(ZIPCODE46815, file)));
		outputFile.println();
		outputFile.println();
		outputFile.println("Zip Code " + ZIPCODE46825);
		outputFile.println("--------------");
		outputFile.println("Number sold: " + (calculateNumberSold(ZIPCODE46825, file))); 
		outputFile.println("Average sales price: " + format.format(calculateAverage(ZIPCODE46825, file)));
		outputFile.println("Range: " + format.format(calculateMin(ZIPCODE46825, file)) 
							+ " - "	+ format.format(calculateMax(ZIPCODE46825, file)));
		outputFile.println();
		outputFile.println();
		outputFile.println("Zip Code " + ZIPCODE46835);
		outputFile.println("--------------");
		outputFile.println("Number sold: " + (calculateNumberSold(ZIPCODE46835, file))); 
		outputFile.println("Average sales price: " + format.format(calculateAverage(ZIPCODE46835, file)));
		outputFile.println("Range: " + format.format(calculateMin(ZIPCODE46835, file)) 
							+ " - "	+ format.format(calculateMax(ZIPCODE46835, file)));
		outputFile.println();
		outputFile.println();
		outputFile.println("All zip codes ");
		outputFile.println("--------------");
		outputFile.println("Number sold: " + (calculateNumberSold(file))); 
		outputFile.println("Average sales price: " + format.format(calculateAverage(file)));
		outputFile.println("Range: " + format.format(calculateMin(file)) 
							+ " - "	+ format.format(calculateMax(file)));
		
		outputFile.close();
	}//end createReport Method
}//End Class HomeSalesAnalysisCalculator
