TITLE Program Template           (main.asm)

; Program Description:
; Author:
; Creation Date:
; Revisions: 
; Date:              
; Modified by:

.386
.model FLAT, stdcall
include C:\masm32\include\masm32rt.inc
include C:\masm32\include\windows.inc
include C:\masm32\include\user32.inc
include C:\masm32\include\kernel32.inc
include \masm32\include\masm32.inc
includelib \masm32\lib\masm32.lib
includelib \masm32\lib\kernel32.lib
STD_OUTPUT_HANDLE EQU -11
STD_INPUT_HANDLE EQU -10
GetStdHandle PROTO, nStdHandle: DWORD

.stack 4096
ExitProcess proto, dwExitCode:dword

.data
	
consoleOutHandle dd ?	;Used to get the pointer to the console being used
consoleInHandle dd ?	;Used to get the pointer to the keyboard
bytesWritten dword ?	;Used to write the amount of bytes written from WriteConsole
message0 db 'Enter the text to be encrypted (25 character max): ',13,10	;message displayed to user
lmessage0 dd 50			;length of above message
message db 'Enter the Key (15 character max):  ',13,10 ;message displayed to user
lmessage dword 35		;length of above message
keyCount dword 0		;a counter to know where we are in buffer
MSGCount dword 0		;a counter to know where we are in the bufferMSG/result
;charswritten    dd  ?	
charsread       dd  ?	;to know how many characters read from key
charsreadMSG	dd	?	;to know how many characters read from message
buffer			db 15 dup(?)	;holds the key
bufferMSG		db 25 dup(?)	;holds the message to be encrypted
result			db 25 dup(?)	;holds the result

.code
main PROC
	
	;first we will get a console and grab the pointer to the standard output and input

	INVOKE AllocConsole
	INVOKE GetStdHandle, STD_OUTPUT_HANDLE
	mov consoleOutHandle, eax 
	INVOKE GetStdHandle, STD_INPUT_HANDLE
	mov consoleInHandle, eax

	;next we will print the first message asking the user to enter the message and key
	mov edx, offset message0    
	mov eax, lmessage0
	INVOKE WriteConsole, consoleOutHandle, edx, eax, offset bytesWritten, 0
	INVOKE ReadConsole,consoleInHandle,offset bufferMSG,sizeof bufferMSG, offset charsreadMSG,NULL
	sub charsreadMSG, 2		;because it reads the newline character we need to subtract
	mov edx, offset message 
	mov eax, lmessage
	INVOKE WriteConsole, consoleOutHandle, edx, eax, offset bytesWritten, 0
	INVOKE ReadConsole,consoleInHandle,offset buffer,sizeof buffer, offset charsread,NULL
	sub charsread, 2
	mov ecx, charsreadMSG


	;for encrypting the string
RESETKEYCOUNT:
	mov keyCount, 0
LEncrypt:
	mov edx, keyCount		;move the keyCount to edx
	cmp edx, charsread		;compare keycount to the size of the key
	JE RESETKEYCOUNT		;jump if equal to reset the key
	mov al, buffer[edx]		;mov the character in the current count of the key into al
	add keyCount, 1			;add 1 to key count
	mov edx, MSGCount		;mov the letter count of the message into edx
	mov bl, bufferMSG[edx]	;move the letter into bl
	xor al, bl				;xor the two letters
	mov result[edx], al		;store the xored letters into the result message
	add MSGCount, 1			;add 1 to message count
	LOOP LEncrypt

	;print to the console
	;mov edx, offset result   
	;mov eax, 18
	;INVOKE WriteConsole, consoleOutHandle, edx, eax, offset bytesWritten, 0

	;reset for decryption
	mov ecx, charsreadMSG
	mov MSGCount, 0

	;for decryting the string


	;same as above process
RESETKEYCOUNT1:
	mov keyCount, 0
LDecrypt:
	mov edx, keyCount
	cmp edx, charsread
	JE RESETKEYCOUNT1
	mov al, buffer[edx]
	add keyCount, 1
	mov edx, MSGCount
	mov bl, result[edx]
	xor al, bl
	mov result[edx], al
	add MSGCount, 1
	LOOP LDecrypt
    
	;print to the console
	mov edx, offset result   
	mov eax, 18
	INVOKE WriteConsole, consoleOutHandle, edx, eax, offset bytesWritten, 0
	INVOKE ExitProcess, 0

main ENDP

	; (insert additional procedures here)

END main
