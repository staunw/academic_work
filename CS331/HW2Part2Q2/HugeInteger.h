/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
// HugeInteger.h
// HugeInteger class definition.
#ifndef HUGEINTEGER_H
#define HUGEINTEGER_H
#include <array>
#include <string>

class HugeInteger {
public:
	HugeInteger(long = 0); // conversion/default constructor
	HugeInteger(const std::string&); // copy constructor

	bool isEqualTo(const HugeInteger& HI) const;				 // is equal to
	bool isNotEqualTo(const HugeInteger& HI) const;			     // not equal to
	bool isGreaterThan(const HugeInteger& HI) const;			 // greater than
	bool isLessThan(const HugeInteger& HI) const;				 // less than
	bool isZero(void) const;									 // is zero

	std::string toString() const; // output   

private:
	std::array<short, 40> integer; // 40 element array
};

#endif