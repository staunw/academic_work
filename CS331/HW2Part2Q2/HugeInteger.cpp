/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
// HugeInteger.cpp
// Member-function definitions for class HugeInteger.
#include "HugeInteger.h" // include definition of class HugeInteger
#include <iostream>
#include <sstream>

using namespace std;

// default constructor; conversion constructor that converts
// a long integer into a HugeInteger object
HugeInteger::HugeInteger(long value) {
	// initialize array to zero
	for (int i{ 0 }; i < 40; ++i) {
		integer[i] = 0;
	}

	// place digits of argument into array
	for (int j{ 39 }; value != 0 && j >= 0; --j) {
		integer[j] = static_cast<short>(value % 10);
		value /= 10;
	}
}


// copy constructor;
// converts a char string representing a large integer into a HugeInteger
HugeInteger::HugeInteger(const string& string) {
	// initialize array to zero
	for (int i{ 0 }; i < 40; ++i) {
		integer[i] = 0;
	}

	// place digits of argument into array
	unsigned int length{ string.size() };

	for (unsigned int j{ 40 - length }, k{ 0 }; j < 40; ++j, ++k) {
		if (isdigit(string[k])) {
			integer[j] = string[k] - '0';
		}
	}
}


// function that tests if two HugeIntegers are equal
bool HugeInteger::isEqualTo(const HugeInteger& HI) const {

	bool isEqual{ true };
	//Loop Array and determine equality
	for (int i{ 0 }; i < 40; i++) {
		if (this->integer[i] != HI.integer[i]) {
			isEqual = false;
			break;
		}
	}

	return isEqual;
}


// function that tests if two HugeIntegers are not equal
bool HugeInteger::isNotEqualTo(const HugeInteger& HI) const {

	bool notEqual{ false };

	for (int i{ 0 }; i < 40; i++) {
		if (this->integer[i] == HI.integer[i]) {
			notEqual = true;
			break;
		}
	}
	return notEqual;
}


// function to test if one HugeInteger is greater than another
bool HugeInteger::isGreaterThan(const HugeInteger& HI) const {

	bool greaterThan{ false };

	for (int i{ 39 }; i >= 0; i--) {
		if (this->integer[i] > HI.integer[i]) {
			greaterThan = true;
		}
	}

	return greaterThan;
}



// function that tests if one HugeInteger is less than another
bool HugeInteger::isLessThan(const HugeInteger& HI) const {

	bool isLess{ false };

	for (int i{ 30 }; i >= 0; i--) {
		if (this->integer[i] < HI.integer[i]) {
			isLess = true;
		}
	}


	return isLess;
}




// function that tests if a HugeInteger is zero
bool HugeInteger::isZero() const {

	bool isZero{ true };

	for (int i{ 0 }; i < 40; i++) {
		if (this->integer[i] != 0) {
			isZero = false;
			break;
		}
	}

	return isZero;
}


// overloaded output operator
string HugeInteger::toString() const {
	int i; // used for looping

	for (i = 0; (i < 40) && (integer[i] == 0); ++i) {
		; // skip leading zeros
	}
	if (i == 40) {
		return "0";
	}
	else {
		ostringstream output;

		for (; i < 40; ++i) { // display the HugeInteger
			output << integer[i];
		}

		return output.str();
	}
}