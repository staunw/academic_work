/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION3
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
// Hugeint.h
// HugeInt class definition.
#ifndef HUGEINT_H
#define HUGEINT_H

#include <iostream>
#include <string>
using namespace std;

class HugeInt
{
	friend ostream& operator<<(ostream&, const HugeInt&);
public:
	static const size_t digits = 30; // maximum digits in a HugeInt

	HugeInt(long = 0); // conversion/default constructor
	HugeInt(const string&); // conversion constructor

	bool operator==(const HugeInt&) const; // equality operator
	bool operator!=(const HugeInt&) const; // inequality operator
	bool operator<(const HugeInt&) const;  // less than operator
	bool operator>(const HugeInt&) const;  // greater than operator

private:
	short integer[digits];
};

#endif

