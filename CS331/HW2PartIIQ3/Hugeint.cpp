/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION3
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
// Hugeint.cpp
// HugeInt member-function and friend-function definitions.
#include <iostream>
#include <stdexcept>
#include <cctype> // isdigit function prototype
#include "Hugeint.h" // HugeInt class definition
using namespace std;

// default constructor; conversion constructor that converts
// a long integer into a HugeInt object
HugeInt::HugeInt(long value) {
	// initialize array to zero
	for (size_t i{ 0 }; i < digits; ++i) {
		integer[i] = 0;
	}

	// place digits of argument into array
	for (int j{ digits - 1 }; value != 0 && j >= 0; --j) {
		integer[j] = value % 10;
		value /= 10;
	}
}

// conversion constructor that converts a character string
// representing a large integer into a HugeInt object
HugeInt::HugeInt(const string& number) {
	// initialize array to zero
	for (size_t i{ 0 }; i < digits; ++i) {
		integer[i] = 0;
	}

	// place digits of argument into array
	size_t length{ number.size() };

	for (size_t j{ digits - length }, k{ 0 }; j < digits; ++j, ++k) {
		if (isdigit(number[k])) {
			integer[j] = number[k] - '0';
		}
	}
}

// equality operator; HugeInt == HugeInt
bool HugeInt::operator==(const HugeInt& other) const {
	bool isEqual{ true };
	//Loop Array and determine equality
	for (int i{ 0 }; i < digits; i++) {
		if (this->integer[i] != other.integer[i]) {
			isEqual = false;
			break;
		}
	}

	return isEqual;
}


// inequality operator; HugeInt != HugeInt
bool HugeInt::operator!=(const HugeInt& other) const {
	bool notEqual{ false };
	//loop through array and determine inequality
	for (int i{ 0 }; i < digits; i++) {
		if (this->integer[i] == other.integer[i]) {
			notEqual = true;
			break;
		}
	}
	return notEqual;
}


// less than operator; HugeInt < HugeInt
bool HugeInt::operator<(const HugeInt& other) const {

	bool greaterThan{ false };

	for (int i{ digits - 1 }; i >= 0; i--) {
		if (this->integer[i] > other.integer[i]) {
			greaterThan = true;
		}
	}

	return greaterThan;
}


// greater than operator; HugeInt > HugeInt
bool HugeInt::operator>(const HugeInt& other) const {
	bool greaterThan{ false };

	for (int i{ digits - 1 }; i >= 0; i--) {
		if (this->integer[i] > other.integer[i]) {
			greaterThan = true;
		}
	}

	return greaterThan;
}


// overloaded output operator
ostream& operator<<(ostream& output, const HugeInt& num) {
	size_t i;

	for (i = 0; i < HugeInt::digits && num.integer[i] == 0; ++i) {
		; // skip leading zeros
	}

	if (i == HugeInt::digits) {
		output << 0;
	}
	else {
		for (; i < HugeInt::digits; ++i) {
			output << num.integer[i];
		}
	}

	return output;
}