// ****************************************************************
// Homework#4, Part II, Q4
// ****************************************************************
// Stack-based Evaluation of Postfix Expressions
// This program uses a stack to evaluate Postfix expressions
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 4, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		8/02/2019
*/
#include <iostream>
#include <string>
#include <sstream>
#include <stack>
using namespace std;



// skipWhiteSpace for Skiping whitespace in an input stream.                        *

void skipWhiteSpace(istream& in)
{
	while (in.good() && isspace(in.peek()))
	{
		// Read and discard the space character
		in.get();
	}
}

//
// postFixEval                        
// If the next token in the input is an integer, read  
// the integer and push it on the stack; but if it is  
// an operator, pop the last two values from the stack 
// and apply the operator, and push the result onto    
// the stack.  At the end of the string, the lone      
// on the stack is the result.                         
//
int postFixEval(string str)
{
	istringstream in = istringstream(str);
	stack<int> postFixStack;
	skipWhiteSpace(in);

	while (in)
	{
		int operand;								//Stores the operand
		char c{ static_cast<char>(in.peek()) };		//stores the character we remove when checking for a negative number

		//Check if a number is negative and push it onto the stack
		if ((in.get() == '-') && (isdigit(in.peek()))) {
			operand = (in.get() - '0');		//store first digit

			//check if there are more than one digits
			while (isdigit(in.peek())) {
				operand = (operand * 10) + (in.get() - '0');
			}
			//Push onto the stack and skip the next whitespace
			operand *= -1;				//make operand negative
			postFixStack.push(operand);
			skipWhiteSpace(in);
			continue;
		}
		//If a number is not negative we need to put back the character we removed
		else {
			in.putback(c);
		}
		//Check for positive numbers
		if(isdigit(in.peek())) {

			operand = (in.get() - '0');		//store digit
			//check if there are more digits
			while (isdigit(in.peek())) {
				operand = (operand * 10) + (in.get() - '0');
			}
			//push onto the stack and skip the next whitespace
			postFixStack.push(operand);
			skipWhiteSpace(in);
			continue;
		}
		//check if we have an operator
		if (in.peek() == '+' || in.peek() == '-' || in.peek() == '*' || in.peek() == '/') {
			char op = in.get();		//stores the operator

			skipWhiteSpace(in);		//skip whitespace

			//store operand2 and then pop the stack
			int operand2 = postFixStack.top();
			postFixStack.pop();
			//store operand1 and then pop the stack
			int operand1 = postFixStack.top();
			postFixStack.pop();
			//apply the appropriate operation
			switch (op) {
				case '+': postFixStack.push(operand1 + operand2); break;
				case '-': postFixStack.push(operand1 - operand2); break;
				case '*': postFixStack.push(operand1 * operand2); break;
				case '/': postFixStack.push(operand1 / operand2); break;
				default: cout << "Invalid operation" << endl; break;
			}

			skipWhiteSpace(in);
			continue;

		}
		

	}
	return postFixStack.top();
}

int main()
{
	string input;

	while (true)
	{
		cout << "Enter a postfix expression, or press ENTER to quit:\n";
		getline(cin, input);

		if (input.length() == 0)
		{
			break;
		}
		int number = postFixEval(input);
		cout << "The value of " << input << " is " << number << endl;
	}

	return 0;
}