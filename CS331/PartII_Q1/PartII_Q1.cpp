/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION1
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

//Tests the capabilities of the Invoice class

#include <iostream>
#include "Invoice.h"
int main() {
	//Create an Invoice object
	Invoice i1{ "leftW1", "A left hand widget", 5, 10 };
	//Test getPartNumber()
	std::cout << "Testing getPartNumber()" << std::endl;
	std::cout << "Part Number: " << i1.getPartNumber() << std::endl;
	std::cout << "\n";
	//Test setPartNumber()
	std::cout << "Testing setPartNumber()" << std::endl;
	i1.setPartNumber("rightW1");
	std::cout << "New Part Number: " << i1.getPartNumber() << std::endl;
	std::cout << "\n";
	//Test getPartDescription()
	std::cout << "Testing getPartDescription()" << std::endl;
	std::cout << "Product Description: " << i1.getPartDescription() << std::endl;
	std::cout << "\n";
	//Test setPartDescription()
	std::cout << "Testing setPartDescription()" << std::endl;
	i1.setPartDescription("A right hand widget");
	std::cout << "New Product Description: " << i1.getPartDescription() << std::endl;
	std::cout << "\n";
	//Test getQuantityPurchased()
	std::cout << "Testing getQuantityPurchased()" << std::endl;
	std::cout << "Quantity Purchased: " << i1.getQuantityPurchased() << std::endl;
	std::cout << "\n";
	//Test setQuantityPurchased()
	std::cout << "Testing setQuantityPurchased()" << std::endl;
	i1.setQuantityPurchased(10);
	std::cout << "New Quantity Purchased: " << i1.getQuantityPurchased() << std::endl;
	std::cout << "\n";
	//Test getPricePerItem()
	std::cout << "Testing getPricePerItem()" << std::endl;
	std::cout << "Price Per Item: " << i1.getPricePerItem() << std::endl;
	std::cout << "\n";
	//Test setPricePerItem()
	std::cout << "Testing setPricePerItem()" << std::endl;
	i1.setPricePerItem(100);
	std::cout << "New Price Per item: " << i1.getPricePerItem() << std::endl;
	std::cout << "\n";
	//Test getInvoiceAmount()
	std::cout << "Testing getInvoiceAmount()" << std::endl;
	std::cout << "Invoice Amount: " << i1.getInvoiceAmount() << std::endl;
	std::cout << "\n";
	//Test negative pricePerItem
	std::cout << "Testing negative pricePerItem" << std::endl;
	i1.setPricePerItem(-10);
	std::cout << "Invoice Amount: " << i1.getInvoiceAmount() << std::endl;
	std::cout << "\n";
	//Test negative quantityPurchased
	std::cout << "Testing negative quantityPurchased" << std::endl;
	i1.setPricePerItem(10);
	i1.setQuantityPurchased(-19);
	std::cout << "Invoice Amount: " << i1.getInvoiceAmount() << std::endl;
	std::cout << "\n";

	return 0;
}
