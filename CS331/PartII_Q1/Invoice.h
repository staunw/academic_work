#pragma once
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION1
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

//Invoice class that represents an invoice with members: partNumber,
//partDescription, quantityPurchased, and pricePerItem. Includes constructor
//and setters/getters for each member. Finally, has function to calculate
//invoice amount

#include <string>

class Invoice {
public:
	//constructor that initializes each member
	Invoice(std::string pNum, std::string pDescrp, int qty, int ppi) :
		partNumber{ pNum },
		partDescription{ pDescrp },
		quantityPurchased{ qty },
		pricePerItem{ ppi }
	{/*empty*/}
	//getter for partNumber member
	std::string getPartNumber() const {
		return partNumber;
	}
	//setter for partNumber member
	void setPartNumber(std::string invoicePartNumber) {
		partNumber = invoicePartNumber;
	}
	//getter for partDescription
	std::string getPartDescription() const {
		return partDescription;
	}
	//setter for partDescription
	void setPartDescription(std::string invoicePartDescription) {
		partDescription = invoicePartDescription;
	}
	//getter for quantityPurchased
	int getQuantityPurchased() const {
		return quantityPurchased;
	}
	//setter for quantityPurchased
	void setQuantityPurchased(int invoiceQuantityPurchased) {
		quantityPurchased = invoiceQuantityPurchased;
	}
	//getter for pricePerItem
	int getPricePerItem() const {
		return pricePerItem;
	}
	//setter for pricePerItem
	void setPricePerItem(int invoicePricePerItem) {
		pricePerItem = invoicePricePerItem;
	}
	//getter for invoice amount. Calculates the invoice amount and returns an
	//integer representing that amount
	int getInvoiceAmount() const {
		int retVal{ 0 };

		if (quantityPurchased < 0 || pricePerItem < 0) {

		}
		else {
			retVal = quantityPurchased * pricePerItem;
		}

		return retVal;
	}

private:
	std::string partNumber;      // the part number for this invoice
	std::string partDescription; // the description for this invoice
	int quantityPurchased;      // the number of parts ordered for this invoice
	int pricePerItem;           // the price per item
};
