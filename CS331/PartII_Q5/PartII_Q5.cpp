/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION5
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

#include <iostream>
#include <array>
#include <limits>
#include <iomanip>

int validInput(void);       //function to allow for only integer values to be entered

const size_t monkey{ 3 };     //constant size for the monkey part of the 2d array
const size_t day{ 7 };        //constant size for the day part of the 2d array
//void printArray(const array<array<int, day>, monkey>)

int main() {

	int amountEaten{ 0 };     //variable to hold the amount a monkey ate entered by the user
	int sum{ 0 };             //the sum of food the monkeys ate
	int sumM1{ 0 };           //the sum of food monkey 1 ate
	int sumM2{ 0 };           //the sum of food monkey 2 ate
	int sumM3{ 0 };           //the sum of food monkey 3 ate
	double avg{ 0.0 };        //The avg the 3 monkeys ate

	std::array<std::array<int, day>, monkey> monkeyBusiness;    //the array of monkeys
	//loop through the array and ask the user for the pounds
	for (int i{ 0 }; i < monkeyBusiness.size(); i++) {

		for (int j{ 0 }; j < monkeyBusiness[i].size(); j++) {
			std::cout << "Enter amount eaten by monkey " << (i + 1) << " for day " << (j + 1) << std::endl;
			amountEaten = validInput();

			sum += amountEaten;     //calculate the sum to find the average
			//determines the sum based on the monkey
			switch (i) {
			case 0:
				sumM1 += amountEaten;
				break;
			case 1:
				sumM2 += amountEaten;
				break;
			case 2:
				sumM3 += amountEaten;
				break;
			default:;
			}
		}
	}
	//calculate average
	avg = static_cast<double>(sum) / (static_cast<double>(monkey) * day);
	//print average
	std::cout << "The average eaten by the family of monkeys per day is " << std::fixed << std::setprecision(2) << avg << std::endl;
	//print minimum amount a monkey ate
	if (sumM1 < sumM2 && sumM1 < sumM3) {
		std::cout << "Monkey 1 ate the least during the week, he ate: " << sumM1 << " pounds " << std::endl;
	}
	else if (sumM2 < sumM1 && sumM2 < sumM3) {
		std::cout << "Monkey 2 ate the least during the week, he ate: " << sumM2 << " pounds " << std::endl;
	}
	else {
		std::cout << "Monkey 3 ate the least during the week, he ate: " << sumM3 << " pounds " << std::endl;
	}
	//print maximum amount a monkey ate
	if (sumM1 > sumM2 && sumM1 > sumM3) {
		std::cout << "Monkey 1 ate the most during the week, he ate: " << sumM1 << " pounds " << std::endl;
	}
	else if (sumM2 > sumM1 && sumM2 > sumM3) {
		std::cout << "Monkey 2 ate the most during the week, he ate: " << sumM2 << " pounds " << std::endl;
	}
	else {
		std::cout << "Monkey 3 ate the most during the week, he ate: " << sumM3 << " pounds " << std::endl;
	}

	return 0;
}
//allows for only integers to be entered by a user
int validInput(void) {
	int x;
	std::cin >> x;
	while (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Invalid entry.  Enter a NUMBER: ";
		std::cin >> x;
	}
	return x;
}
