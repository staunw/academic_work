/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
#include <iostream>
#include "Zipcode.h"
using namespace std;

int main() {

	//instantiate Zipcode object
	Zipcode zipcode;
	//calls readZipcode method
	zipcode.readZipcode();
	//calls printBarcode method
	zipcode.printBarcode();
	return 0;

}