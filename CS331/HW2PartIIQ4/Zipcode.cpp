/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/

/* Represents a zipcode*/
#include "Zipcode.h"
#include <iostream>
#include <iomanip>
#include <stack>

const int ZIPCODE_DIGITS = 5; //Number of digits in a zip code
//Constructor ask the user for the number of zip codes and store them
Zipcode::Zipcode() {

	std::cout << "How many zip codes do you want to enter: " << std::endl;
	std::cin >> this->numZipcode;
	Zipcode::zipcode = new int[this->numZipcode];
	Zipcode::correctionDigit = new int[this->numZipcode];

}
//Deconstructor: deallocate memory allocated by new
Zipcode::~Zipcode() {
	delete [] this->correctionDigit;
	delete [] this->zipcode;
}

//Reads in zip codes from console and calls correctionDigitOf
void Zipcode::readZipcode() {
	
	for (size_t i{ 0 }; i < this->numZipcode; i++) {
		std::cout << "Enter zip code " << (i + 1) << ": "<< std::endl;
		std::cin >> this->zipcode[i];

		correctionDigitOf(i);
	}

}
//Calculates the correction digit of a given zip code
void Zipcode::correctionDigitOf(int index) {

	int zip{ this->zipcode[index] };  //The zip code to be calculated
	int sum{ 0 };
	while (zip != 0) {
		sum += zip % 10;
		zip = zip / 10;
	}

	if ((10 - sum % 10) % 10 == 0) {
		this->correctionDigit[index] = 0;
	}
	else {
		this->correctionDigit[index] = 10 - sum % 10;
	}
	
}
//Extracts a single digit of a given zip code for the given location in the number
int Zipcode::extract(int index, int location) {

	int number = this->zipcode[index];
	std::stack<int> values;				//Stores the individual values
	//Push individual digits onto the stack
	while (number > 0) {
		values.push(number % 10);
		number = number / 10;
	}
	//Pop values until you reach the location you want
	for (int i{ 0 }; i < location; i++) {
		values.pop();
	}
	return values.top();
}
//Pretty prints the barcodes based on the requirements
void Zipcode::printBarcode() {

	std::cout << std::setw(10) << "Zipcode"
		<< std::setw(16) << "Correction" << std::setw(10)
		<< "Barcode" << std::endl;
	std::cout << std::setw(24) << "Digit" << std::endl;

	for (int i{ 0 }; i < this->numZipcode; i++) {
		std::cout << std::setw(9) << this->zipcode[i]
			<< std::setw(13) << this->correctionDigit[i]; std::cout << std::setw(8);
		displayBarcode(i);
		std::cout << std::endl;
	}
}
//Pretty prints barcodes
void Zipcode::displayBarcode(int index) {
	std::cout << "|";
	for (int i{ 0 }; i < ZIPCODE_DIGITS; i++) {
		
		displayCode(extract(index, i));
	}
	displayCode(this->correctionDigit[index]);
	std::cout << "|";
}
//Finds the bar code of a given digit
void Zipcode::displayCode(int digit) {

	switch (digit) {
		case 0: std::cout << "||:::";
			break;
		case 1: std::cout << ":::||";
			break;
		case 2: std::cout << "::|:|";
			break;
		case 3: std::cout << "::||:";
			break;
		case 4: std::cout << ":|::|"; 
			break;
		case 5: std::cout << ":|:|:";
			break;
		case 6: std::cout << ":||::";
			break;
		case 7: std::cout << "|:::|";
			break;
		case 8: std::cout << "|::|:";
			break;
		case 9: std::cout << "|:|::";
			break;
	}

}
