/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
#ifndef ZIPCODE_H
#define ZIPCODE_H
#include <iostream>
#include <iomanip>

class Zipcode {
public:
	Zipcode();
	~Zipcode();
	void readZipcode();
	void correctionDigitOf(int index);
	int extract(int index, int location);
	void printBarcode();
	void displayBarcode(int index);
	void displayCode(int digit);
private:
	int numZipcode;
	int* zipcode{nullptr};
	int* correctionDigit{nullptr};
};
#endif