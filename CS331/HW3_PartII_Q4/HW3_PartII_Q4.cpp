// HW3, Part II, Q4
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 3, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/25/2019
*/
#include <iostream>

//FILL -  vector class-template definition
#include <vector>



using namespace std;

// FILL - function template palindrome definition
template <typename X>
bool palindrome(const vector<X>& vec) {

	return equal(vec.cbegin(), vec.cend(), vec.crbegin());

}

// FILL - function template printVector definition
template <typename Y>
void printVector(const vector<Y>& vec) {

	for (int i{ 0 }; i < vec.size(); i++) {
		cout << vec.at(i) << " ";
	}
}

int main() {
	vector<int> iv;
	vector<char> ic;
	int x{ 0 };

	for (int i{ 75 }; i >= 65; --i) {
		iv.push_back(i);
		ic.push_back(static_cast<char>(i + x));

		if (i <= 70) {
			x += 2;
		}
	}

	printVector(iv);
	cout << (palindrome(iv) ? " is " : " is not ") << "a palindrome\n";

	printVector(ic);
	cout << (palindrome(ic) ? " is " : " is not ") << "a palindrome\n";
}
