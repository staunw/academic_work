/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 4, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		8/02/2019
*/
// ****************************************************************
// Homework#4, Part II, Q2
// ****************************************************************


#include <iostream>
#include <map> // COMPLETE 
#include <iomanip>
using namespace std;

int main()
{
	map<int, int> histogram;// COMPLETE - Create a map object named histogram. 

	int num = 0;
	cout << "Enter positive numbers.  Enter -1 when done." << endl;

	//COMPLETE for the programming task
	while (true) 
	{
		//read input
		cin >> num;
		//check if input was -1; break if true
		if (num == -1) {
			break;
		}
		//Get return value from insertion
		auto ret = histogram.insert(std::make_pair(num, 1));
		//If insertion did not succeed, 
		//add 1 to the key value that tried to be inserted
		if (!ret.second) {
			ret.first->second++;
		}


	}

	// COMPLETE - Output the histogram
	map<int, int>::const_iterator iter;
	//Iterate through map and print values
	for (iter = histogram.begin(); iter != histogram.end(); ++iter)
	{
		cout << "The number " <<   iter->first   << " appears " <<    iter->second    << " times." << endl;
	}

	char ch;
	cin >> ch;
	return 0;
}


