/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 3, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/25/2019
*/

#include "Point.h"
#include <iostream>
#include <cctype>
#include <cstdlib>

//Stream insertion
std::ostream& operator << (std::ostream& output, const Point& pt) {
	
	output << "(" << pt.xCoordinate << ", " << pt.yCoordinate << ")\n";

	return output;
}
//Stream extraction
std::istream& operator >> (std::istream& input, Point& pt) {
	
	if (static_cast<char>(input.get()) != '(') {
		std::cin.setstate(std::ios::failbit);		
	}
	if (!(isdigit(static_cast<char>(input.peek())))) {
		std::cin.setstate(std::ios::failbit);
	}
	else {
		pt.xCoordinate = input.get() - '0';
	}
	if (static_cast<char>(input.get()) != ',') {
		std::cin.setstate(std::ios::failbit);
	}
	if (static_cast<char>(input.get()) != ' ') {
		std::cin.setstate(std::ios::failbit);
	}
	if (!(isdigit(static_cast<char>(input.peek())))) {
		std::cin.setstate(std::ios::failbit);
	}
	else {
		pt.yCoordinate = input.get() - '0';
	}
	if (static_cast<char>(input.get()) != ')') {
		std::cin.setstate(std::ios::failbit);
	}

	return input;
}