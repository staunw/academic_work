/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 3, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/25/2019
*/

#ifndef POINT_H
#define POINT_H

#include <iostream>

class Point {
	public:
		friend std::ostream& operator << (std::ostream& output, const Point&);
		friend std::istream& operator >> (std::istream& input, Point&);
	private:
		int xCoordinate;
		int yCoordinate;
};

#endif // !POINT_H
