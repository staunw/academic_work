/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 4, PARTII, QUESTION5
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		8/02/2019
*/

#include "LinkedList.h"
//Default constructor
template<typename Type>
LinkedList<Type>::LinkedList() : head{ nullptr }, cursor{ nullptr } {}
//Copy constructor
template<typename Type>
LinkedList<Type>::LinkedList(const LinkedList &src) {
	this->head = nullptr;
	this->cursor = nullptr;

	for (ListElement* ptr{ src.head }; ptr != nullptr; ptr = ptr->next) {
		this->insert(ptr->element, 0);
	}
	
}
//Destructor
template<typename Type>
LinkedList<Type>::~LinkedList() {
	this->clear();
}
//insert
template<typename Type>
void LinkedList<Type>::insert(const Type& item, int i) {
	ListElement* inserted = new ListElement(item, nullptr);
	
	//insert as first item
	if (this->empty()) {
		this->head = inserted;
	}
	//insert after cursor element
	else if (!this->empty() && i == 0) {
		ListElement* tmp = this->cursor->next;
		this->cursor->next = inserted;
		inserted->next = tmp;
	}
	//insert before cursor element
	else if (!this->empty() && i == -1) {
		ListElement* tmp = this->cursor;
		this->cursor = inserted;
		inserted->next = tmp;
	}
	else {

	}
	//move cursor to inserted element
	this->cursor = inserted;
}
//remove
template<typename Type>
void LinkedList<Type>::remove() {
	ListElement* removed = this->cursor;				//Element to be removed
	//When list is not empty, remove current element
	//Set cursor to the following element
	if (!this->empty()) {
		//Check if removed is head
		if (removed == this->head) {
			this->head = removed->next;
			this->cursor = removed->next;
			delete removed;
		}
		//Check if 1 element
		else if (this->head->next == nullptr) {
			delete this->head;
			this->cursor = nullptr;
			this->head = nullptr;
		}
		//Check if cursor is last element
		else if (this->cursor->next == nullptr) {
			delete this->cursor;
			this->gotoPrior();
			this->cursor->next = nullptr;
			this->cursor = this->head;
		} 
		//Remove any other element
		else {
			this->gotoPrior();
			this->cursor->next = removed->next;
			delete removed;
			this->cursor = this->cursor->next;
		}
	}
	
}
//Retrieve
template<typename Type>
Type LinkedList<Type>::retrieve() const {

	//return (!this->empty()) ? this->cursor->element : NULL;

	if (!this->empty()) {
		return this->cursor->element;
	}
	else {
		return NULL;
	}
	
}
//gotoPrior
template<typename Type>
int LinkedList<Type>::gotoPrior() {
	//if not empty, and cursor is not pointing to the first element
	//designate element immediately before the current element as
	//the current element and return 1, else return 0
	if (!this->empty() && (this->cursor != this->head)) {
		for (ListElement* ptr{ this->head }; ptr != nullptr; ptr = ptr->next) {
			if (ptr->next == this->cursor) {
				this->cursor = ptr;
				break;
			}
		}
		return 1;
	}
	return 0;
}
//gotoNext
template<typename Type>
int LinkedList<Type>::gotoNext() {
	/*If the current element is not at the end of the list, then
	designates the element immediately after the current element
	as the current element and returns 1. Otherwise, returns 0.*/
	if (this->cursor != nullptr) {
		if (this->cursor->next != nullptr) {
			this->cursor = this->cursor->next;
			return 1;
		}
	}
	return 0;
}
//gotoBeginning
template<typename Type>
int LinkedList<Type>::gotoBeginning() {
	/*If a list is not empty, then designates the element at the
	beginning of the list as the current element and returns 1.
	Otherwise, returns 0.*/

	if (!this->empty()) {
		this->cursor = this->head;
		return 1;
	}
	return 0;
}
//clear
template<typename Type>
void LinkedList<Type>::clear() {
	/*Removes all the elements in a list and deallocates associated
	dynamic memory.*/
	ListElement* tmp;
	while (this->head != nullptr) {
		tmp = this->head;
		head = head->next;
		delete tmp;
	}
	this->head = nullptr;
	this->cursor = nullptr;
}
//empty
template<typename Type>
int LinkedList<Type>::empty() const {

	return (this->head == nullptr) ? 1 : 0;
	/* if (this->head == nullptr) {
		return 1;
	}
	else {
		return 0;
	}*/
}