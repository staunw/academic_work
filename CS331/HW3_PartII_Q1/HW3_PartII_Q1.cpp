// HW3 Part II, Q1
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 3, PARTII, QUESTION1
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/25/2019
*/

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main() {
	// ask user for string and store in character array
	cout << "Enter a string: ";
	string input; // string to hold input string
	cin >> input;

	// get length of string
	int length{ input.cend() - input.cbegin() };
	// display length of string
	cout << "length is: " << length << endl;

	// print string using twice the length as field with
	int width{ 2 * static_cast<int>(length) };
	cout << setw(width) << input << endl;
}
