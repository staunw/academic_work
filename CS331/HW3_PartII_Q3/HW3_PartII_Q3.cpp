// HW3 Part II, Q3
// Point test program.
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 3, PARTII, QUESTION3
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/25/2019
*/
#include <iostream>
#include <fstream>
using namespace std;

// Declaration of Invtry structure
const int DESC_LEN = 31;
const int DATE_LEN = 11;
struct Invtry
{
	char desc[DESC_LEN];
	int qty;
	double wholeSale;
	double retail;
	char date[DATE_LEN];
};

// Function Prototypes
void addRecord();
void viewRecord();
void changeRecord();

fstream inventory;



int main()
{
	int choice;

	do
	{
		// Display menu
		cout << "\n1. Add a new record\n";
		cout << "2. View an existing record\n";
		cout << "3. Change an existing record\n";
		cout << "4. Exit\n\n";

		// Get user's choice
		do
		{
			cout << "Enter your choice (1-4): ";
			/* FILL */
			cin >> choice;

		} while (choice < 1 || choice > 4);    //FILL

		// Process the user's choice
		switch (choice)
		{
		case 1:
			addRecord();
			break;
		case 2:
			viewRecord();
			break;
		case 3:
			changeRecord();
		}
	} while (choice != 4);
	return 0;

}

//********************************
//  Implement   addRecord()
//  - Add a record.
//********************************
void addRecord()
{
	Invtry record;

	// Fill - Open file
	inventory.open("inventory.dat", ios::out | ios::app | ios::binary);

	if (!inventory)    //FILL
	{
		cout << "Error opening file.\n";
		return;
	}
	cin.get();

	// Get description
	cout << "\nEnter the following inventory data:\n";
	cout << "Description: ";
	cin.getline(record.desc, DESC_LEN);                          //FILL

	// Get quantity
	do
	{
		cout << "Quantity: ";

		cin >> record.qty;
		//FILL
		if (record.qty < 0)
			cout << "Enter a positive value, please.\n";
	} while (record.qty < 0);              //FILL

	// Get wholesale cost
	   /* FILL */
	do
	{
		cout << "Wholesale cost: ";

		cin >> record.wholeSale;
		//FILL
		if (record.wholeSale < 0)
			cout << "Enter a positive value, please.\n";
	} while (record.wholeSale < 0);


	// Get retail price
		/* FILL */
	do
	{
		cout << "Retail price: ";

		cin >> record.retail;
		//FILL
		if (record.retail < 0)
			cout << "Enter a positive value, please.\n";
	} while (record.retail < 0);


	// Get date
	cout << "Date added to inventory: ";
	cin >> record.date;                      //FILL

	// Write the inventory record to the file

	inventory.write(reinterpret_cast<const char*>(&record), sizeof(Invtry));                           //FILL

	// Check if successs
	if (!inventory)   //FILL
		cout << "Error writing record to file.\n";
	else
		cout << "record written to file.\n\n";
	inventory.close();
}

// *********************************
//   Implement  viewRecord()
//  - View a record.
// *********************************
void viewRecord()
{
	Invtry record;
	long recNum;

	// Open file for reading
	/* FILL  */
	inventory.open("inventory.dat", ios::in);
	// Get number of record to view and seek to it
	cout << "\nEnter the record number of the item: ";
	cin >> recNum;
	// FILL -  Locate the corresponding record in the file.
	inventory.clear();

	inventory.seekg(recNum * (__int64)sizeof(Invtry), inventory.beg);

	//YOU NEED TO READ FIRST AND THEN CHECK FOR THE EOF
	inventory.read((char*)& record, sizeof(Invtry));

	// Check if success when seeking
	if ( inventory.eof() ) //FILL
	{
		cout << "\nError locating record.\n";
		inventory.close();
		return;
	}
	// CANNOT CATCH THE EOF HERE
	// FILL - Read the record
	//inventory.read((char*)& record, sizeof(Invtry));
	inventory.close();

	// Display the record read
	/* FILL  */
	cout << "Current record contents:\n"
		<< "Description: " << record.desc << "\n"
		<< "Quantity: " << record.qty << "\n"
		<< "Wholesale cost: " << record.wholeSale << "\n"
		<< "Retail price: " << record.retail << "\n"
		<< "Date added: " << record.date << endl;

	cout << "Press any key to continue...\n ";
	cin.get();
}

// *******************************
//  Implement changeRecord()
//  -- Change a record               *
// *******************************
void changeRecord()
{
	Invtry record;
	long recNum;

	// FILL - Open the file

	inventory.open("inventory.dat", ios::in | ios::out | ios::binary);

	if (!inventory) //FILL
	{
		cout << "Error opening file.\n";
		return;
	}

	//  Find out which record to alter and seek to it
	cout << "Enter the record number of the item: ";
	/* FILL */
	cin >> recNum;

	inventory.seekg(recNum * (__int64)sizeof(Invtry));

	cout << inventory.eof() << endl;
	if (false) {
		cout << "\nError locating record.\n";
		inventory.close();
		return;
	}
	inventory.read((char*)& record, sizeof(Invtry));
	// Display the  record
	cout << "Current record contents:\n";
	cout << "Description: " << record.desc << "\n"
		<< "Quantity: " << record.qty << "\n"
		<< "Wholesale cost: " << record.wholeSale << "\n"
		<< "Retail price: " << record.retail << "\n"
		<< "Date added: " << record.date << endl;


	// Allow user to edit the record
	cout << "Enter the new data:\n";
	cin.get();
	cout << "Description: ";
	cin.getline(record.desc, DESC_LEN);  //FILL
	cout << "Quantity: ";
	cin >> record.qty;                        //FILL
	cout << "Wholesale cost: ";
	cin >> record.wholeSale;                        //FILL
	cout << "Retail price: ";
	cin >> record.retail;                        //FILL
	cout << "Date added to inventory: ";
	cin >> record.date;                        //FILL

	// Write the record back to the file
	/*  Fill  */
	inventory.seekp(recNum * (__int64)sizeof(Invtry));

	inventory.write(reinterpret_cast<const char*>(&record), sizeof(Invtry));

	inventory.close();
	cout << "Press any key to continue... ";
	cin.get();
}


