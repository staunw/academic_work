/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION4
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

#include <iostream>
#include <iomanip>

int main() {
	const double PRODUCT_1_PRICE = 2.98;    //the cost for product1
	const double PRODUCT_2_PRICE = 4.50;    //the cost for product2
	const double PRODUCT_3_PRICE = 9.98;    //the cost for product3
	const double PRODUCT_4_PRICE = 4.49;    //the cost for product4
	const double PRODUCT_5_PRICE = 6.87;    //the cost for product5

	int product{ 0 };         //The product
	int quantitySold{ 0 };    //The amount sold of said product
	double sumProduct1{ 0 };  //The total amount of product 1 sold
	double sumProduct2{ 0 };  //The total amount of product 2 sold
	double sumProduct3{ 0 };  //The total amount of product 3 sold
	double sumProduct4{ 0 };  //The total amount of product 4 sold
	double sumProduct5{ 0 };  //The total amount of product 5 sold

	while (product != -1) {

		//ask user for the product number and quantity sold
		std::cout << "Enter the product number (or -1 to exit) and quantity sold: " << std::endl;
		//get user input
		std::cin >> product >> quantitySold;
		//use switch statement to calculate sums
		switch (product) {
		case 1:
			sumProduct1 += PRODUCT_1_PRICE * quantitySold;
			break;
		case 2:
			sumProduct2 += PRODUCT_2_PRICE * quantitySold;
			break;
		case 3:
			sumProduct3 += PRODUCT_3_PRICE * quantitySold;
			break;
		case 4:
			sumProduct4 += PRODUCT_4_PRICE * quantitySold;
			break;
		case 5:
			sumProduct5 += PRODUCT_5_PRICE * quantitySold;
			break;
		default:;
		}
	}
	//Display results
	std::cout << "Total sold for product 1 $" << std::fixed << std::setprecision(2) << sumProduct1 << std::endl;
	std::cout << "Total sold for product 2 $" << std::fixed << std::setprecision(2) << sumProduct2 << std::endl;
	std::cout << "Total sold for product 3 $" << std::fixed << std::setprecision(2) << sumProduct3 << std::endl;
	std::cout << "Total sold for product 4 $" << std::fixed << std::setprecision(2) << sumProduct4 << std::endl;
	std::cout << "Total sold for product 5 $" << std::fixed << std::setprecision(2) << sumProduct5 << std::endl;
	std::cout << "Total sold for all products $" << std::fixed << std::setprecision(2)
		<< (sumProduct1 + sumProduct2 + sumProduct3 + sumProduct4 + sumProduct5) << std::endl;

	return 0;
}
