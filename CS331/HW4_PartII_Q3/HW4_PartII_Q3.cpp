// ****************************************************************
// Homework#4, Part II, Q3
// ****************************************************************
/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 4, PARTII, QUESTION3
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		8/02/2019
*/

#include <iostream>
#include <string>

using namespace std;

class StrExcept{ };   //String Out of Bounds Exception

// COMPLETE
class BCheckString: public string
{
public:
	BCheckString(string s): string(s) { }		//Constructor, uses initialization list
	char operator[] (int k) {
		//check if k is less than zero or greater 
		//than or equal to the lenght of the string
		if (k < 0 || k >= this->length()) {
			throw StrExcept();		//Throw StrExcept
		}
		else {
			return this->at(k);		//Return the character at k
		}
	}
};

int main()
{
	//Explain program to user
	cout << "This program demonstrates bounds checking on string object.";

	//Get string from user and create boundCheck string object
	cout << "\nEnter a string: ";
	string str;
	getline(cin, str);
	BCheckString h(str);

	//Let user access characters at specified positions in the string
	//COMPLETE with try and catch blocks


	cout << "Legitimate string positions are: " << 0 << ".." << h.length() - 1 << endl;
	//Loop
	for (int k = 1; k <= 5; k++)
	{
		//Ask for position
		cout << "Enter an integer describing a position inside or outside the string: ";
		//Try to access element
		try {
			int pos;
			cin >> pos;
			cout << "The character at position " << pos << " is " << h[pos] << endl;
		}
		//Catch out of bounds exception
		catch (const StrExcept & stringException){
			cout << "A string out of bounds exception occured, " 
				<< "You entered a number out of bounds. "<<std::endl;
		}
		
	}

	return 0;
}

