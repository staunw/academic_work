/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION3
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

#include <iostream>
#include <iomanip>
#include <limits>

double validInput(void); //Function to check user entered a valid number

int main() {
	int accountNumber{ 0 };       //the customer's account number
	double balance{ 0.0 };        //the customer's balance
	double totalCharged{ 0.0 };   //the total amount charged by the customer
	double totalCredits{ 0.0 };   //the total credits applied to the customer
	double creditLimit{ 0.0 };    //the customer's credit limit

	//Infinite loop
	while (true) {
		//get customer's account number
		std::cout << "Enter account number (or -1 to quit): ";
		accountNumber = validInput();
		//check if customer entered -1
		if (accountNumber == -1) {
			break;  //exits infinite loop if customer entered -1
		}
		//get beginning balance
		std::cout << "Enter beginning balance: ";
		balance = validInput();
		//get total charged by the customer
		std::cout << "Enter total charges: ";
		totalCharged = validInput();
		//get total credits applied by the customer
		std::cout << "Enter total credits: ";
		totalCredits = validInput();
		//get credit limit
		std::cout << "Enter credit limit: ";
		creditLimit = validInput();
		//calculate new balance and display
		balance += totalCharged - totalCredits;
		std::cout << "New balance is " << std::fixed << std::setprecision(2) << balance << std::endl;
		//display account number
		std::cout << "Account:" << std::setw(9) << accountNumber << std::endl;
		//display credit limit
		std::cout << "Credit limit:" << std::setw(8) << std::fixed << std::setprecision(2) << creditLimit << std::endl;
		//display the new balance
		std::cout << "Balance:" << std::setw(13) << std::fixed << std::setprecision(2) << balance << std::endl;
		//check if balance is greater than the credit limit
		//if balance is greater, inform customer that their credit limit has been exceeded
		if (balance > creditLimit) {
			std::cout << "Credit Limit Exceeded" << std::endl;
		}

		std::cout << std::endl;

	}
	return 0;
}
//get a valid numeric input from the customer
double validInput(void) {
	double x;
	std::cin >> x;
	while (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Invalid entry.  Enter a NUMBER: ";
		std::cin >> x;
	}
	return x;

}
