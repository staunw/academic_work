/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 1, PARTII, QUESTION2
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/5/2019
*/

#include <iostream>
#include <limits>
int validInput(void); //Function to check user entered a valid number

int main() {
	int mile;       //the miles driven per day
	int cost;       //the cost per gallon of gas
	int mpg;        //the miles per gallon of the vehicle
	int parking;    //the fees for parking
	int tolls;      //the amount of tolls

	//Ask user how many miles they drive per day and store it
	std::cout << "Enter miles driven per day: ";
	mile = validInput();
	//Ask the user the cost of gas and store it
	std::cout << "cost per gallon of gas (in cents): ";
	cost = validInput();
	//Ask the user the miles per gallon and store it
	std::cout << "Enter average miles per gallon: ";
	mpg = validInput();
	//Ask the user the Parking fees and store it
	std::cout << "Enter parking fees per day (in dollars): ";
	parking = validInput();
	//Ask the user the tolls paid and stoer it
	std::cout << "Enter tolls per day (in dollars): ";
	tolls = validInput();
	std::cout << std::endl;
	//Calculate the user's cost per day of driving to work and display it
	std::cout << "Cost is " << "$" << (mile * cost / mpg / 100 + parking + tolls) << std::endl;

	return 0;
}
//Function that only accepts a valid numeric entry from the console
//Purpose is to check that the user entered a valid number
int validInput() {
	int x;
	std::cin >> x;
	while (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "Invalid entry.  Enter a NUMBER: ";
		std::cin >> x;
	}
	return x;
}
