/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 4, PARTII, QUESTION1
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		8/02/2019
*/
#include <iostream>
#include <iterator>
#include <vector>
#include <fstream>

int main() {

	std::ifstream inFile{ "sampledata.txt", std::ios::in };		//open file for processing
	std::vector<int> intVector;									//vector of ints
	std::istream_iterator<int> iIterator (inFile);				//istream_iterator
	std::istream_iterator<int> end;								//end 

	//Check if file was opened successfully
	if (!inFile) {
		std::cerr << "File could not be opened" << std::endl;
		std::exit(EXIT_FAILURE);
	}
	//Process file
	else {
		//Copy file contents to vector
		std::copy(iIterator, end, std::back_inserter(intVector));
	}
	//Print out vector using copy
	std::cout << "The file contained: ";
	std::copy(intVector.begin(),
		intVector.end(),
		std::ostream_iterator<int>(std::cout, " "));
	
	//close file
	inFile.close();
	return 0;
}