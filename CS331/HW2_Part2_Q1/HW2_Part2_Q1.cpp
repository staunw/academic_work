/*
NAME:			Nick Stauffer
ASSIGNMENT:		HOMEWORK 2, PARTII, QUESTION1
COURSE:			CS331
INSTRUCTOR:		Dr. Jin Soung Yoo
DUE DATE:		7/16/2019
*/
#include <iostream>
#include <string>
#include <cstdlib>       // Needed to generate random numbers
#include <ctime>         // Needed to use the time function to seed the
// random number generator
using namespace std;

const int SIZE = 5;

// Function prototypes
bool getPlayerNumber(int[]);
void getWinningNumber(int[]);
int countMatches(int[], int[]);
string getNumberString(int[]);

int main()
{
	int winningDigits[SIZE],
		player[SIZE];

	int numMatches = 0;

	bool numberOK;

	// Call functions to get the user's lottery number and
	// to randomly generate the winning lottery number

	cout << "Enter the 5 digits of your lottery number, separated by blanks: ";
	numberOK = getPlayerNumber(player);
	while (!numberOK)
	{
		cout << "\nOnly digits between 0 and 9 are allowed.\n";
		cout << "Re-enter the 5 digits of your lottery number, separated by blanks: ";
		numberOK = getPlayerNumber(player);
	}

	getWinningNumber(winningDigits);

	// Call a function to count the matching digits
	numMatches = countMatches(player, winningDigits);

	// Report the results
	cout << "\nWinning number: " << getNumberString(winningDigits);
	cout << "\nYour number   : " << getNumberString(player);
	cout << "\n\nYou have " << numMatches << " matching digit(s).\n";

	return 0;
}


bool getPlayerNumber(int array[])
{
	//Check if only numbers were entered
	if (!(cin >> array[0] >> array[1] >> array[2] >> array[3] >> array[4])) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		return false;		//return false
	}
	//Check if only single digits were entered
	for (size_t i{ 0 }; i < SIZE; i++) {

		if (*(array + i) < 0 || *(array + i) > 9) {
			return false;	//return false
		}
	}

	return true;	//return true if both of the above passed
}


void getWinningNumber(int array[])
{
	//seed srand with time
	srand(static_cast<unsigned int>(time(0)));

	//fill each element with a random number, 0 - 9 inclusive
	for (size_t i{ 0 }; i < SIZE; i++) {
		*(array + i) = (rand() % 9);
	}


}


int countMatches(int array1[], int array2[])
{
	int matches{ 0 };	//Number of matches

	//loop and count matches
	for (size_t i{ 0 }; i < SIZE; i++) {
		if (array1[i] == array2[i]) {
			matches++;
		}
	}

	return matches;
}


string getNumberString(int array[])
{

	string numbers{ "" }; //string to return

	//loop and add the numbers to the string
	for (size_t i{ 0 }; i < SIZE; i++) {
		numbers += to_string(array[i]);
		if (i != (SIZE - 1)) {
			numbers += " ";
		}
	}
	return numbers;
}