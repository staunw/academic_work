#include <stdio.h>
#include <stdlib.h>

void generatePrimes(int lowerBound, int upperBound){
  int isPrime[upperBound + 1];
  int index1, index2, index3;
  //sets the first two indicies to 0 (false as they are not prime).
  isPrime[0] = 0;
  isPrime[1] = 0;
  isPrime[2] = 1;
  //loops through the array and sets all elements to 1 (true) as they are potentially prime.
  for(index1 = 3; index1 <= upperBound; index1+=2){
    isPrime[index1] = 1;
  }
  //start at index 2, as 0 and 1 are already not prime
  index3 = 2;
  //loop through to find all prime numbers in the range
  while(index3 * index3 <= upperBound){
    //loop through the array to find the next prime. Finds the next prime if the current value is 1.
    for(index1 = index3; index1 <= upperBound; index1++){
      //if the value is 1 (true) then the number is prime and breaks out of the loop
      if(isPrime[index1]){
        break;
      }
    }
    //for the first iteration, finds all multiples of 2, then the multiples of 3 and 
    //continues to loop based on the which multiple is the current index.
    //sets all indicies of the multiples of the current index to false
    for(index2 = 2*index1; index2 <= upperBound; index2 = index2 + index1){
      isPrime[index2] = 0;
    }
    //set up the next iteration of the while loop
    index3 = index1+1;
  }
  int i;
  //print out all primes that were found
  for(i = lowerBound; i <= upperBound; i++){
    if(isPrime[i]){
      printf("%d\n", i);
    }

  }
}
int main ( int argc, char *argv[] ){

    if(argc != 3) {
        printf("Only enter two integer arguments, please");
    }
    else{
        int lowerBound = atoi(argv[1]);
        int upperBound = atoi(argv[2]);
        generatePrimes(lowerBound, upperBound);
    }



}
