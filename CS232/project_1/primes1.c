/*
NAME:			    Nick Stauffer
ASSIGNMENT:		Project 1
COURSE:			  CS232
INSTRUCTOR:		Max Fowler
DUE DATE:		  9/20/2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void generatePrimes(int lowerBound, int upperBound){
  int isPrime[upperBound + 1];
  int index;
  //sets the first two indicies to 0 (false as they are not prime).
  isPrime[0] = 0;
  isPrime[1] = 0;
  
  //loops through the array and sets all elements after the first 2 to 1 (true) as they are potentially prime.
  for(index = 2; index <= upperBound; index++){
    isPrime[index] = 1;
  }
  //Sieve of Eratosthenes
  int notPrimeMultiples;      //index of all multiples of non-primes
  int max = sqrt(upperBound); //max number of iterations of the sieve
  for(index = 2; index <= max; index++){
    if(isPrime[index]){
      for(notPrimeMultiples = index * index; notPrimeMultiples <= upperBound; notPrimeMultiples += index){
        isPrime[notPrimeMultiples] = 0;
      }
    }
  }


  int primes;
  //print out all primes that were found from lower bound to upper bound
  for(primes = lowerBound; primes <= upperBound; primes++){
    if(isPrime[primes]){
      printf("%d\n", primes);
    }

  }
}
int main ( int argc, char *argv[] ){

    if(argc != 3) {
        printf("Only enter two integer arguments, please");
    }
    else{
        int lowerBound = atoi(argv[1]);
        int upperBound = atoi(argv[2]);
        generatePrimes(lowerBound, upperBound);
    }



}
