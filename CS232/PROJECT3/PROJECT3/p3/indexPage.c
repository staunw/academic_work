/*
NAME:		    Nick Stauffer
ASSIGNMENT:	Project 3
COURSE:		  CS232
INSTRUCTOR:	Max Fowler
DUE DATE:	  November 5, 2018
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define ALPH_SIZE 26
#define TEMP_BUFFER 100
#define CHAR_TO_INDEX(c) ((int)c - (int)'a')
/**
 * Struct for Trie
 **/
struct Trie{
  int isLeaf;
  struct Trie *character[ALPH_SIZE];
  int occurences;
};

/* NOTE: int return values can be used to indicate errors (typically non-zero)
   or success (typically zero return value) */
struct Trie* getNewTrieNode(void);

struct Trie* indexPage(const char* url);

int addWordOccurrence(struct Trie *head, const char*);

void printTrieContents(struct Trie *head, char str[], int level, int lf);

int freeTrieMemory(struct Trie*);

int getText(const char* srcAddr, char* buffer, const int bufSize);

int main(int argc, char** argv){
  /* TODO: write the (simple) main function

  /* argv[1] will be the URL to index, if argc > 1 */
  char *url;
  struct Trie *head;
  
  if(argc > 1){
    url = argv[1];
  }else{
    printf("Improper arguments\n");
    exit(EXIT_FAILURE);
  }
  printf("%s\n", url);
  head = indexPage(url);
  char buffer[TEMP_BUFFER];
  
  printTrieContents(head , buffer , 0, 0);
  int j = freeTrieMemory(head);
  return 0;
}

/* TODO: define the functions corresponding to the above prototypes */

/*
 indePage takes a string to a url and returns a pointer to the root of the trie structure.
 Initializes a buffer to the max letters to be processed from the website. Passes the buffer,
 url and buffer size to the getText() function and returns the count and the string created by
 that function. Uses a temporary buffer and parses the website buffer finding the individual
 strings in the buffer and places those into the temp buffer. If the string won't fit in the temp
 buffer, prints to console that there is not enough space in the buffer and exits the program. 
 Otherwise, adds the string to the Trie and returns the root when all words have been added.
*/
struct Trie* indexPage(const char* url){
  struct Trie* root = getNewTrieNode();
  const int BUFFER_SIZE = 300000;
  char buffer[BUFFER_SIZE];
  memset(buffer, '\0', BUFFER_SIZE);
  
  char temp[TEMP_BUFFER];
  memset(temp, '\0', TEMP_BUFFER);
  
  int count = getText(url,buffer,BUFFER_SIZE);
  
  int i, k, l;
  int j = 0;
  for(i = 0; i < count; i++){
    
    if(isalpha(buffer[i])){
      if(strlen(temp) >= TEMP_BUFFER - 1){
        printf("Error adding word to trie, Not enough space in the buffer\n");
        freeTrieMemory(root);
        exit(EXIT_FAILURE);
      }else{
        temp[j] = buffer[i];
        j++;
      }
      
    }else{
      j = 0;
      int i;
      for(i = 0; i < TEMP_BUFFER; i++){
        temp[i] = tolower(temp[i]);
      }
      if(strlen(temp) != 0){
        addWordOccurrence(root, temp);
        printf("\t%s\n", temp);
        memset(temp, '\0', TEMP_BUFFER);
      }

    }
    
  }
  return root;
}
/*
 addWordOccurence() takes a parameter for the root of the Trie and a char* to a string.
 The function iterates until the end of the word to be added to the Trie by way of strlen
 and sets index to the integer value of the letter it represents. Finally sets leaf to true
 and increments occurences of that word.
*/
int addWordOccurrence(struct Trie *head, const char* str){
  int level;
  int length = strlen(str);
  int index;
  struct Trie *curr = head;
  
  for(level = 0; level < length; level++){
    index = CHAR_TO_INDEX(str[level]);
    if(!curr->character[index]){
      curr->character[index] = getNewTrieNode();
    }
    curr = curr->character[index];
  }
  curr->isLeaf = 1;
  if(curr->isLeaf){
    curr->occurences += 1;
  }
}
/*
 printTrieContents is a void function and requires a pointer to the root of the trie,
 the parameter for the array in the trie and two integer values for the level and leaf
 value. Uses recursion to find the strings in the Trie based on the isLeaf value in the Trie.
 And prints each string to console.
*/
void printTrieContents(struct Trie *head, char str[], int level, int lf){
    int leafIndex = lf;
  
    if (head->isLeaf == 1) { 
        str[level] = '\0'; 
        printf("%s: %d\n", str, head->occurences);
    } 
  
    int i;
    for (i = 0; i < ALPH_SIZE; i++)  
    { 
        if (head->character[i]) { 
            str[level] = i + 'a'; 
            printTrieContents(head->character[i], str, level + 1, i); 
        } 
    } 
}
/*
 freeTrieMemory takes a struct for a parameters and returns an integer value to 
 determine the base case. Calls freeTrieMemory recursively in a loop until all
 members are freed and then frees the calling node.
*/
int freeTrieMemory(struct Trie *node){
  
  int i;
  if(!node){
    return 0;
  }
  for(i = 0; i<ALPH_SIZE; i++){
    freeTrieMemory(node->character[i]);
  }
  free(node);
}

/* You should not need to modify this function */
int getText(const char* srcAddr, char* buffer, const int bufSize){
  FILE *pipe;
  int bytesRead;

  snprintf(buffer, bufSize, "curl -s \"%s\" | python getText.py", srcAddr);

  pipe = popen(buffer, "r");
  if(pipe == NULL){
    fprintf(stderr, "ERROR: could not open the pipe for command %s\n",
	    buffer);
    return 0;
  }

  bytesRead = fread(buffer, sizeof(char), bufSize-1, pipe);
  buffer[bytesRead] = '\0';

  pclose(pipe);

  return bytesRead;
}
/*
 getNewTrieNode() takes no parameters and returns a pointer to a new Trie node.
 getNewTrieNode() creates a new Trie node by mallocing for the size of the struct
 and returns a returns a node will all fields set to the default value.
*/
struct Trie* getNewTrieNode(){
    struct Trie *node = NULL;
  
    node = (struct Trie *)malloc(sizeof(struct Trie));
    
    if(node){
      node->isLeaf = 0;
      int i;
      for (i = 0; i < ALPH_SIZE; i++){
        node->character[i] = NULL;
        node->occurences = 0;
      }
    }
    return node;
}
