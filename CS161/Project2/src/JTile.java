/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
import javax.swing.*;
import java.awt.Color;
/**
 * The JTile class extends the JButton class. A JTile represents a game piece on a Tic-Tac-Toe board. A JTile has 
 * methods for changing a JButton's image (to and "X" or and "O") and values to determine if the button has already
 * been selected and for setting a value (Strings "X" or "O"). 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class JTile extends JButton{
	
	private static final long serialVersionUID = 5969156996304344887L;
	private Icon defaultIcon;
	private String value = "";
	private boolean isUsed = false;
	private final ImageIcon iconX1 = new ImageIcon("assets/X1.png");
	private final ImageIcon iconO1 = new ImageIcon("assets/O1.png");
	private final ImageIcon iconX2 = new ImageIcon("assets/X2.png");
	private final ImageIcon iconO2 = new ImageIcon("assets/O2.png");
	private ImageIcon styleIconX;
	private ImageIcon styleIconO;
	/**
	 * No arg constructor
	 */
	public JTile() {
	
	}
	/**
	 * sets the icon on the JTile to the default icon which is a blank button.
	 */
	public void setIcon() {
		setIcon(defaultIcon);
	}
	/**
	 * sets the icon on the JTile to an "X" and sets the value field to "X".
	 */
	public void setX() {
		setIcon(styleIconX);
		this.value = "X";
	}
	/**
	 * sets the icon on the JTile to an "O" and sets the value field to an "O".
	 */
	public void setO() {
		setIcon(styleIconO);
		this.value = "O";
	}
	/**
	 * resets the icon to the default icon, sets the value of used to false and sets the value field to an empty string.
	 */
	public void resetValues() {
		this.value = "";
		setIcon(defaultIcon);
		setUsed(false);
	}
	/**
	 * getter for value field
	 * @return the value field
	 */
	public String getValue() {
		return this.value;
	}
	/**
	 * setter for the value field
	 * @param used a boolean representing if the JTile was used
	 */
	public void setUsed(boolean used) {
		this.isUsed = used;
	}
	/**
	 * a getter for the isUsed field
	 * @return the isUsed field
	 */
	public boolean getUsed() {
		return this.isUsed;
	}
	/**
	 * a setter for the imageIconX field. Sets the X image, as well as background of the JTiles, based on which style parameter was passed.
	 * if Style.Black is passed, sets the image icon to constant iconX1 and sets backgrounds to Color.BLACK. If Style.WHITE is passed,
	 * sets the image icon to constant iconX2 and sets the background color of the JTiles to Color.WHITE.
	 * @param style an enumerated type that determines which icon style to use.
	 */
	public void setStyleIconX(Style style) {
		if (style == Style.BLACK) {
			styleIconX = iconX1;
			setBackground(Color.BLACK);
		}else {
			styleIconX = iconX2;
			setBackground(Color.WHITE);
		}
	}
	/**
	 * a setter for the imageIconX field. Sets the O image, as well as background of the JTiles, based on which style parameter was passed.
	 * if Style.Black is passed, sets the image icon to constant iconO1 and sets backgrounds to Color.BLACK. If Style.WHITE is passed,
	 * sets the image icon to constant iconO2 and sets the background color of the JTiles to Color.WHITE.
	 * @param style an enumerated type that determines which icon style to use.
	 */
	public void setStyleIconY(Style style) {
		if (style == Style.BLACK) {
			styleIconO = iconO1;
			setBackground(Color.BLACK);
		}else {
			styleIconO = iconO2;
			setBackground(Color.WHITE);
		}
	}
}//End Class JTile
