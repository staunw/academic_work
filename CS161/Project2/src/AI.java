/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
import java.util.Random;
/**
 * The AI class represents a computer playing the game. Extended from the Player class.
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class AI extends Player{
	private final int INDEX_0 = 0;
	private final int INDEX_1 = 1;
	private final int INDEX_2 = 2;
	private final int INDEX_3 = 3;
	private final int INDEX_4 = 4;
	private final int INDEX_5 = 5;
	private final int INDEX_6 = 6;
	private final int INDEX_7 = 7;
	private final int INDEX_8 = 8;
	private JTile[] gameTiles;
	private Random randMove = new Random();
	
	
	/**
	 * AI constructor
	 * @param gameTiles an array representing the game board
	 */
	public AI(JTile[] gameTiles) {
		this.gameTiles = gameTiles;
	}
	/**
	 * An implementation of the abstract method from the player class. The makeMove method represents smart AI and attempts to play in an 
	 * unbeatable way. It accomplishes this in three stages. The first stage represents the first move of the computer. If the center is played,
	 * then the AI will select a corner. If the center is not played, then the AI will select the center. The second stage consists of the AI's
	 * second move. If the Human player selects the opposite corner from their first move (assuming they played a corner), the AI will select a 
	 * random edge to play. If the Human selects a corner first and then an edge, the AI will block the potential fork by playing the corner 
	 * adjacent to the human's X on the same side as the Human's corner. If the Human plays two edges, the AI will select the included corner.
	 * If none of those conditions are met, the AI will look for a winning move or look to block two Human moves that are in a row. And if those
	 * conditions are not met, the AI will select an open corner. The third stage consists of all the moves past the AI's second move. The AI
	 * will look to see if it can win. If it cannot win, then it will look to see if it will lose. If it will not lose it will play a random 
	 * available move.
	 * @param count an integer representing the current game count.
	 */
	@Override
	public void makeMove(int count) {
		int bestMove;
		int randomMove;
		
		if(count >= INDEX_0 && count <INDEX_2) {		 //AI's first move
			if (gameTiles[INDEX_4].getUsed()) {
				do {
					randomMove = randMove.nextInt(5) * 2;// generates an even positive number in the range 0 through 8 representing corners 
					
				}while(gameTiles[randomMove].getUsed());
				bestMove = randomMove;
			
			}else {
				bestMove = INDEX_4;						 //Plays the center.		
			}
			gameTiles[bestMove].doClick();
			
		}else if (count >=INDEX_2 && count < INDEX_4) {	//AI's second move
			
			if(gameTiles[INDEX_1].getValue().equals("X") 
				&& gameTiles[INDEX_5].getValue().equals("X")
				&& !gameTiles[INDEX_2].getUsed()) {
				bestMove = INDEX_2;
			}else if(gameTiles[INDEX_1].getValue().equals("X") 
					&& gameTiles[INDEX_3].getValue().equals("X")
					&& !gameTiles[INDEX_0].getUsed()) {
					bestMove = INDEX_0;
			}else if(gameTiles[INDEX_3].getValue().equals("X") 
					&& gameTiles[INDEX_7].getValue().equals("X")
					&& !gameTiles[INDEX_6].getUsed()) {
					bestMove = INDEX_6;
			}else if(gameTiles[INDEX_5].getValue().equals("X") 
					&& gameTiles[INDEX_7].getValue().equals("X")
					&& !gameTiles[INDEX_8].getUsed()) {
					bestMove = INDEX_8;
			}else if(gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) {
				
				do {
					randomMove = Math.abs((randMove.nextInt(5) * 2) - 1);//FIND AN ODD NUMBER BETWEEN 0 AND 8
				}while(gameTiles[randomMove].getUsed() == true);
				
				bestMove = randomMove;
			}else if(gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X")) {
				
				do {
					randomMove = Math.abs((randMove.nextInt(5) * 2) - 1);//FIND AN ODD NUMBER BETWEEN 0 AND 8
				}while(gameTiles[randomMove].getUsed() == true);
				
				bestMove = randomMove;
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X"))
					|| (gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X"))) {
				bestMove = INDEX_2;
			}else if((gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_3].getValue().equals("X"))
					|| (gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X"))) {
				bestMove = INDEX_0;
			}else if((gameTiles[INDEX_6].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X"))
					|| (gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X"))) {
				bestMove = INDEX_8;
			}else if((gameTiles[INDEX_8].getValue().equals("X") && gameTiles[INDEX_3].getValue().equals("X"))
					|| (gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X"))) {
				bestMove = INDEX_6;
			
			//Index 0
			}else if ((gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_2].getValue().equals("O") 
				|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
				|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
				&& (!gameTiles[INDEX_0].getUsed())) {
				bestMove = INDEX_0;
			//Index 1
			}else if ((gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O") 
					|| gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_2].getValue().equals("O")) 
					&& (!gameTiles[INDEX_1].getUsed())) {
					bestMove = INDEX_1;
			//Index 2
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_1].getValue().equals("O") 
					|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_5].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_2].getUsed())) {
					bestMove = INDEX_2;
			//Index 3
			}else if ((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O")) 
					&& (!gameTiles[INDEX_3].getUsed())) {
					bestMove = INDEX_3;
			//Index 4
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O")
					|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O")) 
					&& (!gameTiles[INDEX_4].getUsed())) {
					bestMove = INDEX_4;
			//Index5
			}else if ((gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O") 
					|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O")) 
					&& (!gameTiles[INDEX_5].getUsed())) {
					bestMove = INDEX_5;
			//Index6
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_3].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_7].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_6].getUsed())) {
					bestMove = INDEX_6;
			//Index7
			}else if ((gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_6].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_7].getUsed())) {
					bestMove = INDEX_7;
			//Index8
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O") 
					|| gameTiles[INDEX_6].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O")) 
					&& (!gameTiles[INDEX_8].getUsed())) {
					bestMove = INDEX_8;
					
			//Index 0
			}else if ((gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_2].getValue().equals("X") 
				|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
				|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
				&& (!gameTiles[INDEX_0].getUsed())) {
				bestMove = INDEX_0;
			//Index 1
			}else if ((gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X") 
					|| gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_2].getValue().equals("X")) 
					&& (!gameTiles[INDEX_1].getUsed())) {
					bestMove = INDEX_1;
			//Index 2
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_1].getValue().equals("X") 
					|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_5].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_2].getUsed())) {
					bestMove = INDEX_2;
			//Index 3
			}else if ((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X")) 
					&& (!gameTiles[INDEX_3].getUsed())) {
					bestMove = INDEX_3;
			//Index 4
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X")
					|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X")) 
					&& (!gameTiles[INDEX_4].getUsed())) {
					bestMove = INDEX_4;
			//Index5
			}else if ((gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X") 
					|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X")) 
					&& (!gameTiles[INDEX_5].getUsed())) {
					bestMove = INDEX_5;
			//Index6
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_3].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_7].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_6].getUsed())) {
					bestMove = INDEX_6;
			//Index7
			}else if ((gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_6].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_7].getUsed())) {
					bestMove = INDEX_7;
			//Index8
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X") 
					|| gameTiles[INDEX_6].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X")) 
					&& (!gameTiles[INDEX_8].getUsed())) {
					bestMove = INDEX_8;
			}else {
				do {
					randomMove = randMove.nextInt(5) * 2;//FINDS AN EVEN INTEGER BETWEEN 0 AND 8
					
				}while(gameTiles[randomMove].getUsed() == true);
				bestMove = randomMove;
			
			}
			gameTiles[bestMove].doClick();
		} else {										//Terminal stages of the game
			//Index 0
			if ((gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_2].getValue().equals("O") 
				|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
				|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
				&& (!gameTiles[INDEX_0].getUsed())) {
				bestMove = INDEX_0;
			//Index 1
			}else if ((gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O") 
					|| gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_2].getValue().equals("O")) 
					&& (!gameTiles[INDEX_1].getUsed())) {
					bestMove = INDEX_1;
			//Index 2
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_1].getValue().equals("O") 
					|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_5].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_2].getUsed())) {
					bestMove = INDEX_2;
			//Index 3
			}else if ((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_4].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O")) 
					&& (!gameTiles[INDEX_3].getUsed())) {
					bestMove = INDEX_3;
			//Index 4
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_6].getValue().equals("O") 
					|| gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O")
					|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O")) 
					&& (!gameTiles[INDEX_4].getUsed())) {
					bestMove = INDEX_4;
			//Index5
			}else if ((gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O") 
					|| gameTiles[INDEX_3].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O")) 
					&& (!gameTiles[INDEX_5].getUsed())) {
					bestMove = INDEX_5;
			//Index6
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_3].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_7].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_6].getUsed())) {
					bestMove = INDEX_6;
			//Index7
			}else if ((gameTiles[INDEX_1].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_6].getValue().equals("O") && gameTiles[INDEX_8].getValue().equals("O")) 
					&& (!gameTiles[INDEX_7].getUsed())) {
					bestMove = INDEX_7;
			//Index8
			}else if((gameTiles[INDEX_0].getValue().equals("O") && gameTiles[INDEX_4].getValue().equals("O") 
					|| gameTiles[INDEX_2].getValue().equals("O") && gameTiles[INDEX_5].getValue().equals("O") 
					|| gameTiles[INDEX_6].getValue().equals("O") && gameTiles[INDEX_7].getValue().equals("O")) 
					&& (!gameTiles[INDEX_8].getUsed())) {
					bestMove = INDEX_8;
					
			//Index 0
			}else if ((gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_2].getValue().equals("X") 
				|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
				|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
				&& (!gameTiles[INDEX_0].getUsed())) {
				bestMove = INDEX_0;
			//Index 1
			}else if ((gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X") 
					|| gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_2].getValue().equals("X")) 
					&& (!gameTiles[INDEX_1].getUsed())) {
					bestMove = INDEX_1;
			//Index 2
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_1].getValue().equals("X") 
					|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_5].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_2].getUsed())) {
					bestMove = INDEX_2;
			//Index 3
			}else if ((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_4].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X")) 
					&& (!gameTiles[INDEX_3].getUsed())) {
					bestMove = INDEX_3;
			//Index 4
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_6].getValue().equals("X") 
					|| gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X")
					|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X")) 
					&& (!gameTiles[INDEX_4].getUsed())) {
					bestMove = INDEX_4;
			//Index5
			}else if ((gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X") 
					|| gameTiles[INDEX_3].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X")) 
					&& (!gameTiles[INDEX_5].getUsed())) {
					bestMove = INDEX_5;
			//Index6
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_3].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_7].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_6].getUsed())) {
					bestMove = INDEX_6;
			//Index7
			}else if ((gameTiles[INDEX_1].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_6].getValue().equals("X") && gameTiles[INDEX_8].getValue().equals("X")) 
					&& (!gameTiles[INDEX_7].getUsed())) {
					bestMove = INDEX_7;
			//Index8
			}else if((gameTiles[INDEX_0].getValue().equals("X") && gameTiles[INDEX_4].getValue().equals("X") 
					|| gameTiles[INDEX_2].getValue().equals("X") && gameTiles[INDEX_5].getValue().equals("X") 
					|| gameTiles[INDEX_6].getValue().equals("X") && gameTiles[INDEX_7].getValue().equals("X")) 
					&& (!gameTiles[INDEX_8].getUsed())) {
					bestMove = INDEX_8;
						
			}else {
				do {
					bestMove = randMove.nextInt(9);	
				}while(gameTiles[bestMove].getUsed() == true);
			}
			gameTiles[bestMove].doClick();
		}
 	}//End method makeMove
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public int getWinCount() {
		return 0;
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public int getLossCount() {
		return 0;
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public int getDrawCount() {
		return 0;
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public void setWinCount(int win) {
		
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public void setLossCount(int loss) {
		
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public void setDrawCount(int draw) {
		
	}
	/**
	 * An implementation of the abstract method declared in Player. It is not used by the AI player
	 */
	@Override
	public String toString() {
		return null;
	}
	//Attempt at the minimax algorithm just the base cases. Kept in order to implement at a later date
//	public int miniMax(int player, JTile[] gameTiles) {
//		int score = 1;
//		int playerCount = player;
//		
//		if (GameWinner.checkWinner(gameTiles) && playerCount %2 != 0) {
//			score = 10;
//			//return score;
//		}
//		if (GameWinner.isTie(gameTiles)) {
//			score = 0;
//			//return score;
//		}
//		if (GameWinner.checkWinner(gameTiles) && playerCount %2 == 0) {
//			score = -10;
//			//return score;
//		}
//		for (int i = 0; i < gameTiles.length; i++) {
//			miniMax(i, gameTiles);
//		}	
//		
//		return score;
//	}

}//End Class AI
