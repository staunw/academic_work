/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
/**
 * The GUIDriver class holds the main method and creates an instance of TicTacToeGUI
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class GUIDriver {

	public static void main(String[] args) {
			new TicTacToeGUI();
	}

}//End Class GUIDriver
