/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
/**
 * The Player class is an abstract class representing the attributes of a player.
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public abstract class Player {
	
	private String name; //Name of the player
	/**
	 * No arg constructor
	 */
	public Player() {};
	/**
	 * Constructor that takes a name as a parameter
	 * @param name a String that sets the name field
	 */
	public Player(String name){
		this.name = name;
		
	}
	/**
	 * Returns the name of the player.
	 * @return name A string that returns the name field
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * an abstract method that returns the number of wins of the player
	 */
	public abstract int getWinCount();
	/**
	 * an abstract method that returns the number of losses of the player
	 */
	public abstract int getLossCount();
	/**
	 * an abstract method that returns the number of ties of the player
	 */
	public abstract int getDrawCount();
	/**
	 * an abstract method that sets the win count of the player
	 * @param win an integer representing a win
	 */
	public abstract void setWinCount(int win);
	/**
	 * an abstract method that sets the loss count of the player
	 * @param loss and integer representing a loss
	 */
	public abstract void setLossCount(int loss);
	/**
	 * an abstract method that sets the draw count of the player
	 * @param draw an integer representing a draw
	 */
	public abstract void setDrawCount(int draw);
	/**
	 * an abstract method used to get the AI to make a move
	 * @param count an integer representing the current turn count of the game
	 */
	public abstract void makeMove(int count);
	/**
	 * an abstract method for printing user stats
	 */
	public abstract String toString();
	
}//End class Player
