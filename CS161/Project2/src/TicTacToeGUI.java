/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
import javax.swing.*;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
/**
 * the TicTacToeGUI class extends JFrame. TicTacToeGUI is the primary class representing the playing of the game of Tic-Tac-Toe. This class is
 * is responsible for creating a JPanel and creating nine JTiles(number of playable spots on a tic-tac-toe board) in an array. It adds those JTiles to
 * the JFrame. Before the game is started, the user is allowed to select the appearance of the game board by selecting one of two styles. The player is
 * also able to choose between playing with an AI player or Human player. In addition, the JFrame adds a button to start the game. When the button is 
 * pressed, the user is shown a list of previous users and asked to enter a name. The user may enter a previous name or enter a new name. The stats are 
 * then set to reflect a returning player or new player. Once the game has started, the user may select a position to play, clear their stats, start a 
 * new game, view their stats or exit the application. The user may not change the AI or change the style of the gameboard once play has commenced. 
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class TicTacToeGUI extends JFrame{
	private final int GAME_BOARD = 9;
	private final int MAX_TURN_COUNT = 9;
	private static final long serialVersionUID = 1274647737849101165L;
	private JTile[] gameTiles = new JTile[GAME_BOARD];
	private int gameCount = 0;
	private JRadioButtonMenuItem style1;
	private JRadioButtonMenuItem style2;
	private boolean winner = false;
	private Player player1;
	private Player player2;
	private JRadioButtonMenuItem radioButtonHumanVHuman;
	private JRadioButtonMenuItem radioButtonHumanVComputer;
	private JMenu mnGame;
	private JMenu mnAppearance;
	private JPanel gamePanel;
	private JPanel panelButtons;
	private JButton buttonStartGame;
	private JMenuItem mntmExit;
	private JMenuItem mntmStatistics;
	private JMenuItem mntmNewGame;
	private JSeparator separator;
	private JMenuItem menuClearPlayerStats;
	/**
	 * Constructor that creates all of the GUI elements the player sees, instantiates the JTiles that represent the game pieces the user may select
	 * and calls the loadUserData method from the Human class. 
	 */
	public TicTacToeGUI() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(700,700);
		setTitle("Shall We Play a Game?");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic(KeyEvent.VK_F);
		mnFile.setToolTipText("Click to start a new game, see you win/loss/draw record, or exit");
		menuBar.add(mnFile);
		
		mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.setMnemonic(KeyEvent.VK_N);
		mntmNewGame.setToolTipText("Starts a new game. Sends the player to the starting state.");
		mnFile.add(mntmNewGame);
		mntmNewGame.addActionListener(new NewGameListener());
		
		mntmStatistics = new JMenuItem("Statistics");
		mnFile.add(mntmStatistics);
		mntmStatistics.setMnemonic(KeyEvent.VK_S);
		mntmStatistics.setToolTipText("View your current Wins/Losses/Draws.");
		mntmStatistics.setEnabled(false);
		mntmStatistics.addActionListener(new StatsListener());
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.setMnemonic(KeyEvent.VK_X);
		mntmExit.setToolTipText("Exit the application");
		mnFile.add(mntmExit);
		mntmExit.addActionListener(new ExitButtonListener());
		
		mnAppearance = new JMenu("Appearance");
		mnAppearance.setMnemonic(KeyEvent.VK_A);
		mnAppearance.setToolTipText("Click to change the style of the game");
		menuBar.add(mnAppearance);
		
		style1 = new JRadioButtonMenuItem("Style 1");
		style1.setMnemonic(KeyEvent.VK_1);
		style1.setToolTipText("Style with a digital theme");
		mnAppearance.add(style1);
		style1.addActionListener(new StyleListener());
		
		style2 = new JRadioButtonMenuItem("Style 2");
		style2.setMnemonic(KeyEvent.VK_2);
		style2.setToolTipText("Style with a graffiti theme");
		mnAppearance.add(style2);
		style2.addActionListener(new StyleListener());
		
		ButtonGroup radioMenuButtons = new ButtonGroup();
		radioMenuButtons.add(style1);
		radioMenuButtons.add(style2);
		
		mnGame = new JMenu("Game");
		mnGame.setMnemonic(KeyEvent.VK_G);
		mnGame.setToolTipText("Click to select playing against an AI or another Human.\nClear your stats");
		menuBar.add(mnGame);
		
		radioButtonHumanVComputer = new JRadioButtonMenuItem("Human versus Computer");
		radioButtonHumanVComputer.setMnemonic(KeyEvent.VK_C);
		radioButtonHumanVComputer.setToolTipText("Select to play against an AI opponent");
		mnGame.add(radioButtonHumanVComputer);
		radioButtonHumanVComputer.addActionListener(new ButtonPlayers());
		
		radioButtonHumanVHuman = new JRadioButtonMenuItem("Human versus Human");
		radioButtonHumanVHuman.setMnemonic(KeyEvent.VK_H);
		radioButtonHumanVHuman.setToolTipText("Select to play against another human");
		mnGame.add(radioButtonHumanVHuman);
		radioButtonHumanVHuman.addActionListener(new ButtonPlayers());
		
		ButtonGroup radioGameButtons = new ButtonGroup();
		radioGameButtons.add(radioButtonHumanVHuman);
		radioGameButtons.add(radioButtonHumanVComputer);
		radioButtonHumanVComputer.setSelected(true);
		
		separator = new JSeparator();
		mnGame.add(separator);
		
		menuClearPlayerStats = new JMenuItem("Clear Player stats");
		menuClearPlayerStats.setMnemonic(KeyEvent.VK_S);
		menuClearPlayerStats.setToolTipText("Clear your current Win/Loss/Draw record");
		mnGame.add(menuClearPlayerStats);
		getContentPane().setLayout(new BorderLayout(0, 0));
		menuClearPlayerStats.addActionListener(new ClearPlayerStatsListener());
		menuClearPlayerStats.setEnabled(false);
		
		panelButtons = new JPanel();
		getContentPane().add(panelButtons, BorderLayout.SOUTH);
		
		buttonStartGame = new JButton("Start Game");
		buttonStartGame.setMnemonic(KeyEvent.VK_S);
		buttonStartGame.setToolTipText("Click to start the game");
		panelButtons.add(buttonStartGame);
		buttonStartGame.addActionListener(new StartGameListener());
		
		gamePanel = new JPanel();
		getContentPane().add(gamePanel, BorderLayout.CENTER);
		gamePanel.setLayout(new GridLayout(3, 3, 0, 0));
		
		for (int i =0; i < gameTiles.length; i++) {
			gameTiles[i] = new JTile();
			gameTiles[i].setVisible(false);
			gameTiles[i].addActionListener(new JTileListener());
			gamePanel.add(gameTiles[i]);

		}
		setVisible(true);
		style1.doClick();
		radioButtonHumanVComputer.doClick();
		
		try {
			Human.loadUserData();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	}//End constructor
	/**
	 * NewGameListener implements ActionListener and its purpose is to wait until the New Game button is pressed. When the New Game button is pressed,
	 * the saveUserData method is called in the Human class. player1 and player2 are nulled and all gameTiles is reset. All appropriate menu items are
	 * set accessible or inaccessible depending on their starting values.
	 *
	 */
	private class NewGameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Human.saveUserData();
			} catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
			}
			player1 = null;
			player2 = null;
			
			for(int i = 0; i < gameTiles.length; i++) {
				gameTiles[i].setVisible(false);
				gameTiles[i].resetValues();
				gameTiles[i].setEnabled(true);
			}
			mnGame.setEnabled(true);
			menuClearPlayerStats.setEnabled(false);
			mnAppearance.setEnabled(true);
			panelButtons.setVisible(true);
			mntmStatistics.setEnabled(false);
			radioButtonHumanVHuman.setEnabled(true);
			radioButtonHumanVComputer.setEnabled(true);

		}
		
	}//End class NewGameListener
	/**
	 * StartGameListener implements ActionListener and its purpose is to wait for the Start Game button to be pressed. Once the button is pressed, 
	 * StartGameListener has three string variables to represent player 1's name, player 2's name, and all past players. The getUsers method from
	 * the Human class is called and the ArrayList is looped through to gather all the name in the ArrayList. The names are added to the players
	 * variable. If the player has selected the Human versus Computer, the names are displayed to the player and the player is asked to enter
	 * their name. If the player selects cancel, the game exits. Otherwise, the name is used to instantiate a Human object. The Human object is
	 * then compared to the names of the past players. If the name entered already exists, the temp variable is set to the player that was found
	 * and then the Human object in temp is removed from the ArrayList and player1 is added to the ArrayList. I found this method, while complex,
	 * was a solution to keeping persistence with past players. This process is also used for if Human versus Human button is selected. Finally,
	 * the gameTiles is set to be visible and all appropriate menus are set enabled or disabled.
	 */
	private class StartGameListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String namePlayer1 = "";
			String namePlayer2 = "";
			String players = "";
			Human temp = null;
			
			for(Human h : Human.getUsers()) {
				players += h.getName() + "\n";
			}
			
			if (radioButtonHumanVComputer.isSelected()) {
				
				while(namePlayer1.isEmpty()) {
					namePlayer1 = JOptionPane.showInputDialog(gamePanel, "Previous Players:\n" + players 
							+ "\nEnter a Name for Player 1");
					if (namePlayer1 == null) {
						System.exit(0);
					}
				}
				
				player1 = new Human(namePlayer1);
				player2 = new AI(gameTiles);
				
				
				for (Human h: Human.getUsers()) {
					if (h.getName().equals(player1.getName())) {
						player1.setWinCount(h.getWinCount());
						player1.setDrawCount(h.getDrawCount());
						player1.setLossCount(h.getLossCount());
						temp = h;
					}
					
				}
				Human.removeUser(temp);
				Human.addUser((Human) player1);
				players = "";
				temp = null;
			}else {
				while(namePlayer1.isEmpty()) {
					namePlayer1 =  JOptionPane.showInputDialog(gamePanel,"Previous Players:\n" + players 
							+ "\nEnter a Name for Player 1");
					if (namePlayer1 == null) {
						System.exit(0);
					}
				}
				while(namePlayer2.isEmpty()) {
					namePlayer2 =  JOptionPane.showInputDialog(gamePanel,"Previous Players:\n" + players 
							+ "\nEnter a Name for Player 2");
					if (namePlayer2 == null) {
						System.exit(0);
					}
				}
				player1 = new Human(namePlayer1);
				player2 = new Human(namePlayer2);
				for (Human h: Human.getUsers()) {
					if (h.getName().equals(player1.getName())) {
						player1.setWinCount(h.getWinCount());
						player1.setDrawCount(h.getDrawCount());
						player1.setLossCount(h.getLossCount());
						temp = h;
					}
					
				}
				Human.removeUser(temp);
				
				for (Human h: Human.getUsers()) {
					if (h.getName().equals(player2.getName())) {
						player2.setWinCount(h.getWinCount());
						player2.setDrawCount(h.getDrawCount());
						player2.setLossCount(h.getLossCount());
						temp = h;
					}
					
				}
				Human.removeUser(temp);
				
				Human.addUser((Human) player1);
				Human.addUser((Human) player2);
				temp = null;
			}
			
			for (int i = 0; i < gameTiles.length; i++) {
				gameTiles[i].setVisible(true);
			}
			
			radioButtonHumanVHuman.setEnabled(false);
			radioButtonHumanVComputer.setEnabled(false);
			menuClearPlayerStats.setEnabled(true);
			mnAppearance.setEnabled(false);
			panelButtons.setVisible(false);
			mntmStatistics.setEnabled(true);
			players = "";
		}
		
	}//End class StartGameListener
	/**
	 * ButtonPlayers is used to determine if Human versus Computer is selected or Human versus Human is selected. It has no methods.
	 *
	 */
	private class ButtonPlayers implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
		}
		
	}//End class ButtonPlayers
	/**
	 * JTileListener implements ActionListener. JTileListener represents the core game play. The gameTiles array is looped through to determine which
	 * tile was selected. If the turn count is even, X has played a position and gameTile where x has played is set to x and set to used. gameCount is
	 * incremented by one and the the gameTiles are checked for a winning state. If X has won, a message dialog displays to inform the user X has won.
	 * winner is set to true and player1's win tally is incremented while player2's loss count is incremented. The player is asked if they wish to play
	 * again, if they select yes, a new game is started by calling resetGame method. Otherwise, the game is exited. If the game count is odd, then it is
	 * O's turn to play. Either the computer plays or a second Human player plays a spot. The above process is repeated for player2. If the game count
	 * reaches nine with no winner, a draw is declared. An appropriate message dialog is displayed and draw counts for both players are incremented.
	 * Select "No" after a ie game dialog for an easter egg.  
	 */
	private class JTileListener implements ActionListener {

	
		@Override
		public void actionPerformed(ActionEvent $) {
			
			for (int i = 0; i < gameTiles.length; i++) {
					if ($.getSource() == gameTiles[i] && !gameTiles[i].getUsed() ) {
						if (gameCount %2 == 0 ) {
							gameTiles[i].setX();
							gameTiles[i].setUsed(true);
							gameCount++;
							if (GameWinner.isWinner(gameTiles)) {
								for (int j = 0; j < gameTiles.length; j++) {
									if(!gameTiles[j].getUsed()) {
										gameTiles[j].setEnabled(false);
									}
								}
								JOptionPane.showMessageDialog(gamePanel, "X wins!");
								winner = true;
								player1.setWinCount(1);
								player2.setLossCount(1);
								int choice = JOptionPane.showConfirmDialog(gamePanel, "Would you like to play again?", 
										"Play Again?", JOptionPane.YES_NO_OPTION);
								if (choice == JOptionPane.YES_OPTION) {
									resetGame(gameTiles);
								}else {
									mntmExit.doClick();
								}
							}
							
							
						}else {
							gameTiles[i].setO();
							gameTiles[i].setUsed(true);
							gameCount++;
							if (GameWinner.isWinner(gameTiles)) {
								for (int j = 0; j < gameTiles.length; j++) {
									if(!gameTiles[j].getUsed()) {
										gameTiles[j].setEnabled(false);
									}
								}
								JOptionPane.showMessageDialog(gamePanel, "O wins!");
								winner = true;
								player2.setWinCount(1);
								player1.setLossCount(1);
								int choice = JOptionPane.showConfirmDialog(gamePanel, "Would you like to play again?", 
										"Play Again?", JOptionPane.YES_NO_OPTION);
								if (choice == JOptionPane.YES_OPTION) {
									resetGame(gameTiles);
								}else {
									mntmExit.doClick();
								}
							}
							
						}
						
						
					}	
			}
			if (gameCount %2 != 0 && gameCount < MAX_TURN_COUNT && winner != true) {
				player2.makeMove(gameCount);
			}
			if (gameCount == MAX_TURN_COUNT && winner != true) {
				player1.setDrawCount(1);
				player2.setDrawCount(1);
				int choice = JOptionPane.showConfirmDialog(gamePanel, "Game is a draw\nWould you like to play again?", 
						"Play Again?", JOptionPane.YES_NO_OPTION);
				if (choice == JOptionPane.YES_OPTION) {
					resetGame(gameTiles);
				}else {
					JOptionPane.showMessageDialog(gamePanel, "A strange game.\nThe only winning move is not to play.\n\nHow about a nice game of chess?");
					mntmExit.doClick();
				}
			}
		}
	}//End class JTileListener
	/**
	 * 	StyleListener implements ActionListener and its purpose is change the style of the game depending on which button is selected. 
	 */
	private class StyleListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent $) {
			
			if (style1.isSelected()) {
				
				for (int i = 0; i < gameTiles.length; i++) {
						gameTiles[i].setStyleIconX(Style.BLACK);
						gameTiles[i].setStyleIconY(Style.BLACK);
				}
				
			}
			if (style2.isSelected()) {
				for (int i = 0; i < gameTiles.length; i++) {
						gameTiles[i].setStyleIconX(Style.WHITE);
						gameTiles[i].setStyleIconY(Style.WHITE);
				}
			}
		}
		
	}//End class StyleListener
	/**
	 * ExitButtonListener implements ActionListener and its purpose is to wait for the exit button to be pressed. Once pressed, the saveUserData
	 * method is called from the Human class and the System.exit() is called.
	 */
	private class ExitButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Human.saveUserData();
			} catch (FileNotFoundException $) {
				
				$.printStackTrace();
			}
			
			System.exit(0);
			
		}
		
	}//End class ExitButtonListener 
	/**
	 * StatsListener implements ActionListener. Its purpose is to wait for the Statistics button to be pressed and displays the appropriate stats
	 * for all human players currently playing the game.
	 */
	private class StatsListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (radioButtonHumanVComputer.isSelected()) {
				JOptionPane.showMessageDialog(gamePanel, player1.toString(), "Stats", JOptionPane.PLAIN_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(gamePanel, player1.toString() + "\n\n" + player2.toString(), 
						"Stats", JOptionPane.PLAIN_MESSAGE);
			}
			
			
		}
		
	}//End class StatsListener
	/**
	 * ClearPlayerStatsListener implements ActionListener. Its purpose is to wait for the clear player stats button to be pressed and then to reset
	 * the stats of the current player(s) playing the game. Human versus Human play allows for either player's stats to be cleared or both.
	 */
	private class ClearPlayerStatsListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (radioButtonHumanVComputer.isSelected()) {
				player1.setWinCount(0);
				player1.setDrawCount(0);
				player1.setLossCount(0);
			}else {
				String nameToBeCleared;
				do {
					nameToBeCleared = JOptionPane.showInputDialog(gamePanel, "Type 'Player1'"
							+ " to clear Player1's stats\nType 'Player2' to clear Player2's stats\n" 
							+ "Type 'both' to clear both player's stats");
					if (nameToBeCleared.equalsIgnoreCase("Player1")) {
						player1.setWinCount(0);
						player1.setLossCount(0);
						player1.setDrawCount(0);
					}	
					if(nameToBeCleared.equalsIgnoreCase("Player2")) {
						player2.setWinCount(0);
						player2.setLossCount(0);
						player2.setDrawCount(0);
					}	
					if(nameToBeCleared.equalsIgnoreCase("both")) {
						player1.setWinCount(0);
						player1.setLossCount(0);
						player1.setDrawCount(0);
						player2.setWinCount(0);
						player2.setLossCount(0);
						player2.setDrawCount(0);
					}	
				}while(!nameToBeCleared.equalsIgnoreCase("Player1") 
						&& !nameToBeCleared.equalsIgnoreCase("Player2") 
						&& !nameToBeCleared.equalsIgnoreCase("Both"));
				
			}
			
			
		}
		
	}//End class ClearPlayerStatsListener
	/**
	 * resetGame is a private helper method used to reset gameTiles after playing a round. all appropriate fields and tiles are reset to their 
	 * default states and saveUserData is called from the Human class.
	 * @param array the array of JTiles used to play the game.
	 */
	private void resetGame(JTile[] array) {
		gameCount = 0;
		winner = false;
		
		for (int i = 0; i < array.length; i++) {
			array[i].resetValues();
			array[i].setVisible(true);
			array[i].setEnabled(true);
		}
		
		if (style1.isSelected()) {
			style1.doClick();
		}
		else {
			style2.doClick();
		}
		try {
			Human.saveUserData();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	}
	/**
	 * a getter for the gameTiles field.
	 * @return the JTile array
	 */
	public JTile[] getGameTiles() {
		return this.gameTiles;
	}
}//End class TicTacToeGUI
