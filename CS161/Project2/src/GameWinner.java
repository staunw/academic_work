/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
import java.awt.Color;

/**
 * The GameWinner class is used to determine if the current state of the game is a winner.
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */

public class GameWinner {
	/**
	 * GameWinner no arg constructor
	 */
	public GameWinner() {
		
	}
	/**
	 * The isWinner method takes an array as a parameter and determines if any row, column or diagonal has equal
	 * values. If they do, isWinner returns true and changes the background color of the row, column or diagonal that
	 * made it true.
	 * @return a boolean representing if the game has been won or not.
	 */
	public static boolean isWinner(JTile[] array) {
		boolean isWinner = false;
		//Row 1
		if (array[0].getValue().equals(array[1].getValue()) && array[1].getValue().equals(array[2].getValue())
				&& !array[0].getValue().isEmpty() && !array[1].getValue().isEmpty() && !array[2].getValue().isEmpty()) {
			isWinner = true;
			array[0].setBackground(Color.GREEN);
			array[1].setBackground(Color.GREEN);
			array[2].setBackground(Color.GREEN);
		}
		//Row 2
		else if(array[3].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[5].getValue())
				&& !array[3].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[5].getValue().isEmpty()) {
			isWinner = true;
			array[3].setBackground(Color.GREEN);
			array[4].setBackground(Color.GREEN);
			array[5].setBackground(Color.GREEN);
		}
		//Row 3
		else if(array[6].getValue().equals(array[7].getValue()) && array[7].getValue().equals(array[8].getValue())
				&& !array[6].getValue().isEmpty() && !array[7].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
			isWinner = true;
			array[6].setBackground(Color.GREEN);
			array[7].setBackground(Color.GREEN);
			array[8].setBackground(Color.GREEN);
		}
		//Column 1
		else if(array[0].getValue().equals(array[3].getValue()) && array[3].getValue().equals(array[6].getValue())
				&& !array[0].getValue().isEmpty() && !array[3].getValue().isEmpty() && !array[6].getValue().isEmpty()) {
			isWinner = true;
			array[0].setBackground(Color.GREEN);
			array[3].setBackground(Color.GREEN);
			array[6].setBackground(Color.GREEN);
		}
		//Column 2
		else if(array[1].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[7].getValue())
				&& !array[1].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[7].getValue().isEmpty()) {
			isWinner = true;
			array[1].setBackground(Color.GREEN);
			array[4].setBackground(Color.GREEN);
			array[7].setBackground(Color.GREEN);
		}
		//Column 3
		else if(array[2].getValue().equals(array[5].getValue()) && array[5].getValue().equals(array[8].getValue())
				&& !array[2].getValue().isEmpty() && !array[5].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
			isWinner = true;
			array[2].setBackground(Color.GREEN);
			array[5].setBackground(Color.GREEN);
			array[8].setBackground(Color.GREEN);
		}
		//Diagonal 2,4,6
		else if(array[2].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[6].getValue())
				&& !array[2].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[6].getValue().isEmpty()) {
			isWinner = true;
			array[2].setBackground(Color.GREEN);
			array[4].setBackground(Color.GREEN);
			array[6].setBackground(Color.GREEN);
		}
		//Diagonal 0,4,8
		else if(array[0].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[8].getValue())
				&& !array[0].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
			isWinner = true;
			array[0].setBackground(Color.GREEN);
			array[4].setBackground(Color.GREEN);
			array[8].setBackground(Color.GREEN);
		}else {
			isWinner = false;
		}
		
		return isWinner;
	}
	//Was to be used in miniMax for the base case just copy and paste from above without setting background colors
//	public static boolean checkWinner(JTile[] array) {
//		boolean isWinner = false;
//		if (array[0].getValue().equals(array[1].getValue()) && array[1].getValue().equals(array[2].getValue())
//				&& !array[0].getValue().isEmpty() && !array[1].getValue().isEmpty() && !array[2].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[3].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[5].getValue())
//				&& !array[3].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[5].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[6].getValue().equals(array[7].getValue()) && array[7].getValue().equals(array[8].getValue())
//				&& !array[6].getValue().isEmpty() && !array[7].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[0].getValue().equals(array[3].getValue()) && array[3].getValue().equals(array[6].getValue())
//				&& !array[0].getValue().isEmpty() && !array[3].getValue().isEmpty() && !array[6].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[1].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[7].getValue())
//				&& !array[1].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[7].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[2].getValue().equals(array[5].getValue()) && array[5].getValue().equals(array[8].getValue())
//				&& !array[2].getValue().isEmpty() && !array[5].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[2].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[6].getValue())
//				&& !array[2].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[6].getValue().isEmpty()) {
//			isWinner = true;
//		}else if(array[0].getValue().equals(array[4].getValue()) && array[4].getValue().equals(array[8].getValue())
//				&& !array[0].getValue().isEmpty() && !array[4].getValue().isEmpty() && !array[8].getValue().isEmpty()) {
//			isWinner = true;
//		}else {
//			isWinner = false;
//		}
//		
//		return isWinner;
//	}
	//Was to be used in miniMax as a base case for a tie
//	public static boolean isTie(JTile[] array) {
//		boolean isTie = false;
//		
//		if (!checkWinner(array)) {
//			isTie = true;
//		}
//		return isTie;
//		
//	}
}//End Class GameWinner
