/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * The Human class represents a Human player. Extends from the Player class.
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class Human extends Player {
	private final static String FILENAME = "userdata/UserStats.con";
	private int winCount;
	private int lossCount;
	private int drawCount;
	private static ArrayList<Human> players = new ArrayList<Human>();
	/**
	 * No arg constructor
	 */
	public Human() {
		
	}
	/**
	 * Constructor that takes a name parameter and calls the Player constructor to set the name field
	 * @param name a String representing the player's name
	 */
	public Human(String name) {
		super(name);
		
	}
	/**
	 * Constructor that takes a name, wins, losses and draws and sets the appropriate fields. This constructor is used to instantiate Humans that
	 * have previously played the game and are stored in the user stats file.
	 * @param name a String representing the name of the player
	 * @param wins an int representing the number of wins the player has
	 * @param losses an int representing the number of losses the player has
	 * @param draws an int representing the number of ties the player has
	 */
	public Human(String name, int wins, int losses, int draws) {
		super(name);
		this.winCount = wins;
		this.lossCount = losses;
		this.drawCount = draws;
	}
	/**
	 * returns the winCount field.
	 */
	public int getWinCount() {
		return winCount;
	}
	/**
	 * sets the winCount field. If 0 is passed as a parameter, resets the wins to 0, otherwise increments by 1.
	 */
	public void setWinCount(int winCount) {
		if (winCount == 0) {
			this.winCount = 0;
		}else {
			this.winCount ++;
		}
		
	}
	/**
	 * returns the lossCount field.
	 */
	public int getLossCount() {
		return lossCount;
	}
	/**
	 * sets the lossCount field. If 0 is passed as a parameter, resets the losses to 0, otherwise increments by 1.
	 */
	public void setLossCount(int lossCount) {
		if (lossCount == 0) {
			this.lossCount = 0;
		}else {
			this.lossCount ++;
		}

	}
	/**
	 * returns the drawCount field.
	 */
	public int getDrawCount() {
		return drawCount;
	}
	/**
	 * sets the drawCount field. If 0 is passed as a parameter, resets draws to 0, otherwise increments by 1.
	 */
	public void setDrawCount(int drawCount) {
		if(drawCount == 0) {
			this.drawCount = 0;
		}else {
			this.drawCount ++;
		}
		
	}
	/**
	 * An implementation of the abstract method in the Player class. It is unimplemented.
	 */
	@Override
	public void makeMove(int count) {
		
	}
	/**
	 * An implementation of the abstract method in the Player class. Creates a string with the name of the player, the current win count, the current
	 * loss count and the current draw count separated on separate lines.
	 */
	@Override
	public String toString() {
		
		return "Player: " + getName() 
				+ "\nWins: " + this.winCount 
				+ "\nLosses: " + this.lossCount 
				+ "\nDraws: " + this.drawCount;
	}
	/**
	 * The saveUserData method takes all the players in the ArrayList players field and writes them to a file with the FILENAME.
	 * @throws FileNotFoundException
	 */
	public static void saveUserData() throws FileNotFoundException{
		PrintWriter outputFile = new PrintWriter(FILENAME);
		for (Human h : players) {
			outputFile.println(h.getName());
			outputFile.println(h.getWinCount());
			outputFile.println(h.getLossCount());
			outputFile.println(h.getDrawCount());
		}
		outputFile.close();
	}
	/**
	 * the loadUserData method loads player data with the FILENAME constant. If the file does not exist, it calls the saveUserData method to create it.
	 * A scanner object reads the file and instantiates new Human objects and adds those to the ArrayList players field. 
	 * @throws FileNotFoundException
	 */
	public static void loadUserData() throws FileNotFoundException{
		String userName;
		int wins;
		int losses;
		int draws;
		Human loadedPlayer;
		File users = new File(FILENAME);
		
		if (!users.exists()) {
			saveUserData();
		}
		Scanner inFile = new Scanner(users);
		
		while (inFile.hasNextLine()) {
			userName = inFile.nextLine();
			wins = Integer.parseInt(inFile.nextLine());
			losses = Integer.parseInt(inFile.nextLine());
			draws = Integer.parseInt(inFile.nextLine());
			loadedPlayer = new Human(userName, wins, losses, draws);
			players.add(loadedPlayer);
		}
		 
		inFile.close();
	}
	/**
	 * The getUsers method returns a copy of the ArrayList of Human players. 
	 * @return a copy of the players field
	 */
	public static ArrayList<Human> getUsers() {
		ArrayList<Human> users = new ArrayList<Human>();
		for (Human h : players) {
			users.add(new Human(h.getName(), h.getWinCount(), h.getLossCount(), h.getDrawCount()));
		}
		return users;
	}
	/**
	 * Takes a human object as a parameter, if the name from the human parameter doesn't equal any in the players ArrayList, it adds it to the list.
	 * @param human a Human object
	 */
	public static void addUser(Human human) {
		boolean found = false;
		
		if (human!= null) {
			for (Human h : players) {
				if (human.getName().equals(h.getName())) {
					found = true;
				}
			}
			if (found == false) {
				players.add(human);
			}
		}
			
	}
	/**
	 * the removeUser takes a Human object as a parameter and searches the players ArrayList and removes the player from the ArrayList if the names
	 * match.
	 * @param human a Human object
	 */
	public static void removeUser(Human human) {
		Human temp = null;
		
		if(human != null) {
			for (Human h : players) {
				if (human.getName().equals(h.getName())) {
					temp = h;
				}
			}	
		}
		
		if (temp != null) {
			players.remove(temp);
		}
		
	}
	
}//End Class Human
