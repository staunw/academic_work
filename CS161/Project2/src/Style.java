/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 2
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		3/26/2018
 */
/**
 * The Style class is an enum with two values: White or Black. These are used to determine which style the game board
 * is to use.
 * 
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public enum Style {
	WHITE, BLACK
}//End enum Style
