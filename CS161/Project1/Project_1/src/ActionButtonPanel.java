import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class ActionButtonPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8570773802326816309L;
	private JPanel panelSouthButtons;
	private JPanel panelCalculate;
	private JPanel panelReset;
	private JPanel panelExit;
	private JButton calculateButton;
	private JButton resetButton;
	private JButton exitButton;
	
	public ActionButtonPanel() {
		panelSouthButtons = new JPanel();
		setLayout(new GridLayout(1, 0, 0, 0));
		
		panelCalculate = new JPanel();
		add(panelCalculate);
		
		calculateButton = new JButton("Calculate");
		calculateButton.setMnemonic(KeyEvent.VK_A);
		panelCalculate.add(calculateButton);
		
		panelReset = new JPanel();
		add(panelReset);
		
		resetButton = new JButton("Reset");
		resetButton.setMnemonic(KeyEvent.VK_E);
		panelReset.add(resetButton);
		
		panelExit = new JPanel();
		add(panelExit);
		
		exitButton = new JButton("Exit");
		exitButton.setMnemonic(KeyEvent.VK_X);
		panelExit.add(exitButton);
		
		calculateButton.addActionListener(new calculateButtonListener());
		resetButton.addActionListener(new resetButtonListener());
		exitButton.addActionListener(new exitButtonListener());
	}
	private class calculateButtonListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent $) {
			// TODO Auto-generated method stub
			
		}
	}
	private class resetButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent $) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private class exitButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent $) {
			System.exit(0);
			
		}
		
	}
	public JPanel getPanel() {
		return this.panelSouthButtons;
	}
}
