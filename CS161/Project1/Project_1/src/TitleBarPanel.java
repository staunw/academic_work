import java.awt.Color;

import javax.swing.*;

public class TitleBarPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8650417833894793574L;
	private JPanel panelTitle;
	private JLabel lblBunkerLoanCalculator;
	
	public TitleBarPanel() {
		panelTitle = new JPanel();
		setBackground(new Color(128, 0, 128));
		
		lblBunkerLoanCalculator = new JLabel("Bunker Loan Calculator");
		lblBunkerLoanCalculator.setForeground(Color.WHITE);
		lblBunkerLoanCalculator.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblBunkerLoanCalculator);
	}
	public JPanel getPanel() {
		return this.panelTitle;
	}
}
