import java.awt.GridLayout;

import javax.swing.*;

public class CombinedPanels extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9057007773386601656L;
	private JPanel combinedPanels;
	
	public CombinedPanels() {
		combinedPanels = new JPanel();
		setLayout(new GridLayout(2, 2, 0, 0));
		add(new PaymentInfoPanel());
		add(new LoanTermPanel());
		add(new FinancingInfoPanel());
		add(new OptionsPanel());
	}
	public JPanel getPanelMain() {
		return combinedPanels;
	}
}
