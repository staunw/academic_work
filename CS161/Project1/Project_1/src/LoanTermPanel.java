import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class LoanTermPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8053346930547188739L;
	private JPanel panelLoanTerm;
	private final String MONTHS_24_BUTTON_TEXT = "24 Months @ 4.5% Interest";
	private final String MONTHS_36_BUTTON_TEXT = "36 Months @ 5.5% Interest";
	private final String MONTHS_48_BUTTON_TEXT = "48 Months @ 6.5% Interest";
	private final String MONTHS_60_BUTTON_TEXT = "60 Months @ 7.0% Interest";
	private final double INTEREST_RATE_24_MONTHS = 4.5;
	private final double INTEREST_RATE_36_MONTHS = 5.5;
	private final double INTEREST_RATE_48_MONTHS = 6.5;
	private final double INTEREST_RATE_60_MONTHS = 7.0;
	private ButtonGroup radioButtonGroup;
	private JRadioButton monthsButton24;
	private JRadioButton monthsButton36;
	private JRadioButton monthsButton48;
	private JRadioButton monthsButton60;
	private double interestRate = INTEREST_RATE_24_MONTHS;
	
	public LoanTermPanel() {
		panelLoanTerm = new JPanel();
		setBorder(BorderFactory.createTitledBorder("Loan Term"));
		setLayout(new GridLayout(4, 0, 0, 0));
		
		monthsButton24 = new JRadioButton(MONTHS_24_BUTTON_TEXT);
		monthsButton24.setSelected(true);
		add(monthsButton24);
		
		monthsButton36 = new JRadioButton(MONTHS_36_BUTTON_TEXT);
		add(monthsButton36);
		
		monthsButton48 = new JRadioButton(MONTHS_48_BUTTON_TEXT);
		add(monthsButton48);
		
		monthsButton60 = new JRadioButton(MONTHS_60_BUTTON_TEXT);
		add(monthsButton60);
		
		radioButtonGroup = new ButtonGroup();
		radioButtonGroup.add(monthsButton24);
		radioButtonGroup.add(monthsButton36);
		radioButtonGroup.add(monthsButton48);
		radioButtonGroup.add(monthsButton60);
		
		monthsButton24.addActionListener(new RadioButtonListener());
		monthsButton36.addActionListener(new RadioButtonListener());
		monthsButton48.addActionListener(new RadioButtonListener());
		monthsButton60.addActionListener(new RadioButtonListener());
	}
	
	private class RadioButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent $) {
			if ($.getSource() == monthsButton24) {
				interestRate = INTEREST_RATE_24_MONTHS;
			}else if($.getSource() == monthsButton36) {
				interestRate = INTEREST_RATE_36_MONTHS;
			}else if($.getSource() == monthsButton48) {
				interestRate = INTEREST_RATE_48_MONTHS;
			}else {
				interestRate = INTEREST_RATE_60_MONTHS;
			}
			
		}
		
	}
	public JPanel getPanel() {
		return this.panelLoanTerm;
	}
	public double getIntrestRate() {
		return this.interestRate;
	}
}
