import java.awt.GridLayout;

import javax.swing.*;

public class FinancingInfoPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4021571528804114371L;
	private final String DEFAULT_VALUE = "0.0";
	private JPanel panelFinancingInformation;
	private JTextField textFieldBaseContractPrice;
	private JTextField textFieldDownPayment;
	private JTextField textFieldSalesTaxPercentage;
	
	
	public FinancingInfoPanel() {
		panelFinancingInformation = new JPanel();
		setBorder(BorderFactory.createTitledBorder("Financing Information"));
		setLayout(new GridLayout(3, 2, 0, 0));
		
		JLabel lblBaseContractPrice = new JLabel("Base Contract Price");
		add(lblBaseContractPrice);
		
		textFieldBaseContractPrice = new JTextField();
		textFieldBaseContractPrice.setText(DEFAULT_VALUE);
		add(textFieldBaseContractPrice);
		textFieldBaseContractPrice.setColumns(10);
		
		JLabel lblDownPayment = new JLabel("Down Payment");
		add(lblDownPayment);
		
		textFieldDownPayment = new JTextField();
		textFieldDownPayment.setText(DEFAULT_VALUE);
		add(textFieldDownPayment);
		textFieldDownPayment.setColumns(10);
		
		JLabel lblSalesTaxPercentage = new JLabel("Sales Tax Percentage");
		add(lblSalesTaxPercentage);
		
		textFieldSalesTaxPercentage = new JTextField();
		textFieldSalesTaxPercentage.setText("7.0");
		add(textFieldSalesTaxPercentage);
		textFieldSalesTaxPercentage.setColumns(10);
	}
	public JPanel getPanel() {
		return this.panelFinancingInformation;
	}
	public JTextField getTextFieldBaseContractPrice() {
		return textFieldBaseContractPrice;
	}
	public void setTextFieldBaseContractPrice(JTextField textFieldBaseContractPrice) {
		this.textFieldBaseContractPrice = textFieldBaseContractPrice;
	}
	public JTextField getTextFieldDownPayment() {
		return textFieldDownPayment;
	}
	public void setTextFieldDownPayment(JTextField textFieldDownPayment) {
		this.textFieldDownPayment = textFieldDownPayment;
	}
	public JTextField getTextFieldSalesTaxPercentage() {
		return textFieldSalesTaxPercentage;
	}
	public void setTextFieldSalesTaxPercentage(JTextField textFieldSalesTaxPercentage) {
		this.textFieldSalesTaxPercentage = textFieldSalesTaxPercentage;
	}
}
