import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class OptionsPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5104757773723808255L;
	private final String TEXT_AUTO_LOCK_DOOR = "Auto Lock Door - $21,000";
	private final String TEXT_CONCRETE = "SecuriSafe Ultra Concrete - $30,000";
	private final String TEXT_GUARD_ROBOT = "Guard Robot - $150,000";
	private final String TEXT_ROBOT_SAFETY = "Guard Robot Safety Protocols - $30,000";
	private final String TEXT_MEDICAL_ADDONS = "Medical Addons - $200,000";
	private final String BORDER_TITLE = "Bunker Options";
	private final int PRICE_AUTO_LOCK_DOOR = 21000;
	private final int PRICE_CONCRETE = 30000;
	private final int PRICE_GUARD_ROBOT = 150000;
	private final int PRICE_GUARD_ROBOT_SAFETY = 30000;
	private final int PRICE_MEDICAL_ADDONS = 200000;
	private JPanel panelBunkerOptions;
	private JCheckBox chckbxAutoLockDoor;
	private JCheckBox chckbxSecurisafeUltraConcrete;
	private JCheckBox chckbxGuardRobot;
	private JCheckBox chckbxGuardRobotSafety;
	private JCheckBox chckbxMedicalAddons;
	private int totalPrice = PRICE_CONCRETE;
	
	public OptionsPanel() {
		panelBunkerOptions = new JPanel();
		setBorder(BorderFactory.createTitledBorder(BORDER_TITLE));
		setLayout(new GridLayout(5, 0, 0, 0));
		
		chckbxAutoLockDoor = new JCheckBox(TEXT_AUTO_LOCK_DOOR);
		add(chckbxAutoLockDoor);
		
		chckbxSecurisafeUltraConcrete = new JCheckBox(TEXT_CONCRETE);
		chckbxSecurisafeUltraConcrete.setSelected(true);
		add(chckbxSecurisafeUltraConcrete);
		
		chckbxGuardRobot = new JCheckBox(TEXT_GUARD_ROBOT);
		add(chckbxGuardRobot);
		
		chckbxGuardRobotSafety = new JCheckBox(TEXT_ROBOT_SAFETY);
		add(chckbxGuardRobotSafety);
		
		chckbxMedicalAddons = new JCheckBox(TEXT_MEDICAL_ADDONS);
		add(chckbxMedicalAddons);
		
		chckbxAutoLockDoor.addItemListener(new CheckBoxListener());
		chckbxSecurisafeUltraConcrete.addItemListener(new CheckBoxListener());
		chckbxGuardRobot.addItemListener(new CheckBoxListener());
		chckbxGuardRobotSafety.addItemListener(new CheckBoxListener());
		chckbxMedicalAddons.addItemListener(new CheckBoxListener());
	}
	
	private class CheckBoxListener implements ItemListener{

		@Override
		public void itemStateChanged(ItemEvent $) {
			if ($.getSource() == chckbxAutoLockDoor) {
				if (chckbxAutoLockDoor.isSelected()) {
					totalPrice += PRICE_AUTO_LOCK_DOOR;
				}
				else {
					totalPrice -= PRICE_AUTO_LOCK_DOOR;
				}
			}
			if ($.getSource() == chckbxSecurisafeUltraConcrete) {
				if (chckbxSecurisafeUltraConcrete.isSelected()) {
					totalPrice += PRICE_CONCRETE;
				}
				else {
					totalPrice -= PRICE_CONCRETE;
				}
			}
			if ($.getSource() == chckbxGuardRobot) {
				if (chckbxGuardRobot.isSelected()) {
					totalPrice += PRICE_GUARD_ROBOT;
				}
				else {
					totalPrice -= PRICE_GUARD_ROBOT;
				}
			}
			if ($.getSource() == chckbxGuardRobotSafety) {
				if (chckbxGuardRobotSafety.isSelected()) {
					totalPrice += PRICE_GUARD_ROBOT_SAFETY;
				}
				else {
					totalPrice -= PRICE_GUARD_ROBOT_SAFETY;
				}
			}
			if ($.getSource() == chckbxMedicalAddons) {
				if (chckbxMedicalAddons.isSelected()) {
					totalPrice += PRICE_MEDICAL_ADDONS;
				}
				else {
					totalPrice -= PRICE_MEDICAL_ADDONS;
				}
			}
		}
		
	}
	public int getTotalPrice() {
		return this.totalPrice;
	}
}
