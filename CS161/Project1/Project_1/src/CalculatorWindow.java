import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

public class CalculatorWindow extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8116229125136818991L;
	private ActionButtonPanel abp;
	private TitleBarPanel tbp;
	private CombinedPanels cp;
	
	public CalculatorWindow() {
		setSize(new Dimension(650, 450));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setLayout(new BorderLayout());
		
		abp = new ActionButtonPanel();
		tbp = new TitleBarPanel();
		cp = new CombinedPanels();
		
		add(abp, BorderLayout.SOUTH);
		add(tbp, BorderLayout.NORTH);
		add(cp, BorderLayout.CENTER);
		
		setVisible(true);
	}
	public static void main (String[] sickTwistedFreak) {
		new CalculatorWindow();
	}
}
