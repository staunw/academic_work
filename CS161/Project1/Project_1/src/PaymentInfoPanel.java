import java.awt.GridLayout;

import javax.swing.*;

public class PaymentInfoPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -268806552118431369L;
	private final double DEFAULT_VALUE = 0.0;
	private final String BORDER_TITLE = "Payment Plan";
	private JPanel panelPaymentPlan;
	private JLabel lblTotalLoanAmount;
	private JLabel lblMonthlyPayment;
	private JLabel lblTotalPayment;
	private JTextField textFieldTotalLoanAmount;
	private JTextField textMonthlyPayment;
	private JTextField textFieldTotalPayment;
	
	public PaymentInfoPanel() {
		panelPaymentPlan = new JPanel();
		setBorder(BorderFactory.createTitledBorder(BORDER_TITLE));
		setLayout(new GridLayout(3, 2, 0, 0));
		
		lblTotalLoanAmount = new JLabel("Total Loan Amount");
		add(lblTotalLoanAmount);
		
		textFieldTotalLoanAmount = new JTextField();
		textFieldTotalLoanAmount.setBorder(null);
		textFieldTotalLoanAmount.setText("$" + DEFAULT_VALUE);
		textFieldTotalLoanAmount.setEditable(false);
		add(textFieldTotalLoanAmount);
		textFieldTotalLoanAmount.setColumns(10);
		
		lblMonthlyPayment = new JLabel("Monthly Payment");
		add(lblMonthlyPayment);
		
		textMonthlyPayment = new JTextField();
		textMonthlyPayment.setBorder(null);
		textMonthlyPayment.setText("$" + DEFAULT_VALUE);
		textMonthlyPayment.setEditable(false);
		add(textMonthlyPayment);
		textMonthlyPayment.setColumns(10);
		
		lblTotalPayment = new JLabel("Total Payment");
		add(lblTotalPayment);
		
		textFieldTotalPayment = new JTextField();
		textFieldTotalPayment.setBorder(null);
		textFieldTotalPayment.setEditable(false);
		textFieldTotalPayment.setText("$" + DEFAULT_VALUE);
		add(textFieldTotalPayment);
		textFieldTotalPayment.setColumns(10);
	}
	public JPanel getPanel() {
		return this.panelPaymentPlan;
	}
	public void setTextFieldTotalLoanAmount(JTextField textFieldTotalLoanAmount) {
		this.textFieldTotalLoanAmount = textFieldTotalLoanAmount;
	}
	public void setTextMonthlyPayment(JTextField textMonthlyPayment) {
		this.textMonthlyPayment = textMonthlyPayment;
	}
	public void setTextFieldTotalPayment(JTextField textFieldTotalPayment) {
		this.textFieldTotalPayment = textFieldTotalPayment;
	}
}
