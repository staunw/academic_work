import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundChanger {
	
	private static final File SOUND_FILE_BLACKOUT = new File("Audio/Imminent Blackout.wav");
	private static final File SOUND_FILE_DOUBTS = new File("Audio/Quixotic Doubts.wav");
	private static final File SOUND_FILE_OPINION = new File("Audio/Thirsty Opinion.wav");
	private static final File SOUND_FILE_IMPULSE = new File("Audio/Utopian Impulse.wav");
	
	private static final int SOUND_OPTION_BLACKOUT = 1;
	private static final int SOUND_OPTION_DOUBTS = 2;
	private static final int SOUND_OPTION_OPINION = 3;
	private static final int SOUND_OPTION_IMPULSE = 4;
	private static final int TOTAL_SOUND_OPTIONS = 4;
	private static Clip clip;
	private static AudioInputStream audioIn;
	private static boolean isPlaying = false;
	
	public static Clip getClip() {
		return clip;
	}
	
	private SoundChanger() {
		
	}
	public static void playSoundBlackout() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		try {
			quitPlayingSound();
		}catch (Exception e) {
			
		}finally {
		
			isPlaying = true;
			audioIn = AudioSystem.getAudioInputStream(SOUND_FILE_BLACKOUT);
			clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		
		}
	}
	public static void playSoundDoubts() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		try {
			quitPlayingSound();
		}catch (Exception e) {
			
		}
		finally {
			isPlaying = true;
			audioIn = AudioSystem.getAudioInputStream(SOUND_FILE_DOUBTS);
			clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		}

	}
	public static void playSoundOpinion() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		
		try {
			quitPlayingSound();
		}
		catch (Exception e) {
			
		}
		finally {
			isPlaying = true;
			audioIn = AudioSystem.getAudioInputStream(SOUND_FILE_OPINION);
			clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		}
		
		
	}
	public static void playSoundImpulse() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		
		try {
			quitPlayingSound();
		}
		catch (Exception e) {
			
		}
		finally {
			isPlaying = true;
			audioIn = AudioSystem.getAudioInputStream(SOUND_FILE_IMPULSE);
			clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
		}
		
		
		
	}
	public static void playRandomSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		try {
			quitPlayingSound();
		}
		catch (Exception e) {
			
		}
		finally {
			isPlaying = true;
			int sound;
			Random rand = new Random();
			sound = rand.nextInt(TOTAL_SOUND_OPTIONS) + 1;
			
			if (sound == SOUND_OPTION_BLACKOUT) {
				playSoundBlackout();
			}
			if (sound == SOUND_OPTION_DOUBTS) {
				playSoundDoubts();
			}
			if (sound == SOUND_OPTION_OPINION) {
				playSoundOpinion();
			}
			if (sound == SOUND_OPTION_IMPULSE) {
				playSoundImpulse();
			}
		}
		
		
	}
	public static void quitPlayingSound() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		
		
		clip.drain();
		clip.stop();
		audioIn.close();
		isPlaying = false;
		clip.close();
		
	}

	public static boolean isPlaying() {
		return isPlaying;
	}
	
}
