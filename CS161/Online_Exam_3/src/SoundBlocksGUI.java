import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;

import java.awt.event.KeyEvent;

public class SoundBlocksGUI extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6717575669736902579L;
	private final int HEIGHT = 750;
	private final int WIDTH = 750;
	private JMenu menuFile;
	private JMenuBar menuBar;
	private JMenu menuSound;
	private JMenu menuCredits;
	private JMenuItem menuItemExit;
	
	private JRadioButtonMenuItem menuButtonRandomSound;
	private JMenuItem menuItemCredits;
	private Ball b = new Ball(HEIGHT, WIDTH);
	private JMenuItem menuButtonBlackout;
	private JRadioButtonMenuItem menuButtonDoubts;
	private JRadioButtonMenuItem menuButtonOpinion;
	private AbstractButton menuButtonImpulse;

	public SoundBlocksGUI() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		setSize(new Dimension(HEIGHT, WIDTH));
		setBackground(Color.BLACK);
		setTitle("Sound Blocks");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		
		getContentPane().add(b, BorderLayout.CENTER);
		//getContentPane().add(new Ball(getHeight() -30, getWidth()), BorderLayout.CENTER);
		
		menuBar = new JMenuBar();
		menuBar.setOpaque(false);
		setJMenuBar(menuBar);
		
		menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuFile.setForeground(Color.WHITE);
		menuBar.add(menuFile);
		
		menuItemExit = new JMenuItem("Exit");
		menuItemExit.setMnemonic(KeyEvent.VK_X);
		menuItemExit.addActionListener(new ExitListener());
		menuFile.add(menuItemExit);
		
		menuSound = new JMenu("Sound");
		menuSound.setMnemonic(KeyEvent.VK_S);
		menuSound.setForeground(Color.WHITE);
		menuBar.add(menuSound);
		
		menuButtonBlackout = new JRadioButtonMenuItem("Imminent Blackout");
		menuSound.add(menuButtonBlackout);
		menuButtonBlackout.addActionListener(new SoundSetter());
		menuButtonBlackout.setMnemonic(KeyEvent.VK_B);
		
		menuButtonDoubts = new JRadioButtonMenuItem("Quixotic Doubts");
		menuSound.add(menuButtonDoubts);
		menuButtonDoubts.addActionListener(new SoundSetter());
		menuButtonDoubts.setMnemonic(KeyEvent.VK_Q);
		
		menuButtonOpinion = new JRadioButtonMenuItem("Thirsty Opinion");
		menuSound.add(menuButtonOpinion);
		menuButtonOpinion.addActionListener(new SoundSetter());
		menuButtonOpinion.setMnemonic(KeyEvent.VK_T);
		
		menuButtonImpulse = new JRadioButtonMenuItem("Utopian Impulse");
		menuSound.add(menuButtonImpulse);
		menuButtonImpulse.addActionListener(new SoundSetter());
		menuButtonImpulse.setMnemonic(KeyEvent.VK_U);
		
		menuButtonRandomSound = new JRadioButtonMenuItem("Use Random Sound");
		menuSound.add(menuButtonRandomSound);
		menuButtonRandomSound.setMnemonic(KeyEvent.VK_R);
		
		menuCredits = new JMenu("Credits");
		menuCredits.setMnemonic(KeyEvent.VK_C);
		menuCredits.setForeground(Color.WHITE);
		menuBar.add(menuCredits);
		
		menuItemCredits = new JMenuItem("Display Obligatory Credits");
		menuCredits.add(menuItemCredits);
		menuItemCredits.setMnemonic(KeyEvent.VK_C);
		menuItemCredits.addActionListener(new CreditsDisplay());
		
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(menuButtonBlackout);
		bg.add(menuButtonDoubts);
		bg.add(menuButtonOpinion);
		bg.add(menuButtonImpulse);
		bg.add(menuButtonRandomSound);

		
		setVisible(true);
		menuButtonBlackout.doClick();
	}
	
	private class ExitListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
			
		}
		
	}
	
	private class CreditsDisplay implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Sound Courtesy of\n“Music from Jukedeck - create your own at http://jukedeck.com”");
			
		}
	
	}
	private class SoundSetter implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (menuButtonBlackout.isSelected()) {
				b.setSound(SoundType.BLACKOUT);
			}
			else if(menuButtonDoubts.isSelected()){
				b.setSound(SoundType.DOUBTS);
			}else if(menuButtonOpinion.isSelected()){
				b.setSound(SoundType.OPINION);
			}else if(menuButtonImpulse.isSelected()) {
				b.setSound(SoundType.IMPULSE);
			}
			else {
				b.setSound(SoundType.RANDOM);
			}
			
		}
		
	}
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		new SoundBlocksGUI();
	}
}
