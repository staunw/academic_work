import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Ball extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9126339022135043080L;
	
	private final int SPEED = 1;
	private final int R_WIDTH = 50;
	private final int R_HEIGHT = 50;
	private final int TIMER_OFFSET = 5000;
	private final int BB_OFFSET = 54;
	private final int COLOR_MAX = 256;
	private final int SQUARE_X = 75;
	private final int SQUARE_Y = 60;
	private final int SQUARE_HEIGHT_WIDTH = 90;
	private final int STAR_POINTS = 7;
	private final int STAR_X = 550;
	private final int STAR_Y = 95;
	private final double STAR_OUTER = 64.0;
	private final double STAR_INNER = 75.0;
	private final int OCT_COORD_1 = 588;
	private final int OCT_COORD_2 = 458;
	private final int OCT_COORD_3 = 512;
	private final int OCT_COORD_4 = 642;
	private final int TRI_COORD_1 = 100;
	private final int TRI_COORD_2 = 530;
	private final int TRI_COORD_3 = 39;
	private final int TRI_COORD_4 = 635;
	private final int TRI_COORD_5 = 161;
	
	private SoundType sound = SoundType.RANDOM;
	private Point p = new Point(STAR_X, STAR_Y);
	private int rightBound;
	private int bottomBound;
	private int x_point;
	private int y_point;
	private ArrayList<String> pressedKeys;
	private Double circle;
	private Rectangle2D.Double rectangle;
	private Shape gpTriangle = createTriangle();
	private Shape gpOctagon = createOctagon();
	private Shape gpStar = createStar(STAR_POINTS, p, STAR_OUTER, STAR_INNER);
	private GradientPaint theGradient = new GradientPaint(randomInt(), randomInt(), 
			getRandomColor(), randomInt(),randomInt(), getRandomColor());
	private Timer t = new Timer(TIMER_OFFSET, new TimerListener());
	private Graphics2D graph2;
	
	public Ball(int rb, int bb) {
		pressedKeys = new ArrayList<String>();
		rightBound = rb;
		bottomBound = bb - BB_OFFSET;
		x_point = rightBound / 2;
		y_point = bottomBound / 2;
		this.setFocusable(true);
		this.requestFocus();
		setBackground(Color.BLACK);
		addKeyListener(new BallDriver());
		t.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		graph2 = (Graphics2D)g;
		Stroke stroke= new BasicStroke(5.0F);
		circle = new Ellipse2D.Double(x_point,y_point,R_WIDTH, R_HEIGHT);
		rectangle = new Rectangle2D.Double(SQUARE_X,SQUARE_Y,SQUARE_HEIGHT_WIDTH,SQUARE_HEIGHT_WIDTH);
		Shape rectangle2 = new Rectangle2D.Double(0,0, rightBound, bottomBound + 5);
		
		graph2.setPaint(theGradient);
		graph2.fill(rectangle2);
		
		graph2.setPaint(getRandomColor());
		graph2.setStroke(stroke);
		graph2.draw(rectangle);
		graph2.setPaint(Color.BLUE);
		graph2.fill(rectangle);
		
		graph2.setPaint(getRandomColor());
		graph2.setStroke(stroke);
		graph2.draw(gpOctagon);
		graph2.setPaint(Color.YELLOW);
		graph2.fill(gpOctagon);
	
		
		graph2.setPaint(getRandomColor());
		graph2.draw(gpTriangle);
		graph2.setPaint(Color.ORANGE);
		graph2.fill(gpTriangle);
		graph2.setPaint(getRandomColor());
		
		graph2.setPaint(getRandomColor());
		
		graph2.draw(gpStar);
		graph2.setPaint(Color.MAGENTA);
		graph2.fill(gpStar);
		
		graph2.draw(circle);
		graph2.setPaint(Color.RED);
		graph2.fill(circle);
		
	}

	public void movePoint() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		if (pressedKeys.contains("Up")) {
			if (!(y_point <= 0)) {
				y_point -= SPEED;
			}
			
		}
		if (pressedKeys.contains("Down")) {
			if (!(y_point + R_HEIGHT >= bottomBound)) {
				y_point += SPEED;
			}
			
		}
		if (pressedKeys.contains("Left")) {
			if (!(x_point <= 0)) {
				x_point -= SPEED;
			}
			
		}
		if (pressedKeys.contains("Right")) {
			if (!(x_point + R_WIDTH >= rightBound)) {
				x_point += SPEED;
			}

		}
		playMusic();
	}

	private class BallDriver extends KeyAdapter {
		

		public void keyPressed(KeyEvent e) {
			
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_UP:
			case KeyEvent.VK_W:
				if (!pressedKeys.contains("Up")) {
					pressedKeys.add("Up");
				}
				
				break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_S:
				if (!pressedKeys.contains("Down")) {
					pressedKeys.add("Down");
				}
				
				break;
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_A:
				if (!pressedKeys.contains("Left")) {
					pressedKeys.add("Left");
				}
				
				break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_D:
				if (!pressedKeys.contains("Right")) {
					pressedKeys.add("Right");
					
				}
				
				break;
			}
			try {
				movePoint();
			} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			repaint();
		}

		public void keyReleased(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_UP:
			case KeyEvent.VK_W:
				pressedKeys.remove("Up");
				break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_S:
				pressedKeys.remove("Down");
				break;
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_A:
				pressedKeys.remove("Left");
				break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_D:
				pressedKeys.remove("Right");
				break;
			}
		}
		
		
	}
	
	private class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			theGradient = null;
			theGradient = new GradientPaint(randomInt(),randomInt(), getRandomColor(), 
					      randomInt(),randomInt(), getRandomColor());
			
			repaint();
		}
		
	}
	
	private int randomInt() {
		Random rand = new Random();
		return rand.nextInt(1000);
	}
	
	private Color getRandomColor() {
		Random rand = new Random();
		
		return new Color(rand.nextInt(COLOR_MAX), rand.nextInt(COLOR_MAX), rand.nextInt(COLOR_MAX));
	}
	
	private Shape createTriangle() {
		
		GeneralPath gp = new GeneralPath();
		gp.moveTo(TRI_COORD_1, TRI_COORD_2);
		gp.lineTo(TRI_COORD_3, TRI_COORD_4);
		gp.lineTo(TRI_COORD_5, TRI_COORD_4);
	    gp.closePath();
		return gp;
	}
	
	private Shape createOctagon() {
		
		GeneralPath gp = new GeneralPath();
		gp.moveTo(OCT_COORD_1, OCT_COORD_2);
		gp.lineTo(OCT_COORD_3, OCT_COORD_2);
		gp.lineTo(OCT_COORD_2, OCT_COORD_3);
		gp.lineTo(OCT_COORD_2, OCT_COORD_1);
		gp.lineTo(OCT_COORD_3, OCT_COORD_4);
		gp.lineTo(OCT_COORD_1, OCT_COORD_4);
		gp.lineTo(OCT_COORD_4, OCT_COORD_1);
		gp.lineTo(OCT_COORD_4, OCT_COORD_3);
		gp.closePath();
		return gp;
	}
	
	private Shape createStar(int arms, Point center, double rOuter, double rInner){
	    double angle = Math.PI / arms;

	    GeneralPath path = new GeneralPath();

	    for (int i = 0; i < 2 * arms; i++)
	    {
	        double r = (i & 1) == 0 ? rOuter : rInner;
	        Point2D.Double p = new Point2D.Double(center.x + Math.cos(i * angle) * r, center.y + Math.sin(i * angle) * r);
	        if (i == 0) path.moveTo(p.getX(), p.getY());
	        else path.lineTo(p.getX(), p.getY());
	    }
	    path.closePath();
	    return path;
	}

	public SoundType getSound() {
		return sound;
	}

	public void setSound(SoundType sound) {
		this.sound = sound;
	}
	private void playMusic() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		if (isIntersected() && !SoundChanger.isPlaying()) {
			if (sound == SoundType.RANDOM) {
				SoundChanger.playRandomSound();
			}
			if (sound == SoundType.BLACKOUT) {
				if (circle.intersects(gpStar.getBounds2D())) {
					SoundChanger.playSoundBlackout();
				}else if (circle.intersects(rectangle)){
					SoundChanger.playSoundBlackout();
				}else if (circle.intersects(gpOctagon.getBounds2D())) {
					SoundChanger.playSoundBlackout();
				}else {
					SoundChanger.playSoundBlackout();
				}
			}else if(sound == SoundType.DOUBTS) {
				if (circle.intersects(gpStar.getBounds2D())) {
					SoundChanger.playSoundDoubts();
				}else if (circle.intersects(rectangle)){
					SoundChanger.playSoundDoubts();
				}else if (circle.intersects(gpOctagon.getBounds2D())) {
					SoundChanger.playSoundDoubts();
				}else {
					SoundChanger.playSoundDoubts();
				}
			}else if(sound == SoundType.IMPULSE) {
				if (circle.intersects(gpStar.getBounds2D())) {
					SoundChanger.playSoundImpulse();
				}else if (circle.intersects(rectangle)){
					SoundChanger.playSoundImpulse();
				}else if (circle.intersects(gpOctagon.getBounds2D())) {
					SoundChanger.playSoundImpulse();
				}else {
					SoundChanger.playSoundImpulse();
				}
			}else if(sound == SoundType.OPINION) {
				if (circle.intersects(gpStar.getBounds2D())) {
					SoundChanger.playSoundOpinion();
				}else if (circle.intersects(rectangle)){
					SoundChanger.playSoundOpinion();
				}else if (circle.intersects(gpOctagon.getBounds2D())) {
					SoundChanger.playSoundOpinion();
				}else {
					SoundChanger.playSoundOpinion();
				}
			}
		} 
		if (!isIntersected() && SoundChanger.isPlaying()) {
			SoundChanger.quitPlayingSound();
		}
	}
	private boolean isIntersected() {
		boolean i = false;
		
		if (circle.intersects(gpStar.getBounds2D())) {
			i = true;
		}else if (circle.intersects(rectangle)) {
			i = true;
		}else if (circle.intersects(gpOctagon.getBounds2D())){
			i = true;
		}else if(circle.intersects(gpTriangle.getBounds())) {
			i = true;
		}else {
			i = false;
		}
		return i;
	}
}