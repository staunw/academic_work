/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		4/30/2018
 */
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JRadioButton;
import java.awt.Dimension;
/**
 * OptionsPanel extends JPanel and it is responsible for creating the panel that displays the options for changing 
 * the background and foreground colors of the tiles as well as providing a way to reset the tiles. 
 * @author Nick Stauffer
 * @author Staunw01@students.ipfw.edu
 */
public class OptionsPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4265999352842805L;
	private final int BACKGROUND_COLOR_BLACK = 0;
	private final int BACKGROUND_COLOR_WHITE = 1;
	private final int BACKGROUND_COLOR_RED = 2;
	private final int BACKGROUND_COLOR_GREEN = 3;
	private final int BACKGROUND_COLOR_BLUE = 4;
	private final int BACKGROUND_COLOR_GRAY = 5;
	private final int BACKGROUND_COLOR_ORANGE = 6;
	private final int BACKGROUND_COLOR_PINK = 7;
	
	private String[] backgroundColors = {"Black", "White", "Red", "Green", "Blue", "Gray", "Orange", "Pink"};
	private JButton buttonReset;
	private JPanel panelBackgroundColor;
	private JComboBox<String> comboBox;
	private JPanel panelForegroundColor;
	private JPanel panelBasicColors;
	private JRadioButton radioButtonBlack;
	private JRadioButton radioButtonWhite;
	private JRadioButton radioButtonRed;
	private JRadioButton radioButtonGreen;
	private JRadioButton radioButtonBlue;
	private JPanel panelExtraColors;
	private JRadioButton radioButtonGray;
	private JRadioButton radioButtonOrange;
	private JRadioButton radioButtonPink;
	private JRadioButton radioButtonSkyBlue;
	private JRadioButton radioButtonYellow;
	private Tile[] tiles;
	/**
	 * OptionsPanel constructor sets up the GUI for displaying the options to the user. Providing radio buttons for foreground
	 * colors, provding a JComboBox for background colors and providing a reset button for resetting the current 
	 * tile design. It takes an array of tiles which is the current array of tiles.
	 * @param array an array of Tiles
	 */
	public OptionsPanel(Tile[] array) {
		
		setMinimumSize(new Dimension(250, 10));
		setLayout(new BorderLayout(0, 0));
		
		tiles = array;
		
		buttonReset = new JButton("Reset");
		add(buttonReset, BorderLayout.NORTH);
		buttonReset.setToolTipText("Resets all tiles to their default state");
		buttonReset.setMnemonic(KeyEvent.VK_T);
		buttonReset.addActionListener(new ResetButtonListener());
		
		panelBackgroundColor = new JPanel();
		panelBackgroundColor.setBorder(new TitledBorder(null, "Background Color", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panelBackgroundColor, BorderLayout.SOUTH);
		panelBackgroundColor.setLayout(new GridLayout(0, 1, 0, 0));
		
		comboBox = new JComboBox<String>(backgroundColors);
		comboBox.setToolTipText("Change the background color of the tile");
		panelBackgroundColor.add(comboBox);
		
		panelForegroundColor = new JPanel();
		panelForegroundColor.setBorder(new TitledBorder(null, "Foreground Colors", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panelForegroundColor, BorderLayout.CENTER);
		panelForegroundColor.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelBasicColors = new JPanel();
		panelBasicColors.setBorder(new TitledBorder(null, "Basic Colors", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelForegroundColor.add(panelBasicColors);
		panelBasicColors.setLayout(new GridLayout(5, 0, 0, 0));
		
		radioButtonBlack = new JRadioButton("Black");
		radioButtonBlack.setToolTipText("Choose to change the foreground color to black");
		radioButtonBlack.setMnemonic(KeyEvent.VK_B);
		panelBasicColors.add(radioButtonBlack);
		
		radioButtonWhite = new JRadioButton("White");
		radioButtonWhite.setToolTipText("Choose to change the foreground color to white");
		radioButtonWhite.setMnemonic(KeyEvent.VK_W);
		panelBasicColors.add(radioButtonWhite);
		radioButtonWhite.setSelected(true);
		
		radioButtonRed = new JRadioButton("Red");
		radioButtonRed.setToolTipText("Choose to change the foreground color to red");
		radioButtonRed.setMnemonic(KeyEvent.VK_R);
		panelBasicColors.add(radioButtonRed);
		
		radioButtonGreen = new JRadioButton("Green");
		radioButtonGreen.setToolTipText("Choose to change the foreground color to green");
		radioButtonGreen.setMnemonic(KeyEvent.VK_G);
		panelBasicColors.add(radioButtonGreen);
		
		radioButtonBlue = new JRadioButton("Blue");
		radioButtonBlue.setToolTipText("Choose to change the foreground color to Blue");
		radioButtonBlue.setMnemonic(KeyEvent.VK_U);
		panelBasicColors.add(radioButtonBlue);
		
		panelExtraColors = new JPanel();
		panelExtraColors.setBorder(new TitledBorder(null, "Extra Colors", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelForegroundColor.add(panelExtraColors);
		panelExtraColors.setLayout(new GridLayout(5, 0, 0, 0));
		
		radioButtonGray = new JRadioButton("Gray");
		radioButtonGray.setToolTipText("Choose to change the foreground color to gray");
		radioButtonGray.setMnemonic(KeyEvent.VK_Y);
		panelExtraColors.add(radioButtonGray);
		
		radioButtonOrange = new JRadioButton("Orange");
		radioButtonOrange.setToolTipText("Choose to change the foreground color to orange");
		radioButtonOrange.setMnemonic(KeyEvent.VK_O);
		panelExtraColors.add(radioButtonOrange);
		
		radioButtonPink = new JRadioButton("Pink");
		radioButtonPink.setToolTipText("Choose to change the foreground color to pink");
		radioButtonPink.setMnemonic(KeyEvent.VK_P);
		panelExtraColors.add(radioButtonPink);
		
		radioButtonSkyBlue = new JRadioButton("Sky Blue");
		radioButtonSkyBlue.setToolTipText("Choose to change the foreground color to sky blue");
		radioButtonSkyBlue.setMnemonic(KeyEvent.VK_S);
		panelExtraColors.add(radioButtonSkyBlue);
		
		radioButtonYellow = new JRadioButton("Yellow");
		radioButtonYellow.setToolTipText("Choose to change the foreground color to yellow");
		radioButtonYellow.setMnemonic(KeyEvent.VK_E);
		panelExtraColors.add(radioButtonYellow);
		
		ButtonGroup basicColorGroup = new ButtonGroup();
		basicColorGroup.add(radioButtonBlack);
		basicColorGroup.add(radioButtonWhite);
		basicColorGroup.add(radioButtonRed);
		basicColorGroup.add(radioButtonGreen);
		basicColorGroup.add(radioButtonBlue);
		basicColorGroup.add(radioButtonGray);
		basicColorGroup.add(radioButtonOrange);
		basicColorGroup.add(radioButtonPink);
		basicColorGroup.add(radioButtonSkyBlue);
		basicColorGroup.add(radioButtonYellow);
	}//End constructor
	/**
	 * getForegroundColor checks which radio button is selected and returns the appropriate color associated with
	 * that radio button.
	 * @return the current color that is selected with a radio button.
	 */
	public Color getForegroundColor() {
		Color color = null;
		if (radioButtonBlack.isSelected()) {
			color = Color.BLACK;
		}
		if (radioButtonWhite.isSelected()) {
			color = Color.WHITE;
		}
		if (radioButtonRed.isSelected()) {
			color = Color.RED;
		}
		if (radioButtonGreen.isSelected()) {
			color = Color.GREEN;
		}
		if (radioButtonBlue.isSelected()) {
			color = Color.BLUE;
		}
		if (radioButtonGray.isSelected()) {
			color = Color.GRAY;
		}
		if (radioButtonOrange.isSelected()) {
			color = Color.ORANGE;
		}
		if (radioButtonPink.isSelected()) {
			color = Color.PINK;
		}
		if (radioButtonSkyBlue.isSelected()) {
			color = new Color(135, 206, 235);
		}
		if (radioButtonYellow.isSelected()) {
			color = Color.YELLOW;
		}
		return color;
	}//End getForegroundColor
	/**
	 * getBackgroundColor checks which color is currently selected in the combo box and returns the color associated
	 * with the color currently selected in the combo box. It accomplishes this by getting the index and comparing
	 * it to a constant associated with that index.
	 * @return the current 
	 */
	public Color getBackgroundColor() {
		Color color = null;
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_BLACK) {
			color = Color.BLACK;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_WHITE) {
			color = Color.WHITE;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_RED) {
			color = Color.RED;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_GREEN) {
			color = Color.GREEN;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_BLUE) {
			color = Color.BLUE;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_GRAY) {
			color = Color.GRAY;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_ORANGE) {
			color = Color.ORANGE;
		}
		if (comboBox.getSelectedIndex() == BACKGROUND_COLOR_PINK) {
			color = Color.PINK;
		}
		return color;
	}//End getBackgroundColor
	/**
	 * reset method is a private helper method that sets the tiles back to their default state. It accomplishes this
	 * by setting the background and foreground colors to their default value and then looping through the array
	 * of tiles and setting the background and foreground colors to the default value and then setting the symbol
	 * that is displayed on the tile to the default value.
	 */
	private void reset() {
		radioButtonWhite.setSelected(true);
		comboBox.setSelectedIndex(BACKGROUND_COLOR_BLACK);
		for (Tile t: tiles) {
			t.setBackground(getBackgroundColor());
			t.setForeground(getForegroundColor());
			t.setSymbol(0);
		}
	}
	/**
	 * class ResetButtonListener is an action listener that resets the tiles by calling the reset helper method
	 *
	 */
	private class ResetButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			reset();
			
		}
		
	}
	
}
