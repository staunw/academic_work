/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		4/30/2018
 */
import java.awt.Color;
import java.io.Serializable;
import javax.swing.JButton;
import java.awt.Font;
/**
 * Tile class is an extension of JButton and implements Serializable in order to save the tile. It declares a
 * constant char array of symbols in UTF in order to display them on the tile. It has no fields. It has methods for
 * changing the background and foreground colors as well as a setter for changing the symbol on the tile.
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class Tile extends JButton implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6081443670831699737L;

	private final char[] symbols = {0x2182,0x20AA,0x1368,0x205c,0x272c};
	/**
	 * the Tile() constructor sets up a default tile by setting the background color to black, the foreground color
	 * to white, the font size and type and sets the text on the tile to the first symbol in the symbols array.
	 */
	public Tile() {
		setBackground(Color.BLACK);
		setForeground(Color.WHITE);
		setFont(new Font("Tahoma", Font.PLAIN, 24));
		setText(Character.toString(symbols[0]));
	}//End constructor
	/**
	 * the setSymbol method takes an integer as a parameter and sets the text on the tile based on the index and 
	 * remainder of 5, the number of elements in the symbols array.
	 * @param index
	 */
	public void setSymbol(int index) {
		setText(Character.toString(symbols[index % 5]));
	}//End setSymbol
	/**
	 * setbackgroundColor takes a Color object as a parameter and sets the background of the tile to the color
	 * that was passed as the parameter.
	 * @param backgroundColor the color to set the background color of the tile
	 */
	public void setBackgroundColor(Color backgroundColor) {
		setBackground(backgroundColor);
	}//End 
	/**
	 * setForgroundColor takes a Color object as a parameter and sets the foreground color of the tile to the
	 * color that was passed as a parameter.
	 * @param foregroundColor the color to set the foreground color of the tile
	 */
	public void setForegroundColor(Color foregroundColor) {
		setForeground(foregroundColor);
	}
	
}
