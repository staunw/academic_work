/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		4/30/2018
 */
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
/**
 * TileDesignerDriver is a class that holds the main method.
 * @author Nick Stauffer
 * @author staunw01@students.ipfw.edu
 */
public class TileDesignerDriver {
	/**
	 * The main method of the TileDesignerDriver is responsible for setting the look and feel of the GUI using 
	 * JTattoo, it uses the Hifi look and feel in JTattoo. the main method is also responsible for creating an
	 * instance of TileDesignerGUI.
	 * @param args
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 * @throws LineUnavailableException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws UnsupportedLookAndFeelException
	 */
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		new TileDesignerGUI();
	}//End main method

}//End TileDesignerDriver
