/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		4/30/2018
 */
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
/**
 * The TileDesignerGUI class is the View/Controller of the application. TileDesignerGUI accomplishes it task by
 * setting up the GUI with the appropriate GUI components, primarily adding the tiles and creating an instance of
 * the OptionsPanel class. In addition, TileDesignerGUI acts as the controller by creating the appropriate action
 * listeners and adding them to the tiles.
 * @author Nick Stauffer
 * @author Staunw01@students.ipfw.edu
 */
public class TileDesignerGUI extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1189744708686131265L;
	private OptionsPanel op;
	private JPanel panelTiles;
	private Tile[] tiles = new Tile[81];
	private JMenuItem menuItemSaveFile;
	private JMenu menuFile;
	private JMenuItem menuItemLoadFile;
	/**
	 * TileDesignerGUI() is the sole constructor responsible for setting up the GUI by creating an instance of the
	 * options panel and adding it to the GUI as well as creating a panel that holds the tiles. The tiles are
	 * instantiated and added to an array. In addition, the constructor sets up playing the relaxing background 
	 * music that is heard when designing.
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 * @throws LineUnavailableException
	 */
	public TileDesignerGUI() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(new Dimension(750, 750));
		setTitle("Tile Designer");
		
		panelTiles = new JPanel();
		getContentPane().add(panelTiles, BorderLayout.CENTER);
		panelTiles.setLayout(new GridLayout(9, 9, 0, 0));
		
		for (int i = 0; i < tiles.length; i++) {
			tiles[i] = new Tile();
			panelTiles.add(tiles[i]);
			tiles[i].addActionListener(new ChangeSymbolListener());
			tiles[i].addActionListener(new ChangeBackgroundListener());
			tiles[i].addActionListener(new ChangeForegroundListener());
		}
		
		op = new OptionsPanel(tiles);
		op.setPreferredSize(new Dimension(250, 237));
		getContentPane().add(op, BorderLayout.EAST);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuFile = new JMenu("File");
		menuFile.setToolTipText("Opens a menu to save or open a design");
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(menuFile);
		
		menuItemSaveFile = new JMenuItem("Save Design");
		menuItemSaveFile.setToolTipText("Saves the current design to a .til file");
		menuItemSaveFile.setMnemonic(KeyEvent.VK_S);
		menuFile.add(menuItemSaveFile);
		menuItemSaveFile.addActionListener(new SaveFileListener());
		
		menuItemLoadFile = new JMenuItem("Load Design");
		menuItemLoadFile.setToolTipText("Loads a previous design from a .til file");
		menuItemLoadFile.setMnemonic(KeyEvent.VK_L);
		menuFile.add(menuItemLoadFile);
		menuItemLoadFile.addActionListener(new LoadFileListener());
		
		
		File soundFile = new File("audio/output.wav");
		AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
		Clip clip = AudioSystem.getClip();
		clip.open(audioIn);
		clip.start();
		
		setVisible(true);
	}//end Constructor
	/**
	 * ChangeSymbolListener is a class that creates an action listener for the tiles to changed the symbol that is 
	 * displayed to the user. It accomplishes this by looping through the array of tiles and checking which tile was
	 * clicked and then passes a count that is used to change the symbol using the Tile setSymbol method. 
	 */
	private class ChangeSymbolListener implements ActionListener {
		int count = 0;
		@Override
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < tiles.length; i++) {
				if (e.getSource().equals(tiles[i])) {
					tiles[i].setSymbol(++count);
				}
			}
		}//End actionPerformed
		
	}//End class ChangeSymbolListener
	/**
	 * ChangeBackgroundListener calls the setBackground method in Tiles class for each tile in the tiles array. It then
	 * calls the getBackgroundColor method in the OptionsPanel class and sets the background according to the color
	 * selected in the options panel.
	 */
	private class ChangeBackgroundListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < tiles.length; i++) {
				if (e.getSource().equals(tiles[i])) {
					tiles[i].setBackground(op.getBackgroundColor());
				}
			}
			
		}//End actionPerformed
		
	}//End class ChangeBackgroundListener
	/**
	 * ChangeForegroundListener calls the setForeground method in Tiles class for each tile in the tiles array. It then
	 * calls the getForegroundColor() in the OptionsPanel class and changes the foreground color of the tile according
	 * to the color selected in the options panel.
	 */
	private class ChangeForegroundListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < tiles.length; i++) {
				if (e.getSource().equals(tiles[i])) {
					tiles[i].setForeground(op.getForegroundColor());
				}
			}
			
		}//End actionPerformed
		
	}//End class ChangeForegroundListener
	/**
	 * SaveFileListener saves the current design that was created by calling the static method saveTiles in 
	 * FileOperations class.
	 */
	private class SaveFileListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			FileOperations.saveTiles(tiles);
		}//End actionPerformed
		
	}//End class SaveFileListner
	/**
	 * LoadFileListener removes all the tiles in the panelTiles and then calls the private helper method 
	 * getTiles and loops through the array of tiles that getTiles returns. It adds the tiles from the array
	 * to the panelTiles and adds new ActionListeners to the tiles that are loaded. Finally, it updates the
	 * UI of each tile and repaints panelTiles.
	 *
	 */
	private class LoadFileListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			panelTiles.removeAll();
			
			for (Tile t : getTiles()) {
				panelTiles.add(t);
				t.addActionListener(new ChangeForegroundListener());
				t.addActionListener(new ChangeBackgroundListener());
				t.addActionListener(new ChangeSymbolListener());
				t.updateUI();
			}
			panelTiles.repaint();
		}//End actionPerformed
		
	}//End class LoadFileListner
	/**
	 * getTiles is a private helper method that creates a temporary ArrayList of tiles that are loaded from
	 * the FileOperations loadTiles method. The method then makes a shallow copy of the tile to the tiles 
	 * array field and returns the field of tiles.
	 * @return the array of tiles of field tiles
	 */
	private Tile[] getTiles() {
		ArrayList<Tile> temp = FileOperations.loadTiles(tiles);
		for (int i = 0; i < tiles.length; i++) {
			tiles[i] = temp.get(i);
		}
		temp = null;
		return tiles;
	}//End getTiles method
}//End class TileDesignerGUI
