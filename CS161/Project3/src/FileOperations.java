/*
NAME:			Nick Stauffer
ASSIGNMENT:		Project 3
COURSE:			CS161
INSTRUCTOR:		Max Fowler
LAB TIME:		Thursday 6:00 PM
DUE DATE:		4/30/2018
 */
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * FileOperations is a class that handles loading and saving of .til design files. It has two static methods, one
 * for saving and one for loading files.
 * @author Nick Stauffer
 * @author Staunw01@students.ipfw.edu
 */
public class FileOperations {
	/**
	 * saveTiles is a static method that takes an array of tiles and saves them to a file. It accomplishes this by
	 * opening a file chooser and then taking the array of tiles and writing them to a file using an 
	 * ObjectOutputStream.
	 * @param tiles an array of tiles that are to be saved.
	 */
	public static void saveTiles(Tile[] tiles) {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter f = new FileNameExtensionFilter("Tile File", "til");
		chooser.setFileFilter(f);
	    int returnVal = chooser.showSaveDialog(null);
		try {
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
		           String fileName = chooser.getSelectedFile().getPath() + ".til";
		           FileOutputStream outStream = new FileOutputStream(fileName);
		           ObjectOutputStream objectOutputFile = new ObjectOutputStream(outStream);
		           for (Object t : tiles) {
		        	   objectOutputFile.writeObject(t);
		           }
		           objectOutputFile.close();
		    }
			
		}catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "There was a problem saving the file");
		}
	}//End saveTiles
	/**
	 * loadTiles is a static method that reads a .til file and takes the tile objects that are stored in the file
	 * and adds them to an ArrayList. It then returns the ArrayList so they can be used for restoring the previous
	 * design that was saved.
	 * @return and ArrayList of tiles
	 */
	public static ArrayList<Tile> loadTiles(Tile[] inTiles) {
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		ObjectInputStream objectInputFile;
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter f = new FileNameExtensionFilter("Tile File", "til");
		chooser.setFileFilter(f);
		int returnVal = chooser.showOpenDialog(null);
		try {
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				  String fileName = chooser.getSelectedFile().getPath();
		          FileInputStream inStream = new FileInputStream(fileName);
		          objectInputFile = new ObjectInputStream(inStream);
		          while (true) {
		        	   tiles.add((Tile) objectInputFile.readObject());
		          }
		    }
			if(returnVal == JFileChooser.CANCEL_OPTION) {
				for (Tile t : inTiles) {
					tiles.add(t);
				}
			}
		}catch (EOFException eof) {
			
	    }catch (Exception ex) {
	    	JOptionPane.showMessageDialog(null, "There was a problem loading the file");
		}
		
		return tiles;
	}//End loadTiles
}//End class FileOperations
