import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import java.awt.event.KeyEvent;

public class AddFishFrame extends JFrame{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7130086549632233830L;
	private final String BREED_GUPPY = "Guppy";
	private final String BREED_SIAMESE_FIGHTING_FISH = "Siamese Fighting Fish";
	private final String BREED_GOLDFISH = "Goldfish";
	private final String BREED_NEON_TETRA = "Neon Tetra";
	private final String BREED_RAM_CICHLID = "Ram Cichlid";
	private final String BREED_KOI = "Koi";
	private final String GENDER_MALE = "He";
	private final String GENDER_FEMALE = "She";
	
	private String petName;
	private String petGender;
	private double petAge;
	private String petDescription;
	private String petBreed;
	private final String species = "Fish";
	private DefaultListModel<Pet> dlm;
	
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JPanel panel;
	private JPanel panelInfo;
	private JPanel panelName;
	private JPanel panelGender;
	private JRadioButton radioButtonMale;
	private JRadioButton radioButtonFemale;
	private JPanel panelAge;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelBreeds;
	private JRadioButton rdbtnGuppy;
	private JRadioButton rdbtnSiameseFightingFish;
	private JRadioButton rdbtnGoldfish;
	private JRadioButton rdbtnNeonTetra;
	private JRadioButton rdbtnRamCichlid;
	private JRadioButton rdbtnKoi;

	private JScrollPane scrollBarDescription;
	private JPanel panelAdd;
	private JButton buttonAddPet;
	private JButton btnExit;
	
	public AddFishFrame(DefaultListModel<Pet> dlm) {
		this.dlm = dlm;
		
		setSize(new Dimension(700, 430));
		setPreferredSize(new Dimension(700, 430));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelAdd = new JPanel();
		panelAdd.setBackground(Color.CYAN);
		getContentPane().add(panelAdd, BorderLayout.SOUTH);
		
		buttonAddPet = new JButton("Add Pet");
		buttonAddPet.setMnemonic(KeyEvent.VK_D);
		buttonAddPet.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(buttonAddPet);
		
		btnExit = new JButton("Exit");
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(btnExit);
		buttonAddPet.addActionListener(new AddPetListener());
		btnExit.addActionListener(new ExitFrameListener());
		
		panelInfo = new JPanel();
		panel.add(panelInfo);
		panelInfo.setLayout(new GridLayout(2, 2, 0, 0));
		
		panelName = new JPanel();
		panelName.setBackground(Color.CYAN);
		panelName.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.add(panelName);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("Enter the personal name of the pet");
		panelName.add(textFieldName);
		textFieldName.setColumns(10);
		
		panelGender = new JPanel();
		panelGender.setBackground(Color.CYAN);
		panelGender.setBorder(new TitledBorder(null, "Male/Female", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelGender);
		
		radioButtonMale = new JRadioButton("Male");
		radioButtonMale.setMnemonic(KeyEvent.VK_M);
		panelGender.add(radioButtonMale);
		radioButtonMale.addActionListener(new RadioGenderListener());
		
		radioButtonFemale = new JRadioButton("Female");
		radioButtonFemale.setMnemonic(KeyEvent.VK_F);
		panelGender.add(radioButtonFemale);
		radioButtonFemale.addActionListener(new RadioGenderListener());
		
		panelAge = new JPanel();
		panelAge.setBackground(Color.CYAN);
		panelAge.setBorder(new TitledBorder(null, "Age", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelAge);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("Enter the current age of the pet");
		panelAge.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		panelDescription = new JPanel();
		panelDescription.setBackground(Color.CYAN);
		panelDescription.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setLineWrap(true);
		textAreaDescription.setColumns(15);
		textAreaDescription.setRows(8);
		textAreaDescription.setToolTipText("Enter a short description of the pet");
		scrollBarDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollBarDescription);
		
		panelBreeds = new JPanel();
		panelBreeds.setBackground(Color.CYAN);
		panelBreeds.setBorder(new TitledBorder(null, "Species of Fish", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelBreeds);
		panelBreeds.setLayout(new GridLayout(13, 0, 0, 0));
		
		rdbtnGuppy = new JRadioButton(BREED_GUPPY);
		rdbtnGuppy.setBackground(Color.CYAN);
		rdbtnGuppy.setMnemonic(KeyEvent.VK_G);
		panelBreeds.add(rdbtnGuppy);
		rdbtnGuppy.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSiameseFightingFish = new JRadioButton(BREED_SIAMESE_FIGHTING_FISH);
		rdbtnSiameseFightingFish.setBackground(Color.CYAN);
		rdbtnSiameseFightingFish.setMnemonic(KeyEvent.VK_S);
		panelBreeds.add(rdbtnSiameseFightingFish);
		rdbtnSiameseFightingFish.addActionListener(new RadioButtonBreedListener());
		
		rdbtnGoldfish = new JRadioButton(BREED_GOLDFISH);
		rdbtnGoldfish.setBackground(Color.CYAN);
		rdbtnGoldfish.setMnemonic(KeyEvent.VK_O);
		panelBreeds.add(rdbtnGoldfish);
		rdbtnGoldfish.addActionListener(new RadioButtonBreedListener());
		
		rdbtnNeonTetra = new JRadioButton(BREED_NEON_TETRA);
		rdbtnNeonTetra.setBackground(Color.CYAN);
		rdbtnNeonTetra.setMnemonic(KeyEvent.VK_N);
		panelBreeds.add(rdbtnNeonTetra);
		rdbtnNeonTetra.addActionListener(new RadioButtonBreedListener());
		
		rdbtnRamCichlid = new JRadioButton(BREED_RAM_CICHLID);
		rdbtnRamCichlid.setBackground(Color.CYAN);
		rdbtnRamCichlid.setMnemonic(KeyEvent.VK_R);
		panelBreeds.add(rdbtnRamCichlid);
		rdbtnRamCichlid.addActionListener(new RadioButtonBreedListener());
		
		rdbtnKoi = new JRadioButton(BREED_KOI);
		rdbtnKoi.setBackground(Color.CYAN);
		rdbtnKoi.setMnemonic(KeyEvent.VK_K);
		panelBreeds.add(rdbtnKoi);
		rdbtnKoi.addActionListener(new RadioButtonBreedListener());
		
	
		
		ButtonGroup breeds = new ButtonGroup();
		breeds.add(rdbtnGuppy);
		breeds.add(rdbtnSiameseFightingFish);
		breeds.add(rdbtnGoldfish);
		breeds.add(rdbtnNeonTetra);
		breeds.add(rdbtnRamCichlid);
		breeds.add(rdbtnKoi);
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(radioButtonMale);
		gender.add(radioButtonFemale);
		
		setVisible(true);
	}
	
	private class RadioButtonBreedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(rdbtnGuppy.isSelected()) {
				petBreed = BREED_GUPPY;
			}
			if(rdbtnSiameseFightingFish.isSelected()) {
				petBreed = BREED_SIAMESE_FIGHTING_FISH;
			}
			if(rdbtnGoldfish.isSelected()) {
				petBreed = BREED_GOLDFISH;
			}
			if(rdbtnNeonTetra.isSelected()) {
				petBreed = BREED_NEON_TETRA;
			}
			if(rdbtnRamCichlid.isSelected()) {
				petBreed = BREED_RAM_CICHLID;
			}
			if(rdbtnKoi.isSelected()) {
				petBreed = BREED_KOI;
			}
			
		}
		
	}
	private class RadioGenderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(radioButtonMale.isSelected()) {
				petGender = GENDER_MALE;
			}
			if(radioButtonFemale.isSelected()) {
				petGender = GENDER_FEMALE;
			}
		}
		
	}
	private class AddPetListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			petName = textFieldName.getText();
			try {
				petAge = Double.parseDouble(textFieldAge.getText());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(rootPane, "Invalid age, please inter a valid number.");
			}
			
			petDescription = textAreaDescription.getText();
			
			Pet pet = new Fish(petName, species, petAge, petGender, petBreed, petDescription);
			dlm.add(0,pet);
		}
		
	}
	private class ExitFrameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dispose();
		}
		
	}
}
