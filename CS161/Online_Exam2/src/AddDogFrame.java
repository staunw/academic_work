import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import java.awt.event.KeyEvent;

public class AddDogFrame extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2553668562289232906L;
	private final String BREED_LABRADOR_RETRIEVER = "Labrador Retriever";
	private final String BREED_DACHSHUND = "Dachshund";
	private final String BREED_YORKSHIRE_TERRIER = "Yorkshire Terrier";
	private final String BREED_GERMAN_SHEPARD = "German Shepard";
	private final String BREED_GOLDEN_RETRIEVER = "Golden Retriever";
	private final String BREED_GREAT_DANE = "Great Dane";
	private final String BREED_SIBERIAN_HUSKEY = "Siberian Huskey";
	private final String BREED_BULLDOG = "Bulldog";
	private final String BREED_BEAGLE = "Beagle";
	private final String BREED_BOXER = "Boxer";
	private final String BREED_ROTWEILER = "Rotweiler";
	private final String BREED_POODLE = "Poodle";
	private final String BREED_CHIHUAHUA = "Chihuahua";
	private final String GENDER_MALE = "He";
	private final String GENDER_FEMALE = "She";
	
	private String petName;
	private String petGender;
	private double petAge;
	private String petDescription;
	private String petBreed;
	private final String species = "Dog";
	private DefaultListModel<Pet> dlm;
	
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JPanel panel;
	private JPanel panelInfo;
	private JPanel panelName;
	private JPanel panelGender;
	private JRadioButton radioButtonMale;
	private JRadioButton radioButtonFemale;
	private JPanel panelAge;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelBreeds;
	private JRadioButton rdbtnLabradorRetriever;
	private JRadioButton rdbtnGermanShepard;
	private JRadioButton rdbtnGoldenRetriever;
	private JRadioButton rdbtnBulldog;
	private JRadioButton rdbtnBeagle;
	private JRadioButton rdbtnRotweiler;
	private JRadioButton rdbtnYorkshireTerrier;
	private JRadioButton rdbtnDachshund;
	private JRadioButton rdbtnSiberianHuskey;
	private JRadioButton rdbtnChihuahua;
	private JRadioButton rdbtnBoxer;
	private JRadioButton rdbtnPoodle;
	private JRadioButton rdbtnGreatDane;
	private JScrollPane scrollBarDescription;
	private JPanel panelAdd;
	private JButton buttonAddPet;
	private JButton btnExit;
	
	public AddDogFrame(DefaultListModel<Pet> dlm) {
		this.dlm = dlm;
		
		setSize(new Dimension(700, 430));
		setPreferredSize(new Dimension(700, 430));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("(How Much Is) That Doggy in the Window?");
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelAdd = new JPanel();
		panelAdd.setBackground(Color.CYAN);
		getContentPane().add(panelAdd, BorderLayout.SOUTH);
		
		buttonAddPet = new JButton("Add Pet");
		buttonAddPet.setMnemonic(KeyEvent.VK_D);
		buttonAddPet.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(buttonAddPet);
		
		btnExit = new JButton("Exit");
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(btnExit);
		buttonAddPet.addActionListener(new AddPetListener());
		btnExit.addActionListener(new ExitFrameListener());
		
		panelInfo = new JPanel();
		panel.add(panelInfo);
		panelInfo.setLayout(new GridLayout(2, 2, 0, 0));
		
		panelName = new JPanel();
		panelName.setBackground(Color.CYAN);
		panelName.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.add(panelName);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("Enter the personal name of the pet");
		panelName.add(textFieldName);
		textFieldName.setColumns(10);
		
		panelGender = new JPanel();
		panelGender.setBackground(Color.CYAN);
		panelGender.setBorder(new TitledBorder(null, "Male/Female", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelGender);
		
		radioButtonMale = new JRadioButton("Male");
		radioButtonMale.setMnemonic(KeyEvent.VK_M);
		panelGender.add(radioButtonMale);
		radioButtonMale.addActionListener(new RadioGenderListener());
		
		radioButtonFemale = new JRadioButton("Female");
		radioButtonFemale.setMnemonic(KeyEvent.VK_F);
		panelGender.add(radioButtonFemale);
		radioButtonFemale.addActionListener(new RadioGenderListener());
		
		panelAge = new JPanel();
		panelAge.setBackground(Color.CYAN);
		panelAge.setBorder(new TitledBorder(null, "Age", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelAge);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("Enter the current age of the pet");
		panelAge.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		panelDescription = new JPanel();
		panelDescription.setBackground(Color.CYAN);
		panelDescription.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setLineWrap(true);
		textAreaDescription.setColumns(15);
		textAreaDescription.setRows(8);
		textAreaDescription.setToolTipText("Enter a short description of the pet");
		scrollBarDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollBarDescription);
		
		panelBreeds = new JPanel();
		panelBreeds.setBackground(Color.CYAN);
		panelBreeds.setBorder(new TitledBorder(null, "Breed of Dog", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelBreeds);
		panelBreeds.setLayout(new GridLayout(13, 0, 0, 0));
		
		rdbtnLabradorRetriever = new JRadioButton(BREED_LABRADOR_RETRIEVER);
		rdbtnLabradorRetriever.setBackground(Color.CYAN);
		rdbtnLabradorRetriever.setMnemonic(KeyEvent.VK_L);
		panelBreeds.add(rdbtnLabradorRetriever);
		rdbtnLabradorRetriever.addActionListener(new RadioButtonBreedListener());
		
		rdbtnGermanShepard = new JRadioButton(BREED_GERMAN_SHEPARD);
		rdbtnGermanShepard.setBackground(Color.CYAN);
		rdbtnGermanShepard.setMnemonic(KeyEvent.VK_G);
		panelBreeds.add(rdbtnGermanShepard);
		rdbtnGermanShepard.addActionListener(new RadioButtonBreedListener());
		
		rdbtnGoldenRetriever = new JRadioButton(BREED_GOLDEN_RETRIEVER);
		rdbtnGoldenRetriever.setBackground(Color.CYAN);
		rdbtnGoldenRetriever.setMnemonic(KeyEvent.VK_O);
		panelBreeds.add(rdbtnGoldenRetriever);
		rdbtnGoldenRetriever.addActionListener(new RadioButtonBreedListener());
		
		rdbtnBulldog = new JRadioButton(BREED_BULLDOG);
		rdbtnBulldog.setBackground(Color.CYAN);
		rdbtnBulldog.setMnemonic(KeyEvent.VK_L);
		panelBreeds.add(rdbtnBulldog);
		rdbtnBulldog.addActionListener(new RadioButtonBreedListener());
		
		rdbtnBeagle = new JRadioButton(BREED_BEAGLE);
		rdbtnBeagle.setBackground(Color.CYAN);
		rdbtnBeagle.setMnemonic(KeyEvent.VK_E);
		panelBreeds.add(rdbtnBeagle);
		rdbtnBeagle.addActionListener(new RadioButtonBreedListener());
		
		rdbtnRotweiler = new JRadioButton(BREED_ROTWEILER);
		rdbtnRotweiler.setBackground(Color.CYAN);
		rdbtnRotweiler.setMnemonic(KeyEvent.VK_R);
		panelBreeds.add(rdbtnRotweiler);
		rdbtnRotweiler.addActionListener(new RadioButtonBreedListener());
		
		rdbtnYorkshireTerrier = new JRadioButton(BREED_YORKSHIRE_TERRIER);
		rdbtnYorkshireTerrier.setBackground(Color.CYAN);
		rdbtnYorkshireTerrier.setMnemonic(KeyEvent.VK_Y);
		panelBreeds.add(rdbtnYorkshireTerrier);
		rdbtnYorkshireTerrier.addActionListener(new RadioButtonBreedListener());
		
		rdbtnDachshund = new JRadioButton(BREED_DACHSHUND);
		rdbtnDachshund.setBackground(Color.CYAN);
		rdbtnDachshund.setMnemonic(KeyEvent.VK_C);
		panelBreeds.add(rdbtnDachshund);
		rdbtnDachshund.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSiberianHuskey = new JRadioButton(BREED_SIBERIAN_HUSKEY);
		rdbtnSiberianHuskey.setBackground(Color.CYAN);
		rdbtnSiberianHuskey.setMnemonic(KeyEvent.VK_S);
		panelBreeds.add(rdbtnSiberianHuskey);
		rdbtnSiberianHuskey.addActionListener(new RadioButtonBreedListener());
		
		rdbtnChihuahua = new JRadioButton(BREED_CHIHUAHUA);
		rdbtnChihuahua.setBackground(Color.CYAN);
		rdbtnChihuahua.setMnemonic(KeyEvent.VK_H);
		panelBreeds.add(rdbtnChihuahua);
		rdbtnChihuahua.addActionListener(new RadioButtonBreedListener());
		
		rdbtnBoxer = new JRadioButton(BREED_BOXER);
		rdbtnBoxer.setBackground(Color.CYAN);
		rdbtnBoxer.setMnemonic(KeyEvent.VK_B);
		panelBreeds.add(rdbtnBoxer);
		rdbtnBoxer.addActionListener(new RadioButtonBreedListener());
		
		rdbtnPoodle = new JRadioButton(BREED_POODLE);
		rdbtnPoodle.setBackground(Color.CYAN);
		rdbtnPoodle.setMnemonic(KeyEvent.VK_P);
		panelBreeds.add(rdbtnPoodle);
		rdbtnPoodle.addActionListener(new RadioButtonBreedListener());
		
		rdbtnGreatDane = new JRadioButton(BREED_GREAT_DANE);
		rdbtnGreatDane.setBackground(Color.CYAN);
		rdbtnGreatDane.setMnemonic(KeyEvent.VK_G);
		panelBreeds.add(rdbtnGreatDane);
		rdbtnGreatDane.addActionListener(new RadioButtonBreedListener());
		
		ButtonGroup breeds = new ButtonGroup();
		breeds.add(rdbtnLabradorRetriever);
		breeds.add(rdbtnDachshund);
		breeds.add(rdbtnYorkshireTerrier);
		breeds.add(rdbtnGermanShepard);
		breeds.add(rdbtnGoldenRetriever);
		breeds.add(rdbtnGreatDane);
		breeds.add(rdbtnSiberianHuskey);
		breeds.add(rdbtnBulldog);
		breeds.add(rdbtnBeagle);
		breeds.add(rdbtnBoxer);
		breeds.add(rdbtnRotweiler);
		breeds.add(rdbtnPoodle);
		breeds.add(rdbtnChihuahua);
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(radioButtonMale);
		gender.add(radioButtonFemale);
		
		setVisible(true);
	}
	
	private class RadioButtonBreedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(rdbtnLabradorRetriever.isSelected()) {
				petBreed = BREED_LABRADOR_RETRIEVER;
			}
			if(rdbtnDachshund.isSelected()) {
				petBreed = BREED_DACHSHUND;
			}
			if(rdbtnYorkshireTerrier.isSelected()) {
				petBreed = BREED_YORKSHIRE_TERRIER;
			}
			if(rdbtnGermanShepard.isSelected()) {
				petBreed = BREED_GERMAN_SHEPARD;
			}
			if(rdbtnGoldenRetriever.isSelected()) {
				petBreed = BREED_GOLDEN_RETRIEVER;
			}
			if(rdbtnGreatDane.isSelected()) {
				petBreed = BREED_GREAT_DANE;
			}
			if(rdbtnSiberianHuskey.isSelected()) {
				petBreed = BREED_SIBERIAN_HUSKEY;
			}
			if(rdbtnBulldog.isSelected()) {
				petBreed = BREED_BULLDOG;
			}
			if(rdbtnBeagle.isSelected()) {
				petBreed = BREED_BEAGLE;
			}
			if(rdbtnBoxer.isSelected()) {
				petBreed = BREED_BOXER;
			}
			if(rdbtnRotweiler.isSelected()) {
				petBreed = BREED_ROTWEILER;
			}
			if(rdbtnPoodle.isSelected()) {
				petBreed = BREED_POODLE;
			}
			if(rdbtnChihuahua.isSelected()) {
				petBreed = BREED_CHIHUAHUA;
			}
			
		}
		
	}
	private class RadioGenderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(radioButtonMale.isSelected()) {
				petGender = GENDER_MALE;
			}
			if(radioButtonFemale.isSelected()) {
				petGender = GENDER_FEMALE;
			}
		}
		
	}
	private class AddPetListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			petName = textFieldName.getText();
			try {
				petAge = Double.parseDouble(textFieldAge.getText());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(rootPane, "Invalid age, please inter a valid number.");
			}
			petDescription = textAreaDescription.getText();
			
			Pet pet = new Dog(petName, species, petAge, petGender, petBreed, petDescription);
			dlm.add(0,pet);
		}
		
	}
	private class ExitFrameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dispose();
		}
		
	}
}
