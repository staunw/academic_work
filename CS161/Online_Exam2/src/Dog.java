
public class Dog extends Pet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -484317059879990471L;
	private static final String SPECIES = "Dog";
	private String breed;
	private String description;
	
	public Dog(String name, String species, double age, String pronoun, String breed,String description) {
		super(name, SPECIES, age, pronoun);
		this.breed = breed;
		this.description = description;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String toStringDescription() {
		return super.toString() + " is " + age + " years old. " + pronoun + " is a " 
								+ breed + " and " + description; 
	}
}
