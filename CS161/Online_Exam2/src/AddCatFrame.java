import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.KeyEvent;

public class AddCatFrame extends JFrame{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1279439213211774503L;
	private final String BREED_ABYSSINIAN = "Abyssinian";
	private final String BREED_AMERICAN_SHORTHAIR = "American Shorthair";
	private final String BREED_BIRMAN = "Birman";
	private final String BREED_EXOTIC_SHORTHAIR = "Exotic Shorthair";
	private final String BREED_MAINE_COON = "Maine Coon";
	private final String BREED_NORWEGIAN_FOREST_CAT = "Norwegian Forest Cat";
	private final String BREED_ORIENTAL = "Oriental";
	private final String BREED_PERSIAN = "Persian";
	private final String BREED_RAGDOLL = "Ragdoll";
	private final String BREED_SAVANNAH = "Savannah";
	private final String BREED_SIAMESE = "Siamese";
	private final String BREED_SIBERIAN = "Siberian";
	private final String BREED_SPHYNX = "Sphynx";
	private final String GENDER_MALE = "He";
	private final String GENDER_FEMALE = "She";
	
	private String petName;
	private String petGender;
	private double petAge;
	private String petDescription;
	private String petBreed;
	private final String species = "Cat";
	private DefaultListModel<Pet> dlm;
	
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JPanel panel;
	private JPanel panelInfo;
	private JPanel panelName;
	private JPanel panelGender;
	private JRadioButton radioButtonMale;
	private JRadioButton radioButtonFemale;
	private JPanel panelAge;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelBreeds;
	private JRadioButton rdbtnPersian;
	private JRadioButton rdbtnMaineCoon;
	private JRadioButton rdbtnSiamese;
	private JRadioButton rdbtnExoticShorthair;
	private JRadioButton rdbtnAbyssinian;
	private JRadioButton rdbtnRagdoll;
	private JRadioButton rdbtnBirman;
	private JRadioButton rdbtnAmericanShorthair;
	private JRadioButton rdbtnOriental;
	private JRadioButton rdbtnSphynx;
	private JRadioButton rdbtnSavannah;
	private JRadioButton rdbtnSiberian;
	private JRadioButton rdbtnNorwegianForestCat;
	private JScrollPane scrollBarDescription;
	private JPanel panelAdd;
	private JButton buttonAddPet;
	private JButton btnExit;
	
	public AddCatFrame(DefaultListModel<Pet> dlm) {
		this.dlm = dlm;
		
		setSize(new Dimension(700, 430));
		setPreferredSize(new Dimension(700, 430));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Stray cat strut");
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelAdd = new JPanel();
		panelAdd.setBackground(Color.CYAN);
		getContentPane().add(panelAdd, BorderLayout.SOUTH);
		
		buttonAddPet = new JButton("Add Pet");
		buttonAddPet.setMnemonic(KeyEvent.VK_D);
		buttonAddPet.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(buttonAddPet);
		
		btnExit = new JButton("Exit");
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setSize(new Dimension(71, 24));
		btnExit.setPreferredSize(new Dimension(100, 24));
		btnExit.setMinimumSize(new Dimension(71, 23));
		btnExit.setMaximumSize(new Dimension(71, 23));
		panelAdd.add(btnExit);
		buttonAddPet.addActionListener(new AddPetListener());
		btnExit.addActionListener(new ExitFrameListener());
		
		panelInfo = new JPanel();
		panel.add(panelInfo);
		panelInfo.setLayout(new GridLayout(2, 2, 0, 0));
		
		panelName = new JPanel();
		panelName.setBackground(Color.CYAN);
		panelName.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.add(panelName);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("Enter the personal name of the pet");
		panelName.add(textFieldName);
		textFieldName.setColumns(10);
		
		panelGender = new JPanel();
		panelGender.setBackground(Color.CYAN);
		panelGender.setBorder(new TitledBorder(null, "Male/Female", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelGender);
		
		radioButtonMale = new JRadioButton("Male");
		radioButtonMale.setMnemonic(KeyEvent.VK_M);
		panelGender.add(radioButtonMale);
		radioButtonMale.addActionListener(new RadioGenderListener());
		
		radioButtonFemale = new JRadioButton("Female");
		radioButtonFemale.setMnemonic(KeyEvent.VK_F);
		panelGender.add(radioButtonFemale);
		radioButtonFemale.addActionListener(new RadioGenderListener());
		
		panelAge = new JPanel();
		panelAge.setBackground(Color.CYAN);
		panelAge.setBorder(new TitledBorder(null, "Age", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelAge);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("Enter the current age of the pet");
		panelAge.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		panelDescription = new JPanel();
		panelDescription.setBackground(Color.CYAN);
		panelDescription.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setLineWrap(true);
		textAreaDescription.setColumns(15);
		textAreaDescription.setRows(8);
		textAreaDescription.setToolTipText("Enter a short description of the pet");
		scrollBarDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollBarDescription);
		
		panelBreeds = new JPanel();
		panelBreeds.setBackground(Color.CYAN);
		panelBreeds.setBorder(new TitledBorder(null, "Breed of Cat", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelBreeds);
		panelBreeds.setLayout(new GridLayout(13, 0, 0, 0));
		
		rdbtnPersian = new JRadioButton(BREED_PERSIAN);
		rdbtnPersian.setBackground(Color.CYAN);
		rdbtnPersian.setMnemonic(KeyEvent.VK_P);
		panelBreeds.add(rdbtnPersian);
		rdbtnPersian.addActionListener(new RadioButtonBreedListener());
		
		rdbtnMaineCoon = new JRadioButton(BREED_MAINE_COON);
		rdbtnMaineCoon.setBackground(Color.CYAN);
		rdbtnMaineCoon.setMnemonic(KeyEvent.VK_C);
		panelBreeds.add(rdbtnMaineCoon);
		rdbtnMaineCoon.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSiamese = new JRadioButton(BREED_SIAMESE);
		rdbtnSiamese.setBackground(Color.CYAN);
		rdbtnSiamese.setMnemonic(KeyEvent.VK_S);
		panelBreeds.add(rdbtnSiamese);
		rdbtnSiamese.addActionListener(new RadioButtonBreedListener());
		
		rdbtnExoticShorthair = new JRadioButton(BREED_EXOTIC_SHORTHAIR);
		rdbtnExoticShorthair.setBackground(Color.CYAN);
		rdbtnExoticShorthair.setMnemonic(KeyEvent.VK_E);
		panelBreeds.add(rdbtnExoticShorthair);
		rdbtnExoticShorthair.addActionListener(new RadioButtonBreedListener());
		
		rdbtnAbyssinian = new JRadioButton(BREED_ABYSSINIAN);
		rdbtnAbyssinian.setBackground(Color.CYAN);
		rdbtnAbyssinian.setMnemonic(KeyEvent.VK_A);
		panelBreeds.add(rdbtnAbyssinian);
		rdbtnAbyssinian.addActionListener(new RadioButtonBreedListener());
		
		rdbtnRagdoll = new JRadioButton(BREED_RAGDOLL);
		rdbtnRagdoll.setBackground(Color.CYAN);
		rdbtnRagdoll.setMnemonic(KeyEvent.VK_R);
		panelBreeds.add(rdbtnRagdoll);
		rdbtnRagdoll.addActionListener(new RadioButtonBreedListener());
		
		rdbtnBirman = new JRadioButton(BREED_BIRMAN);
		rdbtnBirman.setBackground(Color.CYAN);
		rdbtnBirman.setMnemonic(KeyEvent.VK_B);
		panelBreeds.add(rdbtnBirman);
		rdbtnBirman.addActionListener(new RadioButtonBreedListener());
		
		rdbtnAmericanShorthair = new JRadioButton(BREED_AMERICAN_SHORTHAIR);
		rdbtnAmericanShorthair.setBackground(Color.CYAN);
		rdbtnAmericanShorthair.setMnemonic(KeyEvent.VK_H);
		panelBreeds.add(rdbtnAmericanShorthair);
		rdbtnAmericanShorthair.addActionListener(new RadioButtonBreedListener());
		
		rdbtnOriental = new JRadioButton(BREED_ORIENTAL);
		rdbtnOriental.setBackground(Color.CYAN);
		rdbtnOriental.setMnemonic(KeyEvent.VK_O);
		panelBreeds.add(rdbtnOriental);
		rdbtnOriental.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSphynx = new JRadioButton(BREED_SPHYNX);
		rdbtnSphynx.setBackground(Color.CYAN);
		rdbtnSphynx.setMnemonic(KeyEvent.VK_Y);
		panelBreeds.add(rdbtnSphynx);
		rdbtnSphynx.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSavannah = new JRadioButton(BREED_SAVANNAH);
		rdbtnSavannah.setBackground(Color.CYAN);
		rdbtnSavannah.setMnemonic(KeyEvent.VK_V);
		panelBreeds.add(rdbtnSavannah);
		rdbtnSavannah.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSiberian = new JRadioButton(BREED_SIBERIAN);
		rdbtnSiberian.setBackground(Color.CYAN);
		rdbtnSiberian.setMnemonic(KeyEvent.VK_N);
		panelBreeds.add(rdbtnSiberian);
		rdbtnSiberian.addActionListener(new RadioButtonBreedListener());
		
		rdbtnNorwegianForestCat = new JRadioButton(BREED_NORWEGIAN_FOREST_CAT);
		rdbtnNorwegianForestCat.setBackground(Color.CYAN);
		rdbtnNorwegianForestCat.setMnemonic(KeyEvent.VK_W);
		panelBreeds.add(rdbtnNorwegianForestCat);
		rdbtnNorwegianForestCat.addActionListener(new RadioButtonBreedListener());
		
		ButtonGroup breeds = new ButtonGroup();
		breeds.add(rdbtnAbyssinian);
		breeds.add(rdbtnAmericanShorthair);
		breeds.add(rdbtnBirman);
		breeds.add(rdbtnExoticShorthair);
		breeds.add(rdbtnMaineCoon);
		breeds.add(rdbtnNorwegianForestCat);
		breeds.add(rdbtnOriental);
		breeds.add(rdbtnPersian);
		breeds.add(rdbtnRagdoll);
		breeds.add(rdbtnSavannah);
		breeds.add(rdbtnSiamese);
		breeds.add(rdbtnSiberian);
		breeds.add(rdbtnSphynx);
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(radioButtonMale);
		gender.add(radioButtonFemale);
		
		setVisible(true);
	}
	
	private class RadioButtonBreedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(rdbtnAbyssinian.isSelected()) {
				petBreed = BREED_ABYSSINIAN;
			}
			if(rdbtnAmericanShorthair.isSelected()) {
				petBreed = BREED_AMERICAN_SHORTHAIR;
			}
			if(rdbtnBirman.isSelected()) {
				petBreed = BREED_BIRMAN;
			}
			if(rdbtnExoticShorthair.isSelected()) {
				petBreed = BREED_EXOTIC_SHORTHAIR;
			}
			if(rdbtnMaineCoon.isSelected()) {
				petBreed = BREED_MAINE_COON;
			}
			if(rdbtnNorwegianForestCat.isSelected()) {
				petBreed = BREED_NORWEGIAN_FOREST_CAT;
			}
			if(rdbtnOriental.isSelected()) {
				petBreed = BREED_ORIENTAL;
			}
			if(rdbtnPersian.isSelected()) {
				petBreed = BREED_PERSIAN;
			}
			if(rdbtnRagdoll.isSelected()) {
				petBreed = BREED_RAGDOLL;
			}
			if(rdbtnSavannah.isSelected()) {
				petBreed = BREED_SAVANNAH;
			}
			if(rdbtnSiamese.isSelected()) {
				petBreed = BREED_SIAMESE;
			}
			if(rdbtnSiberian.isSelected()) {
				petBreed = BREED_SIBERIAN;
			}
			if(rdbtnSphynx.isSelected()) {
				petBreed = BREED_SPHYNX;
			}
			
		}
		
	}
	private class RadioGenderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(radioButtonMale.isSelected()) {
				petGender = GENDER_MALE;
			}
			if(radioButtonFemale.isSelected()) {
				petGender = GENDER_FEMALE;
			}
		}
		
	}
	private class AddPetListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			petName = textFieldName.getText();
			try {
				petAge = Double.parseDouble(textFieldAge.getText());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(rootPane, "Invalid age, please inter a valid number.");
			}
			petDescription = textAreaDescription.getText();
			
			Pet pet = new Cat(petName, species, petAge, petGender, petBreed, petDescription);
			dlm.add(0,pet);
		}
		
	}
	private class ExitFrameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dispose();
		}
		
	}
}
