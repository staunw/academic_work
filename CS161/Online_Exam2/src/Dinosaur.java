
public class Dinosaur extends Pet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2776228496557329416L;

	private final static String SPECIES = "Dinosaur";

	private String breed;
	private String description;
	
	public Dinosaur(String name, String species, double age, String pronoun, String breed, String description) {
		super(name, SPECIES, age, pronoun);
		this.setBreed(breed);
		this.description = description;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String toString() {
		return super.toString();
	}
	public String toStringDescription() {
		return super.toString() + " is " + age + " years old. " + pronoun + " is a " 
								+ breed + " and " + description; 
	}
}
