import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import java.awt.event.KeyEvent;

public class AddPigFrame extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7896039607558083851L;
	private final String BREED_KUNE_KUNE = "Kune Kune";
	private final String BREED_VIETNAMESE_POTBELLY = "Vietnamese Potbelly";
	private final String BREED_PAINTED_MINI = "Painted Mini";
	private final String GENDER_MALE = "He";
	private final String GENDER_FEMALE = "She";
	private String petName;
	private String petGender;
	private double petAge;
	private String petDescription;
	private String petBreed;
	private final String species = "Pig";
	private DefaultListModel<Pet> dlm;
	
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JPanel panel;
	private JPanel panelInfo;
	private JPanel panelName;
	private JPanel panelGender;
	private JRadioButton radioButtonMale;
	private JRadioButton radioButtonFemale;
	private JPanel panelAge;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelBreeds;
	private JRadioButton rdbtnVietnamesePotbelly;
	private JRadioButton rdbtnKuneKune;
	private JRadioButton rdbtnPaintedMini;
	private JScrollPane scrollBarDescription;
	private JPanel panelAdd;
	private JButton buttonAddPet;
	private JButton btnExit;
	
	public AddPigFrame(DefaultListModel<Pet> dlm) {
		this.dlm = dlm;
		
		setSize(new Dimension(700, 430));
		setPreferredSize(new Dimension(700, 430));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelAdd = new JPanel();
		panelAdd.setBackground(Color.CYAN);
		getContentPane().add(panelAdd, BorderLayout.SOUTH);
		
		buttonAddPet = new JButton("Add Pet");
		buttonAddPet.setMnemonic(KeyEvent.VK_D);
		buttonAddPet.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(buttonAddPet);
		
		btnExit = new JButton("Exit");
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(btnExit);
		buttonAddPet.addActionListener(new AddPetListener());
		btnExit.addActionListener(new ExitFrameListener());
		
		panelInfo = new JPanel();
		panel.add(panelInfo);
		panelInfo.setLayout(new GridLayout(2, 2, 0, 0));
		
		panelName = new JPanel();
		panelName.setBackground(Color.CYAN);
		panelName.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.add(panelName);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("Enter the personal name of the pet");
		panelName.add(textFieldName);
		textFieldName.setColumns(10);
		
		panelGender = new JPanel();
		panelGender.setBackground(Color.CYAN);
		panelGender.setBorder(new TitledBorder(null, "Male/Female", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelGender);
		
		radioButtonMale = new JRadioButton("Male");
		radioButtonMale.setMnemonic(KeyEvent.VK_M);
		panelGender.add(radioButtonMale);
		radioButtonMale.addActionListener(new RadioGenderListener());
		
		radioButtonFemale = new JRadioButton("Female");
		radioButtonFemale.setMnemonic(KeyEvent.VK_F);
		panelGender.add(radioButtonFemale);
		radioButtonFemale.addActionListener(new RadioGenderListener());
		
		panelAge = new JPanel();
		panelAge.setBackground(Color.CYAN);
		panelAge.setBorder(new TitledBorder(null, "Age", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelAge);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("Enter the current age of the pet");
		panelAge.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		panelDescription = new JPanel();
		panelDescription.setBackground(Color.CYAN);
		panelDescription.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setLineWrap(true);
		textAreaDescription.setColumns(15);
		textAreaDescription.setRows(8);
		textAreaDescription.setToolTipText("Enter a short description of the pet");
		scrollBarDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollBarDescription);
		
		panelBreeds = new JPanel();
		panelBreeds.setBackground(Color.CYAN);
		panelBreeds.setBorder(new TitledBorder(null, "Breed of Pig", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelBreeds);
		panelBreeds.setLayout(new GridLayout(13, 0, 0, 0));
		
		rdbtnVietnamesePotbelly = new JRadioButton(BREED_VIETNAMESE_POTBELLY);
		rdbtnVietnamesePotbelly.setBackground(Color.CYAN);
		rdbtnVietnamesePotbelly.setMnemonic(KeyEvent.VK_V);
		panelBreeds.add(rdbtnVietnamesePotbelly);
		rdbtnVietnamesePotbelly.addActionListener(new RadioButtonBreedListener());
		
		rdbtnKuneKune = new JRadioButton(BREED_KUNE_KUNE);
		rdbtnKuneKune.setBackground(Color.CYAN);
		rdbtnKuneKune.setMnemonic(KeyEvent.VK_K);
		panelBreeds.add(rdbtnKuneKune);
		rdbtnKuneKune.addActionListener(new RadioButtonBreedListener());
		
		rdbtnPaintedMini = new JRadioButton(BREED_PAINTED_MINI);
		rdbtnPaintedMini.setBackground(Color.CYAN);
		rdbtnPaintedMini.setMnemonic(KeyEvent.VK_P);
		panelBreeds.add(rdbtnPaintedMini);
		rdbtnPaintedMini.addActionListener(new RadioButtonBreedListener());
		
		
		
		ButtonGroup breeds = new ButtonGroup();
		breeds.add(rdbtnKuneKune);
		breeds.add(rdbtnVietnamesePotbelly);
		breeds.add(rdbtnPaintedMini);
		
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(radioButtonMale);
		gender.add(radioButtonFemale);
		
		setVisible(true);
	}
	
	private class RadioButtonBreedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(rdbtnKuneKune.isSelected()) {
				petBreed = BREED_KUNE_KUNE;
			}
			if(rdbtnVietnamesePotbelly.isSelected()) {
				petBreed = BREED_VIETNAMESE_POTBELLY;
			}
			if(rdbtnPaintedMini.isSelected()) {
				petBreed = BREED_PAINTED_MINI;
			}
			
			
		}
		
	}
	private class RadioGenderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(radioButtonMale.isSelected()) {
				petGender = GENDER_MALE;
			}
			if(radioButtonFemale.isSelected()) {
				petGender = GENDER_FEMALE;
			}
		}
		
	}
	private class AddPetListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			petName = textFieldName.getText();
			try {
				petAge = Double.parseDouble(textFieldAge.getText());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(rootPane, "Invalid age, please inter a valid number.");
			}
			petDescription = textAreaDescription.getText();
			
			Pet pet = new Pig(petName, species, petAge, petGender, petBreed, petDescription);
			dlm.add(0,pet);
		}
		
	}
	private class ExitFrameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dispose();
		}
		
	}
}
