import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.KeyEvent;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class PetShopGUI extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8793339934486839101L;
	
	private final ImageIcon IMAGE_DOG = new ImageIcon("assets/dog.png");
	private final ImageIcon IMAGE_FISH = new ImageIcon("assets/aquarium.png");
	private final ImageIcon IMAGE_CAT = new ImageIcon("assets/kitty.png");
	private final ImageIcon iMAGE_PIG = new ImageIcon("assets/pig.png");
	private final ImageIcon IMAGE_RED_PANDA = new ImageIcon("assets/red-panda.png");
	private final ImageIcon IMAGE_EGG = new ImageIcon("assets/egg.png");
	
	
	
	private int selectedIndex = -1;
	private JPanel panelBuy;
	private JButton buttonPurchasePet;
	private JPanel panelArt1;
	private JPanel panelArt2;
	private JPanel panelMain;
	private JList<Pet> list;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem menuSaveInventory;
	private JMenuItem menuLoadInventory;
	private JMenu menuAdministrator;
	private JScrollPane scrollPanePetList;
	private DefaultListModel<Pet> dlm = new DefaultListModel<Pet>(); 
	private JMenuItem mntmAddNewCat;
	private JMenuItem mntmAddNewDog;
	private JMenuItem mntmAddNewFish;
	private JMenuItem mntmAddNewPig;
	//private JMenuItem mntmAddNewRed;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelList;
	private JScrollPane scrollPaneDescription;
	private JMenuItem mntmExit;
	private JPanel panelTitle;
	private JLabel labelTitle;
	private JLabel labelDog;
	private JLabel labelFish;
	private JLabel labelPig;
	private JLabel labelCat;
	private JLabel labelRedPanda;
	private JMenu mnCredits;
	private JMenuItem mntmDisplayCredits;
	private JMenuItem mntmAddNewDinosaur;
	private JLabel labelEgg;

	public PetShopGUI() {
		setSize(new Dimension(900, 430));
		setResizable(false);
		setTitle("Let's Make Lots of Money");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		panelBuy = new JPanel();
		panelBuy.setBackground(new Color(0, 255, 255));
		getContentPane().add(panelBuy, BorderLayout.SOUTH);
		
		buttonPurchasePet = new JButton("Purchase Pet");
		buttonPurchasePet.setMnemonic(KeyEvent.VK_P);
		buttonPurchasePet.setToolTipText("Purchase the selected pet");
		panelBuy.add(buttonPurchasePet);
		buttonPurchasePet.addActionListener(new PurchasePetListener());
		
		panelArt1 = new JPanel();
		panelArt1.setBackground(new Color(0, 255, 255));
		getContentPane().add(panelArt1, BorderLayout.WEST);
		panelArt1.setLayout(new GridLayout(3, 0, 0, 0));
		
		labelDog = new JLabel("");
		panelArt1.add(labelDog);
		labelDog.setIcon(IMAGE_DOG);
		
		labelFish = new JLabel("");
		panelArt1.add(labelFish);
		labelFish.setIcon(IMAGE_FISH);
		
		labelPig = new JLabel("");
		panelArt1.add(labelPig);
		labelPig.setIcon(iMAGE_PIG);
		
		panelArt2 = new JPanel();
		panelArt2.setBackground(new Color(0, 255, 255));
		getContentPane().add(panelArt2, BorderLayout.EAST);
		panelArt2.setLayout(new GridLayout(3, 1, 0, 0));
		
		labelCat = new JLabel("");
		panelArt2.add(labelCat);
		labelCat.setIcon(IMAGE_CAT);
		
		labelRedPanda = new JLabel("");
		panelArt2.add(labelRedPanda);
		labelRedPanda.setIcon(IMAGE_RED_PANDA);
		
		labelEgg = new JLabel("");
		panelArt2.add(labelEgg);
		labelEgg.setIcon(IMAGE_EGG);
		
		panelMain = new JPanel();
		getContentPane().add(panelMain, BorderLayout.CENTER);
		panelMain.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelList = new JPanel();
		panelList.setBackground(new Color(0, 255, 255));
		panelList.setBorder(new TitledBorder(null, "Pets for Sale", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMain.add(panelList);
		
		list = new JList<Pet>(dlm);
		list.setVisibleRowCount(19);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setToolTipText("List of pets available for purchase");
		scrollPanePetList = new JScrollPane(list);
		panelList.add(scrollPanePetList);
		list.addListSelectionListener(new PetListListener());
		
		panelDescription = new JPanel();
		panelDescription.setBackground(new Color(0, 255, 255));
		panelDescription.setBorder(new TitledBorder(null, "Description of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMain.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setEditable(false);
		textAreaDescription.setToolTipText("Displays a short description of the pet selected");
		textAreaDescription.setColumns(30);
		textAreaDescription.setWrapStyleWord(true);
		textAreaDescription.setRows(16);
		textAreaDescription.setLineWrap(true);
		scrollPaneDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollPaneDescription);
		
		panelTitle = new JPanel();
		panelTitle.setBackground(new Color(0, 255, 255));
		panelTitle.setForeground(Color.BLUE);
		getContentPane().add(panelTitle, BorderLayout.NORTH);
		
		labelTitle = new JLabel("The Pet Shoppe Boys");
		labelTitle.setFont(new Font("Stencil", Font.BOLD, 20));
		panelTitle.add(labelTitle);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		mnFile.setMnemonic(KeyEvent.VK_F);
		menuBar.add(mnFile);
		
		menuSaveInventory = new JMenuItem("Save Inventory");
		menuSaveInventory.setMnemonic(KeyEvent.VK_S);
		mnFile.add(menuSaveInventory);
		menuSaveInventory.addActionListener(new SaveFileListener());
		
		menuLoadInventory = new JMenuItem("Load Inventory");
		menuLoadInventory.setMnemonic(KeyEvent.VK_L);
		mnFile.add(menuLoadInventory);
		menuLoadInventory.addActionListener(new LoadFileListener());
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.setMnemonic(KeyEvent.VK_X);
		mnFile.add(mntmExit);
		mntmExit.addActionListener(new ExitButtonListner());
		
		menuAdministrator = new JMenu("Administrator");
		menuAdministrator.setMnemonic(KeyEvent.VK_A);
		menuBar.add(menuAdministrator);
		
		mntmAddNewCat = new JMenuItem("Add New Cat");
		mntmAddNewCat.setMnemonic(KeyEvent.VK_C);
		menuAdministrator.add(mntmAddNewCat);
		mntmAddNewCat.addActionListener(new NewCatListener());
		
		mntmAddNewDog = new JMenuItem("Add New Dog");
		mntmAddNewDog.setMnemonic(KeyEvent.VK_D);
		menuAdministrator.add(mntmAddNewDog);
		mntmAddNewDog.addActionListener(new NewDogListener());
		
		mntmAddNewFish = new JMenuItem("Add New Fish");
		mntmAddNewFish.setMnemonic(KeyEvent.VK_F);
		menuAdministrator.add(mntmAddNewFish);
		mntmAddNewFish.addActionListener(new NewFishListener());
		
		mntmAddNewPig = new JMenuItem("Add New Pig");
		mntmAddNewPig.setMnemonic(KeyEvent.VK_P);
		menuAdministrator.add(mntmAddNewPig);
		mntmAddNewPig.addActionListener(new NewPigListener());
		
		mntmAddNewDinosaur = new JMenuItem("Add New Dinosaur");
		mntmAddNewDinosaur.setMnemonic(KeyEvent.VK_R);
		menuAdministrator.add(mntmAddNewDinosaur);
		mntmAddNewDinosaur.addActionListener(new NewDinosaurListener());
		
		mnCredits = new JMenu("Credits");
		menuBar.add(mnCredits);
		
		mntmDisplayCredits = new JMenuItem("Display Credits");
		mnCredits.add(mntmDisplayCredits);
		
		mntmDisplayCredits.addActionListener(new DisplayCreditsListener());
		
		setVisible(true);
	}
	private class PurchasePetListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ((selectedIndex >= 0) && (selectedIndex <= dlm.size())) {
				dlm.remove(selectedIndex);
			}
			
		}
		
	}
	private class PetListListener implements ListSelectionListener {
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				selectedIndex = list.getSelectedIndex();
				if (selectedIndex >= 0 && selectedIndex < dlm.size()) {
					textAreaDescription.setText(dlm.get(selectedIndex).toStringDescription());
				}else {
					textAreaDescription.setText("");
				}
			}
			
		}
		
	}
	
	private class SaveFileListener implements ActionListener {
		String fileName;
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
//		    FileNameExtensionFilter filter = new FileNameExtensionFilter("dat");
//		    chooser.setFileFilter(filter);
		    int returnVal = chooser.showOpenDialog(rootPane);
			try {
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			           fileName = chooser.getSelectedFile().getName();
			           FileOutputStream outStream = new FileOutputStream(fileName);
			           ObjectOutputStream objectOutputFile = new ObjectOutputStream(outStream);
			           for (Object pets : dlm.toArray()) {
			        	   objectOutputFile.writeObject(pets);
			           }
			           objectOutputFile.close();
			    }
				
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(rootPane, "There was a problem saving the file");
			}
			
		}
		
	}
		String fileName;
		ObjectInputStream objectInputFile;
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			 int returnVal = chooser.showOpenDialog(rootPane);
			 try {
				 if(returnVal == JFileChooser.APPROVE_OPTION) {
			           fileName = chooser.getSelectedFile().getName();
			           FileInputStream inStream = new FileInputStream(fileName);
			           objectInputFile = new ObjectInputStream(inStream);
			           while (true) {
			        	   dlm.add(0,(Pet) objectInputFile.readObject());
			           }
				 }      
			 }catch (EOFException eof) {
				
			 }catch (Exception ex) {
				 JOptionPane.showMessageDialog(rootPane, "There was a problem loading the file");
			 }
		}
		
	}
	private class NewCatListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new AddCatFrame(dlm);
			
		}
		
	}
	private class NewDogListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new AddDogFrame(dlm);
			
		}
		
	}
	private class NewFishListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new AddFishFrame(dlm);
			
		}
		
	}
	private class NewPigListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new AddPigFrame(dlm);
			
		}
		
	}
	private class NewDinosaurListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new AddDinosaurFrame(dlm);
			
		}
		
	}
	private class ExitButtonListner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
			
		}
		
	}
	public class DisplayCreditsListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(rootPane, "Icons made by Freepik" 
					+ "\nhttps://www.flaticon.com/authors/freepik" 
					+ "\nfrom www.flaticon.com ");
			
		}
		
	}
	public static void main(String[] petShopBoys) {
		new PetShopGUI();
	}
}
