
public class Pig extends Pet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7845627758151192492L;
	/**
	 * 
	 */
	
	private static final String SPECIES = "Pig";
	private String breed;
	private String description;
	
	public Pig(String name, String species, double age, String pronoun, String breed, String description) {
		super(name, SPECIES, age, pronoun);
		this.breed = breed;
		this.description = description;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	public String getDescription() {
		return description;
	}
	public String toString() {
		return super.toString();
	}
	public String toStringDescription() {
		return super.toString() + " is " + age + " years old. " + pronoun + " is a " 
								+ breed + " and " + description; 
	}
	
}
