import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class AddDinosaurFrame extends JFrame{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6756844579435119010L;
	private final String GENDER_MALE = "He";
	private final String GENDER_FEMALE = "She";
	private final String BREED_APATOSAURUS = "Apatosaurus";
	private final String BREED_DILOPHOSAURUS = "Dilophosaurus";
	private final String BREED_VELOCIRAPTOR = "Velociraptor";
	private final String BREED_STEGOSAURUS = "Stegosaurus";
	private final String BREED_TIRCERATOPS = "Triceratops";
	private final String BREED_SPINOSAURUS = "Spinosaurus";
	private final String BREED_PARASAUROLOPHUS = "Parasaurolophus";
	private final String BREED_HYPSILOPHODON = "Hypsilophodon";
	private final String BREED_TYRANNOSAURS_REX = "Tyrannosaurus Rex";
	private final String BREED_ANKYLOSAURUS = "Ankylosaurus";
	private final String BREED_PTERANODON = "Pteranodon";
	private final String BREED_THERIZINOSAURUS = "Therizinosaurus";
	private final String BREED_PACHYCEPHALOSAURUS = "Pachycephalosaurus";
	private String petName;
	private String petGender;
	private double petAge;
	private String petDescription;
	private String petBreed;
	private final String species = "Dinosaur";
	private DefaultListModel<Pet> dlm;
	
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JPanel panel;
	private JPanel panelInfo;
	private JPanel panelName;
	private JPanel panelGender;
	private JRadioButton radioButtonMale;
	private JRadioButton radioButtonFemale;
	private JPanel panelAge;
	private JPanel panelDescription;
	private JTextArea textAreaDescription;
	private JPanel panelBreeds;
	private JRadioButton rdbtnHypsilophodon;
	private JRadioButton rdbtnTriceratops;
	private JRadioButton rdbtnPteranodon;
	private JRadioButton rdbtnStegosaurus;
	private JRadioButton rdbtnApatosaurus;
	private JRadioButton rdbtnTyrannosaurusRex;
	private JRadioButton rdbtnVelociraptor;
	private JRadioButton rdbtnDilophosaurus;
	private JRadioButton rdbtnParasaurolophus;
	private JRadioButton rdbtnPachycephalosaurus;
	private JRadioButton rdbtnAnkylosaurus;
	private JRadioButton rdbtnTherizinosaurus;
	private JRadioButton rdbtnSpinosaurus;
	private JScrollPane scrollBarDescription;
	private JPanel panelAdd;
	private JButton buttonAddPet;
	private JButton btnExit;
	
	public AddDinosaurFrame(DefaultListModel<Pet> dlm) {
		this.dlm = dlm;
		
		setSize(new Dimension(700, 430));
		setPreferredSize(new Dimension(700, 430));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("I walk the dinosaur");
		
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		panelAdd = new JPanel();
		panelAdd.setBackground(Color.CYAN);
		getContentPane().add(panelAdd, BorderLayout.SOUTH);
		
		buttonAddPet = new JButton("Add Pet");
		buttonAddPet.setMnemonic(KeyEvent.VK_D);
		buttonAddPet.setPreferredSize(new Dimension(100, 24));
		panelAdd.add(buttonAddPet);
		
		btnExit = new JButton("Exit");
		btnExit.setMnemonic(KeyEvent.VK_X);
		btnExit.setSize(new Dimension(71, 24));
		btnExit.setPreferredSize(new Dimension(100, 24));
		btnExit.setMinimumSize(new Dimension(71, 23));
		btnExit.setMaximumSize(new Dimension(71, 23));
		panelAdd.add(btnExit);
		buttonAddPet.addActionListener(new AddPetListener());
		btnExit.addActionListener(new ExitFrameListener());
		
		panelInfo = new JPanel();
		panel.add(panelInfo);
		panelInfo.setLayout(new GridLayout(2, 2, 0, 0));
		
		panelName = new JPanel();
		panelName.setBackground(Color.CYAN);
		panelName.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Name of Pet", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfo.add(panelName);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("Enter the personal name of the pet");
		panelName.add(textFieldName);
		textFieldName.setColumns(10);
		
		panelGender = new JPanel();
		panelGender.setBackground(Color.CYAN);
		panelGender.setBorder(new TitledBorder(null, "Male/Female", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelGender);
		
		radioButtonMale = new JRadioButton("Male");
		radioButtonMale.setMnemonic(KeyEvent.VK_M);
		panelGender.add(radioButtonMale);
		radioButtonMale.addActionListener(new RadioGenderListener());
		
		radioButtonFemale = new JRadioButton("Female");
		radioButtonFemale.setMnemonic(KeyEvent.VK_F);
		panelGender.add(radioButtonFemale);
		radioButtonFemale.addActionListener(new RadioGenderListener());
		
		panelAge = new JPanel();
		panelAge.setBackground(Color.CYAN);
		panelAge.setBorder(new TitledBorder(null, "Age", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelAge);
		
		textFieldAge = new JTextField();
		textFieldAge.setToolTipText("Enter the current age of the pet");
		panelAge.add(textFieldAge);
		textFieldAge.setColumns(10);
		
		panelDescription = new JPanel();
		panelDescription.setBackground(Color.CYAN);
		panelDescription.setBorder(new TitledBorder(null, "Description", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.add(panelDescription);
		
		textAreaDescription = new JTextArea();
		textAreaDescription.setLineWrap(true);
		textAreaDescription.setColumns(15);
		textAreaDescription.setRows(8);
		textAreaDescription.setToolTipText("Enter a short description of the pet");
		scrollBarDescription = new JScrollPane(textAreaDescription);
		panelDescription.add(scrollBarDescription);
		
		panelBreeds = new JPanel();
		panelBreeds.setBackground(Color.CYAN);
		panelBreeds.setBorder(new TitledBorder(null, "Breed of Dinosaur", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelBreeds);
		panelBreeds.setLayout(new GridLayout(13, 0, 0, 0));
		
		rdbtnHypsilophodon = new JRadioButton(BREED_HYPSILOPHODON);
		rdbtnHypsilophodon.setBackground(Color.CYAN);
		rdbtnHypsilophodon.setMnemonic(KeyEvent.VK_S);
		panelBreeds.add(rdbtnHypsilophodon);
		rdbtnHypsilophodon.addActionListener(new RadioButtonBreedListener());
		
		rdbtnTriceratops = new JRadioButton(BREED_TIRCERATOPS);
		rdbtnTriceratops.setBackground(Color.CYAN);
		rdbtnTriceratops.setMnemonic(KeyEvent.VK_T);
		panelBreeds.add(rdbtnTriceratops);
		rdbtnTriceratops.addActionListener(new RadioButtonBreedListener());
		
		rdbtnPteranodon = new JRadioButton(BREED_PTERANODON);
		rdbtnPteranodon.setBackground(Color.CYAN);
		rdbtnPteranodon.setMnemonic(KeyEvent.VK_P);
		panelBreeds.add(rdbtnPteranodon);
		rdbtnPteranodon.addActionListener(new RadioButtonBreedListener());
		
		rdbtnStegosaurus = new JRadioButton(BREED_STEGOSAURUS);
		rdbtnStegosaurus.setBackground(Color.CYAN);
		rdbtnStegosaurus.setMnemonic(KeyEvent.VK_G);
		panelBreeds.add(rdbtnStegosaurus);
		rdbtnStegosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnApatosaurus = new JRadioButton(BREED_APATOSAURUS);
		rdbtnApatosaurus.setBackground(Color.CYAN);
		rdbtnApatosaurus.setMnemonic(KeyEvent.VK_A);
		panelBreeds.add(rdbtnApatosaurus);
		rdbtnApatosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnTyrannosaurusRex = new JRadioButton(BREED_TYRANNOSAURS_REX);
		rdbtnTyrannosaurusRex.setBackground(Color.CYAN);
		rdbtnTyrannosaurusRex.setMnemonic(KeyEvent.VK_R);
		panelBreeds.add(rdbtnTyrannosaurusRex);
		rdbtnTyrannosaurusRex.addActionListener(new RadioButtonBreedListener());
		
		rdbtnVelociraptor = new JRadioButton(BREED_VELOCIRAPTOR);
		rdbtnVelociraptor.setBackground(Color.CYAN);
		rdbtnVelociraptor.setMnemonic(KeyEvent.VK_V);
		panelBreeds.add(rdbtnVelociraptor);
		rdbtnVelociraptor.addActionListener(new RadioButtonBreedListener());
		
		rdbtnDilophosaurus = new JRadioButton(BREED_DILOPHOSAURUS);
		rdbtnDilophosaurus.setBackground(Color.CYAN);
		rdbtnDilophosaurus.setMnemonic(KeyEvent.VK_L);
		panelBreeds.add(rdbtnDilophosaurus);
		rdbtnDilophosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnParasaurolophus = new JRadioButton(BREED_PARASAUROLOPHUS);
		rdbtnParasaurolophus.setBackground(Color.CYAN);
		rdbtnParasaurolophus.setMnemonic(KeyEvent.VK_U);
		panelBreeds.add(rdbtnParasaurolophus);
		rdbtnParasaurolophus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnPachycephalosaurus = new JRadioButton(BREED_PACHYCEPHALOSAURUS);
		rdbtnPachycephalosaurus.setBackground(Color.CYAN);
		rdbtnPachycephalosaurus.setMnemonic(KeyEvent.VK_H);
		panelBreeds.add(rdbtnPachycephalosaurus);
		rdbtnPachycephalosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnAnkylosaurus = new JRadioButton(BREED_ANKYLOSAURUS);
		rdbtnAnkylosaurus.setBackground(Color.CYAN);
		rdbtnAnkylosaurus.setMnemonic(KeyEvent.VK_K);
		panelBreeds.add(rdbtnAnkylosaurus);
		rdbtnAnkylosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnTherizinosaurus = new JRadioButton(BREED_THERIZINOSAURUS);
		rdbtnTherizinosaurus.setBackground(Color.CYAN);
		rdbtnTherizinosaurus.setMnemonic(KeyEvent.VK_Z);
		panelBreeds.add(rdbtnTherizinosaurus);
		rdbtnTherizinosaurus.addActionListener(new RadioButtonBreedListener());
		
		rdbtnSpinosaurus = new JRadioButton(BREED_SPINOSAURUS);
		rdbtnSpinosaurus.setBackground(Color.CYAN);
		rdbtnSpinosaurus.setMnemonic(KeyEvent.VK_N);
		panelBreeds.add(rdbtnSpinosaurus);
		rdbtnSpinosaurus.addActionListener(new RadioButtonBreedListener());
		
		ButtonGroup breeds = new ButtonGroup();
		breeds.add(rdbtnApatosaurus);
		breeds.add(rdbtnDilophosaurus);
		breeds.add(rdbtnVelociraptor);
		breeds.add(rdbtnStegosaurus);
		breeds.add(rdbtnTriceratops);
		breeds.add(rdbtnSpinosaurus);
		breeds.add(rdbtnParasaurolophus);
		breeds.add(rdbtnHypsilophodon);
		breeds.add(rdbtnTyrannosaurusRex);
		breeds.add(rdbtnAnkylosaurus);
		breeds.add(rdbtnPteranodon);
		breeds.add(rdbtnTherizinosaurus);
		breeds.add(rdbtnPachycephalosaurus);
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(radioButtonMale);
		gender.add(radioButtonFemale);
		
		setVisible(true);
	}
	
	private class RadioButtonBreedListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(rdbtnApatosaurus.isSelected()) {
				petBreed = BREED_APATOSAURUS;
			}
			if(rdbtnDilophosaurus.isSelected()) {
				petBreed = BREED_DILOPHOSAURUS;
			}
			if(rdbtnVelociraptor.isSelected()) {
				petBreed = BREED_VELOCIRAPTOR;
			}
			if(rdbtnStegosaurus.isSelected()) {
				petBreed = BREED_STEGOSAURUS;
			}
			if(rdbtnTriceratops.isSelected()) {
				petBreed = BREED_TIRCERATOPS;
			}
			if(rdbtnSpinosaurus.isSelected()) {
				petBreed = BREED_SPINOSAURUS;
			}
			if(rdbtnParasaurolophus.isSelected()) {
				petBreed = BREED_PARASAUROLOPHUS;
			}
			if(rdbtnHypsilophodon.isSelected()) {
				petBreed = BREED_HYPSILOPHODON;
			}
			if(rdbtnTyrannosaurusRex.isSelected()) {
				petBreed = BREED_TYRANNOSAURS_REX;
			}
			if(rdbtnAnkylosaurus.isSelected()) {
				petBreed = BREED_ANKYLOSAURUS;
			}
			if(rdbtnPteranodon.isSelected()) {
				petBreed = BREED_PTERANODON;
			}
			if(rdbtnTherizinosaurus.isSelected()) {
				petBreed = BREED_THERIZINOSAURUS;
			}
			if(rdbtnPachycephalosaurus.isSelected()) {
				petBreed = BREED_PACHYCEPHALOSAURUS;
			}
			
		}
		
	}
	private class RadioGenderListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(radioButtonMale.isSelected()) {
				petGender = GENDER_MALE;
			}
			if(radioButtonFemale.isSelected()) {
				petGender = GENDER_FEMALE;
			}
		}
		
	}
	private class AddPetListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			petName = textFieldName.getText();
			try {
				petAge = Double.parseDouble(textFieldAge.getText());
			}catch (Exception ex){
				JOptionPane.showMessageDialog(rootPane, "Invalid age, please inter a valid number.");
			}
			petDescription = textAreaDescription.getText();
			
			Pet pet = new Dinosaur(petName, species, petAge, petGender, petBreed, petDescription);
			dlm.add(0,pet);
		}
		
	}
	private class ExitFrameListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dispose();
		}
		
	}
}
