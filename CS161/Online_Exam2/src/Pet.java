import java.io.Serializable;

public abstract class Pet implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5626198613891988841L;
	protected String petName;
	protected String species;
	protected double age;
	protected String pronoun;
	
	public Pet(String name, String species, double age, String pronoun) {
		this.petName = name;
		this.species = species;
		this.age = age;
		this.pronoun = pronoun;
	}
	public String getName() {
		return this.petName;
	}
	
	public String getSpecies() {
		return this.species;
	}
	
	public double getAge() {
		return this.age;
	}
	
	public String toString() {
		return this.petName + " the " 
			   + this.species;
	}
	public abstract String toStringDescription();
}
